<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant_setting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Authentication_model');
        $this->load->model('Common_model');
        $this->load->model('Outlet_model');
        $this->load->library('form_validation');
        $this->Common_model->setDefaultTimezone();
        
        if (!$this->session->has_userdata('user_id')) {
            redirect('Authentication/index');
        }

        if ($this->session->userdata('role') != 'Admin') {
            redirect('Authentication/index');
        }
    }

    public function setting($id = '') {
        $encrypted_id = $id = $outlet_id = $this->session->userdata('outlet_id');
        $id = $this->custom->encrypt_decrypt($id, 'decrypt');

        
        
        if ($this->input->post('submit')) {
            // dd($this->input->post());
            $this->form_validation->set_rules('outlet_name', 'Outlet Name', 'required|max_length[50]');
            $this->form_validation->set_rules('address', 'Address', 'required|max_length[200]');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('invoice_footer', 'Invoice Footer', 'max_length[500]');
            if ($this->form_validation->run() == TRUE) {
                $outlet_info = array();
                $outlet_info['outlet_name'] = $this->input->post($this->security->xss_clean('outlet_name'));
                $outlet_info['address'] = $this->input->post($this->security->xss_clean('address'));
                $outlet_info['phone'] = $this->input->post($this->security->xss_clean('phone'));
               
                $outlet_info['invoice_footer'] = $this->input->post($this->security->xss_clean('invoice_footer'));
                $outlet_info['pre_or_post_payment'] = $this->input->post($this->security->xss_clean('pre_or_post_payment'));
                if ($id == "") {
                    $outlet_info['starting_date'] = date("Y-m-d"); 
                    $outlet_info['user_id'] = $this->session->userdata('user_id');
                    $outlet_info['company_id'] = $this->session->userdata('company_id');
                    $outlet_info['outlet_code'] = $this->Outlet_model->generateOutletCode();
                }

                

                    $this->Common_model->updateInformation($outlet_info, $id, "tbl_outlets");
                    $this->Common_model->deletingMultipleFormData('outlet_id', $id, 'tbl_outlet_taxes');
					
                    $this->session->set_flashdata('exception', 'Information has been updated successfully!');
                
                redirect('Restaurant_setting/setting');
            } else {
                $data = array();
                $data['encrypted_id'] = $encrypted_id;
                $data['outlet_information'] = $this->Common_model->getDataById($id, "tbl_outlets");
                $data['outlet_taxes'] = $this->Outlet_model->getTaxesByOutletId($id);
                $data['main_content'] = $this->load->view('restaurant_setting/editOutlet', $data, TRUE);
                $this->load->view('userHome', $data);
            }
        } else {
            $data = array();
            $data['encrypted_id'] = $encrypted_id;
            $data['outlet_information'] = $this->Common_model->getDataById($id, "tbl_outlets");
            $data['outlet_taxes'] = $this->Outlet_model->getTaxesByOutletId($id);
            $data['main_content'] = $this->load->view('restaurant_setting/editOutlet', $data, TRUE);
            $this->load->view('userHome', $data); 
        }
        
    }
    public function saveOutletTaxes($outlet_taxes, $outlet_id, $table_name)
    {
        foreach($outlet_taxes as $single_tax){
            $oti = array();
            $oti['tax'] = $single_tax;
            $oti['outlet_id'] = $outlet_id;
            $oti['user_id'] = $this->session->userdata('user_id');
            $oti['company_id'] = $this->session->userdata('company_id');
            $this->Common_model->insertInformation($oti, "tbl_outlet_taxes");
        }
    }
}

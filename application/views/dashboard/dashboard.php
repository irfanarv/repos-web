
  <div class="container-fluid">
      <!-- Page-Title -->
      <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Hallo, Selamat 
                  <?php 
                  $waktuskarang = date('H:i:s');
                  if($waktuskarang <'11:00:00')
                  {
                        echo ("Pagi");
                  }elseif($waktuskarang <'15:00:00'){
                    echo ("Siang");
                  }elseif($waktuskarang <'18:00:00'){
                    echo ("Sore");
                  }elseif($waktuskarang <'23:59:59'){
                    echo ("Malam");
                  }
                  ?>
                  <?php echo $this->session->userdata('outlet_name'); ?> 👋</h4>
              </div>
          </div>
      </div>
      <!-- end page title end breadcrumb -->

      <div class="row">
        
          <div class="col-sm-6 col-xl-3">
            <a href="<?php echo base_url(); ?>Master/foodMenus">
              <div class="card">
                  <div class="card-body">
                      <div class="row">
                          <div class="col-4 align-self-center">
                              <div class="icon-info">
                                  <i class="mdi mdi-food text-warning"></i>
                              </div>
                          </div>
                          <div class="col-8 align-self-center text-center">
                              <div class="ml-2 text-right">
                                  <p class="mb-1 text-muted font-13"><?php echo $food_menu_count->data_count; ?> Item </p>
                                  <h4 class="mt-0 mb-1 font-20">Menu Makanan</h4>
                              </div>
                          </div>
                      </div>
                      <div class="progress mt-2" style="height:3px;">
                          <div class="progress-bar progress-animated bg-warning" role="progressbar" style="max-width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
              </div>
            </a>
          </div>

          <div class="col-sm-6 col-xl-3">
            <a href="<?php echo base_url(); ?>Master/ingredients">
              <div class="card">
                  <div class="card-body">
                      <div class="row">
                          <div class="col-4 align-self-center">
                              <div class="icon-info">
                                  <i class="mdi mdi-bowl text-info"></i>
                              </div>
                          </div>
                          <div class="col-8 align-self-center text-center">
                              <div class="ml-2 text-right">
                                  <p class="mb-1 text-muted font-13"><?php echo $ingredient_count->data_count; ?> Bahan</p>
                                  <h4 class="mt-0 mb-1 font-20">Bahan Baku</h4>
                              </div>
                          </div>
                      </div>
                      <div class="progress mt-2" style="height:3px;">
                          <div class="progress-bar progress-animated bg-info" role="progressbar" style="max-width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
              </div>
            </a>
          </div>

          <div class="col-sm-6 col-xl-3">
            <a href="<?php echo base_url(); ?>Inventory/getInventoryAlertList">
              <div class="card">
                  <div class="card-body">
                      <div class="row">
                          <div class="col-4 align-self-center">
                              <div class="icon-info">
                                  <i class=" mdi mdi-store text-danger"></i>
                              </div>
                          </div>
                          <div class="col-8 align-self-center text-center">
                              <div class="ml-2 text-right">
                                  <p class="mb-1 text-muted font-13"><?= getAlertCount() ?> Bahan Hampir Habis </p>
                                  <h4 class="mt-0 mb-1 font-20">Inventory</h4>
                              </div>
                          </div>
                      </div>
                      <div class="progress mt-2" style="height:3px;">
                          <div class="progress-bar progress-animated bg-danger" role="progressbar" style="max-width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
              </div>
            </a>
          </div>

          <div class="col-sm-6 col-xl-3">
            <a href="<?php echo base_url(); ?>Master/foodMenuCategories">
              <div class="card">
                  <div class="card-body">
                      <div class="row">
                          <div class="col-4 align-self-center">
                              <div class="icon-info">
                                  <i class=" mdi mdi-google-circles-communities text-success"></i>
                              </div>
                          </div>
                          <div class="col-8 align-self-center text-center">
                              <div class="ml-2 text-right">
                                  <p class="mb-1 text-muted font-13"><?php echo $unit_count->data_count; ?> Unit Usaha Aktif</p>
                                  <h4 class="mt-0 mb-1 font-20">Unit Usaha</h4>
                              </div>
                          </div>
                      </div>
                      <div class="progress mt-2" style="height:3px;">
                          <div class="progress-bar progress-animated bg-success" role="progressbar" style="max-width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
              </div>
            </a>
          </div>

      </div>

      <div class="row">
          <div class="col-lg-12">
              <div class="card">
                  <div class="card-body" style="height: 380px;">
                      <h5 class="mt-0"><?php echo lang('operational_comparision'); ?> (<?php echo lang('this_month'); ?>)</h5>
                      <div class="chart"> 
                        <div class="chart" id="operational_comparision" style="height: 250px; width: 1200px;"></div>
                      </div>  
                  </div>
              </div>
          </div>
          <!-- end col -->
          
          <!-- end col -->
      </div>

      <div class="row">
          <div class="col-lg-8">
              <div class="card">
                  <div class="card-body" style="height: 380px;">
                      <h5 class="mt-0"><?php echo lang('monthly_sales_comparision'); ?></h5>
                      <div class="chart">
                        <div id="chart_div" style="width: 780px; height: 280px;"></div>
                      </div>   
                  </div>
              </div>
          </div>
          <!-- end col -->
          <div class="col-lg-4">
              <div class="card">
                  <div class="card-body">
                      <h5 class="mt-0">10 Menu Teratas Bulan ini</h5>
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Menu</th>
                                        <th>Terjual</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if ($top_ten_food_menu && !empty($top_ten_food_menu)) { foreach ($top_ten_food_menu as $key => $value) { $key++; ?>  
                                    <tr>
                                        <th scope="row"><?= $key ?></th>
                                        <td><?= $value->menu_name ?></td>
                                        <td>
                                            <span class="badge badge-boxed badge-success"><?= $value->totalQty ?> item</span>
                                        </td>
                                    </tr>
                                  <?php } } ?>     
                                </tbody>
                            </table>
                        </div>
                  </div>
              </div>
        </div>
          <!-- end col -->
      </div>

    <!-- <div class="row">
        <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="mt-0"><?php echo lang('dine'); ?>/<?php echo lang('take_away'); ?> (<?php echo lang('this_month'); ?>)</h5>
                        <div class="chart-responsive" style="height: 350px;">
                        <canvas id="pieChart" style="height:280px"></canvas>
                </div>
                    </div>
                </div>
            </div>

        
    </div> -->

  </div>
  <!-- container -->
  <script src="<?php echo base_url(); ?>assets/new_ui/plugins/chart.js/Chart.js"></script>
  <script src="<?php echo base_url(); ?>assets/new_ui/plugins/chart.js/loader.js"></script>
  <script src="<?php echo base_url(); ?>assets/new_ui/plugins/morris/morris.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/new_ui/plugins/raphael/raphael-min.js"></script>
  <script> 

  $(function () {

    //BAR CHART
    var bar = new Morris.Bar({
      element: 'operational_comparision',
      resize: true,
      data: [
        {y: '<?php echo lang('sale'); ?>', a: <?php echo $sale_sum->sale_sum; ?>},
        {y: 'PPN', a: <?php echo $pajak_sum->pajak_sum; ?>}, 
        {y: 'Piutang Supplier', a: <?php echo $utang->utang; ?>}, 
        {y: 'Pembelian (Lunas)', a: <?php echo $purchase_sum->purchase_sum; ?>},
        {y: '<?php echo lang('expense'); ?>', a: <?php echo $expense_sum->expense_sum; ?>},
        {y: 'Limbah', a: <?php echo $waste_sum->waste_sum; ?>},
        {y: 'Compliment', a: <?php echo $cmpl_sum->cmpl_sum; ?>}
        
      ],
      barColors: ['#3498db', '#f56954'],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Rp'],
      hideHover: 'auto'
    });

  $('#low_stock_ingredients, #top_ten_food_menu, #top_ten_customer, #customer_receivable, #supplier_payable').slimscroll({
    height: '220px'
  });

  // -------------
  // - PIE CHART -
  // -------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
  var pieChart       = new Chart(pieChartCanvas);
  var PieData        = [
    {
      value    : <?php echo $dinein_count->dinein_count; ?>,
      color    : '#6F61AA',
      highlight: '#6F61AA',
      label    : 'Makan ditempat'
    },
    {
      value    : <?php echo $take_away_count->take_away_count; ?>,
      color    : '#f1c40f',
      highlight: '#f1c40f',
      label    : 'Take Away'
    },
    {
      value    : <?php echo $delivery_count->delivery_count; ?>,
      color    : '#6F61AA',
      highlight: '#6F61AA',
      label    : 'Delivery'
    } 
  ];
  var pieOptions     = {
    // Boolean - Whether we should show a stroke on each segment
    segmentShowStroke    : true,
    // String - The colour of each segment stroke
    segmentStrokeColor   : '#fff',
    // Number - The width of each segment stroke
    segmentStrokeWidth   : 1,
    // Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    // Number - Amount of animation steps
    animationSteps       : 100,
    // String - Animation easing effect
    animationEasing      : 'easeOutBounce',
    // Boolean - Whether we animate the rotation of the Doughnut
    animateRotate        : true,
    // Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale         : false,
    // Boolean - whether to make the chart responsive to window resizing
    responsive           : true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio  : false,
    // String - A legend template
    legendTemplate       : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
    // String - A tooltip template
    tooltipTemplate      : '<%=value %> <%=label%> '
  };
  // Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
  // -----------------
  // - END PIE CHART -
  // -----------------  
  })

</script>


<script type="text/javascript">
  var currency = "<?php echo $this->session->userdata('currency') ?>";
        var base_url = "<?php echo base_url();?>";
  $(document).ready(function(){
      selectMonth(12);
      $('#operational_coparision_range').on('click',function(){
        $('#operation_comparision_range_fields').show();
      });
      $('#operation_comparision_cancel').on('click',function(){
        $('#operation_comparision_range_fields').hide();
      })
      $('#operation_comparision_input').daterangepicker({
        opens: 'left',
        locale: {
          format: 'YYYY-MM-DD'
        }
      });
      $('#operation_comparision_submit').on('click',function(){
        
        var date_range = $('#operation_comparision_input').val();
        var date_range_array = date_range.split(" - ");
        var from_this_day = date_range_array[0];
        var to_this_day = date_range_array[1];
        $.ajax({
            url: '<?php echo base_url("Dashboard/operation_comparision_by_date_ajax") ?>',
            method:"POST",
            data:{
                from_this_day : from_this_day, 
                to_this_day : to_this_day,
                csrf_test_name: $.cookie('csrf_cookie_name')
            },
            success:function(response) {
              response = JSON.parse(response);
              //BAR CHART
              var bar = new Morris.Bar({
                element: 'operational_comparision',
                resize: true,
                data: [
                  {y: 'Pembelian ', a: response.purchase_sum.purchase_sum},
                  {y: 'Piutang Supplier ', a: response.utang.utang},
                  {y: 'Penjualan', a: response.sale_sum.sale_sum},
                  {y: 'Limbah', a: response.waste_sum.waste_sum},
                  {y: 'Operasional', a: response.expense_sum.expense_sum},
                  {y: 'Cust Rcv', a: response.sale_sum.sale_sum},
                  {y: 'Supp Pay', a: response.supplier_due_payment_sum.supplier_due_payment_sum} 
                ],
                barColors: ['#3498db', '#f56954'],
                xkey: 'y',
                ykeys: ['a'],
                labels: ['Jumlah'],
                hideHover: 'auto'
              });
              
            },
            error:function(){
                alert("error");
            }
        });
        $('#operation_comparision_range_fields').hide();
      });
  });

function  selectMonth(value) {
        $.ajax({
            url: '<?= base_url() ?>Dashboard/comparison_sale_report_ajax_get',
            type: 'get',
            datatype: 'json',
            data: {months: value},
            success: function (response) {
                var json = $.parseJSON(response);
                google.charts.load('current', {'packages':['corechart', 'bar']});
                google.charts.setOnLoadCallback(drawStuff);
                function drawStuff() {
                    var chartDiv = document.getElementById('chart_div');

                    var data = '';
                    var dataArray = [];
                    var dataArrayValue = [];
                    dataArrayValue = [];
                    dataArrayValue.push('');
                    dataArrayValue.push('');
                    dataArray.push(dataArrayValue);

                    $.each(json, function (i, v) {
                        window['monthName'+i]=v.month;
                        window['collection'+i]=v.saleAmount;
                        dataArrayValue = [];
                        dataArrayValue.push(v.month);
                        dataArrayValue.push(v.saleAmount);
                        dataArray.push(dataArrayValue);
                    });
                    data = google.visualization.arrayToDataTable(dataArray);
                    var options = {
                        legend: { position: "none" },
                        colors: ['#2ecc71', '#2ecc71', '#2ecc71'],
                        axes: {
                            y: {
                                all: {
                                    format: {
                                        pattern: 'decimal'
                                    }
                                }
                            }
                        },
                        series: {
                            0: { axis: '0' }
                        }
                    };

                    function drawMaterialChart() {
                        var materialChart = new google.charts.Bar(chartDiv);
                        materialChart.draw(data,options);
                    }
                    function drawClassicChart() {
                        var classicChart = new google.visualization.ColumnChart(chartDiv);
                        classicChart.draw(data, classicOptions);

                    }
                    drawMaterialChart();
                }
            }
        });

    }
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/POS/js/jquery.cookie.js"></script>
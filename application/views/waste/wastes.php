 
<script type="text/javascript">  
    var ingredient_id_container = [];


    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    })
</script>
<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                    <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Limbah <small>(Bahan atau Menu Kadaluarsa)</small></li>
                      </ol> -->
                      <a href="<?php echo base_url() ?>Waste/addEditWaste"><button type="button" class="btn btn-block btn-primary pull-right mb-3 mt-3">Tambah Data</button></a>
                  </div>
                  <h4 class="page-title">Limbah <small>(Bahan atau Menu Kadaluarsa)</small></h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php
                        if ($this->session->flashdata('exception')) {

                            echo '<section class="content-header"><div class="alert alert-success alert-dismissible"> 
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-check"></i>';
                            echo $this->session->flashdata('exception');
                            echo '</p></div></section>';
                        } 
                    ?>
<div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 11%"><?php echo lang('ref_no'); ?></th>
                                <th style="width: 10%"><?php echo lang('date'); ?></th>
                                <th style="width: 15%"><?php echo lang('total_loss'); ?></th>
                                <th style="width: 10%">Jumlah</th>
                                <th style="width: 15%"><?php echo lang('responsible_person'); ?></th>
                                <th style="width: 20%" style="width: 22%;"><?php echo lang('note'); ?></th>
                                <th style="width: 12%">Unit</th>
                                <th style="width: 10%"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($wastes && !empty($wastes)) {
                                $i = count($wastes);
                            }
                            foreach ($wastes as $wsts) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $wsts->reference_no; ?></td>
                                    <td><?php echo date($this->session->userdata('date_format'), strtotime($wsts->date)); ?></td>
                                    <td><?php echo $this->session->userdata('currency') . " " . number_format($wsts->total_loss,0) ?></td>
                                    <td style="text-align: center;"><?php echo ingredientCount($wsts->id); ?></td>
                                    <td><?php echo employeeName($wsts->employee_id); ?></td>  
                                    <td><?php echo $wsts->note; ?></td>  
                                    <td><?php echo getRestoNameById($wsts->menu_resto_id); ?></td>

                                    <td style="text-align: center">
                                        <div class="">
                                        <a href="<?php echo base_url() ?>Waste/wasteDetails/<?php echo $this->custom->encrypt_decrypt($wsts->id, 'encrypt'); ?>" >
                                                <i class="fas fa-info mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Waste/deleteWaste/<?php echo $this->custom->encrypt_decrypt($wsts->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>  

                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>
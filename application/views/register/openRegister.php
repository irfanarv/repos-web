<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Buka Transaksi</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
        <?php if ($this->session->flashdata('exception_3')) {

            echo '<div class="alert alert-danger mb-0 bg-danger text-white" role="alert">';
            echo $this->session->flashdata('exception_3');
            echo '</div>';
            
        }
        ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="mt-0">Klik Tombol dibawah ini untuk memulai transaksi</h5>
                    <?php echo form_open(base_url('Register/addBalance')); ?>
                    <form>
                        <!-- <div class="form-group">
                            <input tabindex="1" type="text" name="opening_balance" class="form-control" placeholder="<?php echo lang('opening_balance'); ?>" value="<?php echo set_value('opening_balance'); ?>">
                        </div> -->
                            <!-- <?php if (form_error('opening_balance')) { ?>
                                <div class="alert alert-danger mb-0 bg-danger text-white" role="alert" style="padding: 5px !important;">
                                    <p><?php echo form_error('opening_balance'); ?></p>
                                </div><br>
                            <?php } ?> -->
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-primary px-4">Mulai Transaksi</button>
                    </form>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

</div>


 
<script type="text/javascript">  
    var ingredient_id_container = [];


    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    })
</script>
<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                        <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Inventory/index">Inventory</a>
                          </li>
                          <li class="breadcrumb-item active">Inventory Adjustments</li>
                        </ol> -->
                        <a href="<?php echo base_url() ?>Inventory_adjustment/addEditInventoryAdjustment"><button type="button" class="btn btn-block btn-primary pull-right">Tambah data inventory adjustments</button></a><br>
                  </div>
                  <h4 class="page-title">Inventory Adjustments</h4>
              </div>
          </div>
    </div>
 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">SN</th>
                                <th style="width: 11%">RF</th>
                                <th style="width: 8%">Tanggal</th>
                                <th style="width: 13%">Jumlah Bahan</th>
                                <th style="width: 15%">Penanggung Jawab</th>
                                <th style="width: 21%">Catatan</th>
                                <th style="width: 6%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($inventory_adjustments && !empty($inventory_adjustments)) {
                                $i = count($inventory_adjustments);
                            }
                            foreach ($inventory_adjustments as $value) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $value->reference_no; ?></td>
                                    <td><?php echo date($this->session->userdata('date_format'), strtotime($value->date)); ?></td>
                                    <td style="text-align: center;"><?php echo ingredientCountConsumption($value->id); ?></td>
                                    <td><?php echo employeeName($value->employee_id); ?></td>  
                                    <td><?php echo $value->note; ?></td>  

                                    
                                    <td style="text-align: center">
                                        <div class="">
                                            <a href="<?php echo base_url() ?>Inventory_adjustment/inventoryAdjustmentDetails/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fas fa-info mr-2"></i>
                                            </a>
                                            <a href="<?php echo base_url() ?>Inventory_adjustment/addEditInventoryAdjustment/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fas fa-edit text-success mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Inventory_adjustment/deleteInventoryAdjustment/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>

                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>SN</th>
                                <th>RF</th>
                                <th>Tanggal</th>
                                <th>Jumlah Bahan</th>
                                <th>Penanggung Jawab</th> 
                                <th>Catatan</th>    
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>

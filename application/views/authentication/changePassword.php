<style type="text/css">
    .required_star{
        color: #dd4b39;
    } 
</style>
<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Ganti Password</h4>
              </div>
          </div>
    </div>
 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <?php
if ($this->session->flashdata('exception')) {

    echo '<section class="content-header"><div class="alert alert-success alert-dismissible"> 
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p><i class="icon fa fa-check"></i>';
    echo $this->session->flashdata('exception');
    echo '</p></div></section>';
}

if ($this->session->flashdata('exception_1')) {

    echo '<section class="content-header"><div class="alert alert-danger alert-dismissible"> 
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p><i class="icon fa fa-check"></i>';
    echo $this->session->flashdata('exception_1');
    echo '</p></div></section>';
}
?>  
<div class="box box-primary"> 
                <?php echo form_open(base_url('Authentication/changePassword')); ?>
                <div class="box-body">
                    <div class="row">


                        <div class="col-md-6"> 
                            <div class="form-group">
                                <label><?php echo lang('old_password'); ?> <span class="required_star">*</span></label>
                                <input tabindex="1" type="password" name="old_password" class="form-control" placeholder="<?php echo lang('old_password'); ?>">
                            </div>
                            <?php if (form_error('old_password')) { ?>
                                <div class="alert alert-danger" style="padding: 5px !important;">
                                    <p><?php echo form_error('old_password'); ?></p>
                                </div>
                            <?php } ?>  
                        </div>

                        <div class="col-md-6">  
                            <div class="form-group">
                                <label><?php echo lang('new_password'); ?> <span class="required_star">*</span></label>
                                <input tabindex="2" type="password" name="new_password" class="form-control" placeholder="<?php echo lang('new_password'); ?>">
                            </div>
                            <?php if (form_error('new_password')) { ?>
                                <div class="alert alert-danger" style="padding: 5px !important;">
                                    <p><?php echo form_error('new_password'); ?></p>
                                </div>
                            <?php } ?>  
                        </div> 
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name="submit" value="submit" class="btn btn-primary"><?php echo lang('submit'); ?></button>
                </div>
                <?php echo form_close(); ?>
            </div>
                </div>
            </div>
        </div>
    </div>


</div>

<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            #loading {
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            display: block;
            opacity: 0.7;
            background-color: #fff;
            z-index: 99;
            text-align: center;
            }

            #loading-image {
            position: absolute;
            top: 230px;
            left: 800px;
            z-index: 100;
            }
        </style>
        <meta charset="utf-8">
        <title>RePOS Cafe</title>
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
        <link href="<?php echo base_url(); ?>assets/new_ui/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/new_ui/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/new_ui/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/new_ui/css/pace2.css" rel="stylesheet" type="text/css">

        <script src="<?php echo base_url(); ?>assets/new_ui/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/js/waves.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/js/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/js/pace.min.js"></script>
        <script>
            jQuery(document).ready(function($) {
                $(document).on('keydown', '.integerchk', function(e){ 
                    return (
                    keys == 8 ||
                        keys == 9 ||
                        keys == 13 ||
                        keys == 46 ||
                        keys == 110 ||
                        keys == 86 ||
                        keys == 190 ||
                        (keys >= 35 && keys <= 40) ||
                        (keys >= 48 && keys <= 57) ||
                        (keys >= 96 && keys <= 105));
                });

                $(document).on('keyup', '.integerchk', function(e){
                    var input = $(this).val();
                    var ponto = input.split('.').length;
                    var slash = input.split('-').length;
                    if (ponto > 2)
                        $(this).val(input.substr(0,(input.length)-1));
                    $(this).val(input.replace(/[^0-9]/,''));
                    if(slash > 2)
                        $(this).val(input.substr(0,(input.length)-1));
                    if (ponto ==2)
                        $(this).val(input.substr(0,(input.indexOf('.')+3)));
                    if(input == '.')
                        $(this).val("");

                });

            });
        </script>
        
    </head>
    <div id="loading">
        <img id="loading-image" src="<?php echo base_url(); ?>assets/iconss.gif" alt="Loading..." />
    </div>
    <body class="bg-white">
        <!-- Log In page -->
        <div class="row">
            <div class="col-lg-3 pr-0">
                <div class="card mb-0 shadow-none">
                    <div class="card-body">
                        <h3 class="text-center m-0">
                            <a href="#" class="logo logo-admin"><img src="<?php echo base_url(); ?>assets/images/icon.png" height="120" class="my-3"></a>
                        </h3>
                        <div class="px-3">
                            <!-- <h4 class="text-muted font-18 mb-2 text-center">Bafageh, Resto, Bakery & Cafe</h4>
                            <p class="text-muted text-center">Silahkan Login</p> -->
                            <?php
                            if ($this->session->flashdata('exception_1')) {
                                echo '<div class="alert alert-danger bg-danger text-white alert-dismissible"> 
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-check"></i>';
                                echo $this->session->flashdata('exception_1');
                                echo '</p></div>';
                            }
                            ?>
                            <?php echo form_open(base_url('Authentication/loginCheck')); ?>
                            <form class="form-horizontal my-4" >
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-envelope"></i></span></div><input
                                                    type="text"
                                                    class="form-control"
                                                    name="email_address"
                                                    placeholder="Masukan Email Kamu"></div>
                                </div>
                                    <?php if (form_error('email_address')) { ?>
                                    <div class="alert alert-danger bg-danger text-white"> 
                                        <?php echo form_error('email_address'); ?>
                                    </div>
                                    <?php } ?>
                                <div class="form-group">
                                    <label for="userpassword">Password</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-key"></i></span></div><input
                                                    type="password"
                                                    class="form-control"
                                                    name="password"
                                                    placeholder="Masukan Password"></div>
                                </div>
                                <?php if (form_error('password')) { ?>
                                    <div class="alert alert-danger bg-danger text-white"> 
                                        <p><?php echo form_error('password'); ?></p>
                                    </div>
                                <?php } ?>
                                
                                <div class="form-group mb-0 row">
                                    <div class="col-12 mt-2">
                                        <button
                                            class="btn btn-primary btn-block waves-effect waves-light"
                                            type="submit"
                                            name="submit" value="submit" id="klik"
                                            >Masuk
                                            <i class="fas fa-sign-in-alt ml-1"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <?php echo form_close(); ?>
                        </div>
                        
                        <div class="mt-4 text-center">
                            <p class="mb-0">© <?php echo date("Y")?> <a href="https://www.repos.gomaya.id">RePOS</a>
                            <br> Crafted with
                                <i class="mdi mdi-heart text-danger"></i>
                                by <a href="https://www.gomaya.id">gomaya.id</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 p-0 h-100vh d-flex justify-content-center">
                <div class="accountbg d-flex align-items-center">
                    <div class="account-title text-center text-white">
                        <h4 class="mt-3">Selamat datang di
                            <span class="text-warning">Aplikasi</span></h4>
                        <h1 class="">RePOS Cafe</h1>
                        <p class="font-14 mt-3">Dirancang khusus untuk cafe anda
                            </p>
                        <div class="border w-25 mx-auto border-warning"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            $(document).ready(function() {
                $("#loading").hide();
            });
            $("#klik").click(function(){
                $("#loading").show();
            }); 
        </script>
    </body>
</html>
<?php 
    
    $show_register_report = "";
    if(isset($register_info) && count($register_info)>0){
        
        $i = 1;
        foreach($register_info as $single_register_info){
            $payment_methods_sale = json_decode($single_register_info->payment_methods_sale);
            $cash = is_null($payment_methods_sale)?lang('register_cash').' 0.00':lang('register_cash').$payment_methods_sale->Cash;
            $paypal = is_null($payment_methods_sale)?lang('register_paypal').'0.00':lang('register_paypal').$payment_methods_sale->Paypal;
            $card = is_null($payment_methods_sale)?lang('register_card').'0.00':lang('register_card').$payment_methods_sale->Card;
            $show_register_report .= "<tr>";
            $show_register_report .= '<td>'.$i.'</td>';
            $show_register_report .= '<td>'.date('d M Y', strtotime($single_register_info->opening_balance_date_time)).'</td>';
            $show_register_report .= '<td>Jam '.date('H:i (d M Y)', strtotime($single_register_info->opening_balance_date_time)).'</td>';
            
            if ($single_register_info->closing_balance_date_time == "0000-00-00 00:00:00"){
                $show_register_report .= '<td>Transaksi belum ditutup</td>';
            }else{
                $show_register_report .= '<td>Jam '.date('H:i (d M Y)', strtotime($single_register_info->closing_balance_date_time)).'</td>';
            }
            
           
            $show_register_report .= '<td>Rp. '.number_format($single_register_info->sale_paid_amount,0,',','.').'</td>';
           
            
            $show_register_report .= "</tr>";        
            $i++;
        }
    }
    $user_option = '';
    foreach($users as $single_user){
        $user_option .= '<option value="'.$single_user->id.'">'.$single_user->full_name.'</option>';
    }
    
?>
<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Rekap Buka Tutup Transaksi</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="mt-0">Cari menurut tanggal yang diinginkan</h5>
                    <hr style="border: 1px solid #3c8dbc;">
                    <div class="row"> 
                        <div class="col-md-2">
                            <?php echo form_open(base_url() . 'Report/registerReport') ?>
                            <div class="form-group"> 
                                <input tabindex="1" type="text" id="" name="startDate" readonly class="form-control customDatepicker" placeholder="<?php echo lang('start_date'); ?>" value="<?php echo set_value('startDate'); ?>">
                            </div> 
                        </div>
                        <div class="col-md-2">

                            <div class="form-group">
                                <input tabindex="2" type="text" id="endMonth" name="endDate" readonly class="form-control customDatepicker" placeholder="<?php echo lang('end_date'); ?>" value="<?php echo set_value('endDate'); ?>">
                            </div>
                        </div>
                        <!-- <div class="col-md-2">

                            <div class="form-group">
                                <select tabindex="2" class="form-control select2" id="user_id" name="user_id" style="width: 100%;">
                                    <option value="<?= $this->session->userdata['user_id']; ?>"><?= $this->session->userdata['full_name']; ?></option>

                                </select>
                            </div>
                        </div> -->
                        <div class="col-md-1">
                            <div class="form-group">
                                <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left"><?php echo lang('submit'); ?></button>
                            </div>
                        </div>
                        <div class="hidden-lg">
                            <div class="clearfix"></div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="mt-0"><?php echo lang('register_report'); ?></h5>
                    <div class="box box-primary"> 
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            
                            <h5 class="mt-0"><?= isset($start_date) && $start_date && isset($end_date) && $end_date ? "Tanggal " . date($this->session->userdata('date_format'), strtotime($start_date)) . " - " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?><?= isset($start_date) && $start_date && !$end_date ? "Tanggal " . date($this->session->userdata('date_format'), strtotime($start_date)) : '' ?><?= isset($end_date) && $end_date && !$start_date ? "Tanggal " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?></h5>
                            <br>
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="title" style="width: 5%">No</th>
                                        <th class="title" style="width: 10%">Tanggal Buka</th>
                                        <th class="title" style="width: 10%">Waktu Buka</th>
                                        <th class="title" style="width: 10%">Waktu Tutup </th>
                                        <th class="title" style="width: 20%">Total Pendapatan </th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    echo $show_register_report;
                                    ?>
                                </tbody>
                                <!-- <tfoot>
                                    <tr>
                                        <th style="width: 2%;text-align: center"></th>
                                        <th style="text-align: right">Total </th>
                                        <th><?= number_format($grandTotal) ?></th>
                                    </tr>
                                </tfoot> -->
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div> 
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready( function () {
    $('#datatable').DataTable();
    } );
</script>
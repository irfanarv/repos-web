<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Rekap Operasional</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <h6 class="page-title">Masukan tanggal untuk mencari</h6>
                <div class="row">
        <div class="col-md-2">
            <?php echo form_open(base_url() . 'Report/expenseReport') ?>
            <div class="form-group">
                <input tabindex="1" type="text" id="" name="startDate" readonly class="form-control customDatepicker" placeholder="<?php echo lang('start_date'); ?>" value="<?php echo set_value('startDate'); ?>">
            </div>
        </div>
        <div class="col-md-2">

            <div class="form-group">
                <input tabindex="2" type="text" id="endMonth" name="endDate" readonly class="form-control customDatepicker" placeholder="<?php echo lang('end_date'); ?>" value="<?php echo set_value('endDate'); ?>">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <select tabindex="2" class="form-control select2" id="expense_item_id" name="expense_item_id" style="width: 100%;">
                    <option value="">Kategori Operasional</option>
                    <?php
                    foreach ($expense_items as $value) {
                        ?>
                        <option value="<?php echo $value->id ?>" <?php echo set_select('expense_item_id', $value->id); ?>><?php echo $value->name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <select tabindex="2" class="form-control select2" id="menu_resto_id" name="menu_resto_id" style="width: 100%;" required>
                    <option value="">Unit</option>
                    <?php
                    foreach ($menu_resto as $value) {
                        ?>
                        <option value="<?php echo $value->id ?>" <?php echo set_select('menu_resto_id', $value->id); ?>><?php echo $value->category_name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left"><?php echo lang('submit'); ?></button>
            </div>
        </div>
        <div class="hidden-lg">
            <div class="clearfix"></div>
        </div> 
    </div>
                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
    h1,h2,h3,h4,p{
        margin:3px 0px;
        text-align: center;
    }

    .tbl  {
        border-collapse:collapse;
        border-spacing:0;
        width: 100%;

    }
    .tbl tr td{
        padding:14px;
        font-family:Arial, sans-serif;
        font-size:15px;
        border-style:solid;
        border-width:1px;
        word-break:break-all;
    }

    .title{
        font-weight: bold;
    }
    .box-primary{
        border-top-color: white !important;
        margin-top: 5px;
    }
</style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <h3>Rekap Operasional</h3>
                    <h4><?= isset($start_date) && $start_date && isset($end_date) && $end_date ? lang('report_date') . date($this->session->userdata('date_format'), strtotime($start_date)) . " - " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?><?= isset($start_date) && $start_date && !$end_date ? lang('report_date') . date($this->session->userdata('date_format'), strtotime($start_date)) : '' ?><?= isset($end_date) && $end_date && !$start_date ? lang('report_date') . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?></h4>
                    
                    <br>
                    <table id="datatable"  class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th><?php echo lang('date'); ?></th>
                                <th>Jumlah</th>
                                <th>Kategori</th>
                                <th>Unit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $grandTotal = 0;
                            $countTotal = 0;
                            if (isset($expenseReport)):
                                foreach ($expenseReport as $key => $value) {
                                    $grandTotal+=$value->amount;
                                    $key++;
                                    ?>
                                    <tr>
                                        <td style="text-align: center"><?php echo $key; ?></td>
                                        <td><?= date($this->session->userdata('date_format'), strtotime($value->date)) ?></td>
                                        <td>Rp. <?php echo number_format($value->amount,0) ?></td>
                                        <td><?php echo $value->categoryName ?></td>
                                        <td><?php echo getRestoNameById($value->menu_resto_id); ?></td>
                                    </tr>
                                    <?php
                                }
                            endif;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="width: 2%;text-align: center"></th>
                                <th style="text-align: right"><?php echo lang('total'); ?> </th>
                                <th>Rp. <?= number_format($grandTotal, 0) ?></th>
                                <th></th>
                                <th></th>
                                
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script src="<?php echo base_url(); ?>assets/new_ui/js/jquery.min.js"></script>
<script>  
    var jqry = $.noConflict();
    jqry(document).ready(function(){

    var TITLE = "Rekap Operasional "+today; 

    jqry('#datatable').DataTable( {
        'autoWidth'   : false,
        'ordering'    : false,
        dom: 'Bfrtip',
        buttons: [ 
            {
                extend: 'print',
                title: TITLE
            },
            {
                extend: 'excelHtml5',
                title: TITLE
            },
            {
                extend: 'pdfHtml5',
                title: TITLE
            }
        ]
    } );
} );
</script> 
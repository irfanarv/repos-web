<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Rekap Pembelian Bahan Baku</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <h5 class="page-title">Masukan tanggal untuk mencari data</h5>
                <div class="row"> 
        <div class="col-md-2">
            <?php echo form_open(base_url() . 'Report/purchaseReportByDate') ?>
            <div class="form-group"> 
                <input tabindex="1" type="text" id="" name="startDate" readonly class="form-control customDatepicker" placeholder="<?php echo lang('start_date'); ?>" value="<?php echo set_value('startDate'); ?>" required>
            </div> 
        </div>
        <div class="col-md-2">

            <div class="form-group">
                <input tabindex="2" type="text" id="endMonth" name="endDate" readonly class="form-control customDatepicker" placeholder="<?php echo lang('end_date'); ?>" value="<?php echo set_value('endDate'); ?>" required>
            </div>
        </div>

        <div class="col-md-2"> 

            <div class="form-group">
            <select name="category_id" class="form-control select2" required>
                <option value="">Pilih Unit</option>
                <?php foreach ($menu_resto as $resto) { ?>
                    <option value="<?php echo $resto->id ?>" <?php echo set_select('menu_resto_id', $resto->id); ?>><?php echo $resto->category_name ?></option>
                <?php } ?>
            </select>
            </div>
        </div>

        <div class="col-md-1">
            <div class="form-group">
                <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left"><?php echo lang('submit'); ?></button>
            </div>
        </div>
        <div class="hidden-lg">
            <div class="clearfix"></div>
        </div> 
    </div> 
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
    h1,h2,h3,h4,p{
        margin:3px 0px;
        text-align: center;
    }

    .tbl  {
        border-collapse:collapse;
        border-spacing:0;
        width: 100%;

    }
    .tbl tr td{
        padding:14px;
        font-family:Arial, sans-serif;
        font-size:15px;
        border-style:solid;
        border-width:1px;
        word-break:break-all;
    }

    .title{
        font-weight: bold;
    }
    .box-primary{
        border-top-color: white !important;
        margin-top: 5px;
    }
</style> 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <h5>Rekap Pembelian Bahan Baku</h5>
                    <h6><?= isset($start_date) && $start_date && isset($end_date) && $end_date ? lang('report_date') . date($this->session->userdata('date_format'), strtotime($start_date)) . " - " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?><?= isset($start_date) && $start_date && !$end_date ? lang('report_date') . date($this->session->userdata('date_format'), strtotime($start_date)) : '' ?><?= isset($end_date) && $end_date && !$start_date ? lang('report_date') . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?></h6>
                    <br>
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 2%;text-align: center;">No</th>
                                <th style="width: 10%;"><?php echo lang('ref_no'); ?></th>
                                <th style="width: 5%;"><?php echo lang('date'); ?></th>
                                <th style="width: 10%;"><?php echo lang('supplier'); ?></th>
                                <th style="width: 12%;">Total</th>
                                <th style="width: 7%;">Lunas</th>
                                <th style="width: 7%;">Sisa</th>
                                <th style="width: 32%;">Bahan Baku</th>
                                <th style="width: 15%;">Dibeli</th> 
                                <th style="width: 15%;">Unit</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sum_of_grand_total = 0;
                            $sum_of_paid = 0;
                            $sum_of_due = 0;

                            if (isset($purchaseReportByDate)):
                                foreach ($purchaseReportByDate as $key => $value) { 
                                    $sum_of_grand_total += $value->grand_total;
                                    $sum_of_paid += $value->paid;
                                    $sum_of_due += $value->due;
                                    $key++;
                                    ?>
                                    <tr>
                                        <td style="text-align: center"><?php echo $key; ?></td>
                                        <td><?php echo $value->reference_no; ?></td> 
                                        <td><?= date($this->session->userdata('date_format'), strtotime($value->date)) ?></td>
                                        <td><?php echo getSupplierNameById($value->supplier_id); ?></td> 
                                        <td>Rp. <?php echo number_format($value->grand_total,0) ?></td>
                                        <td>Rp. <?php echo number_format($value->paid,0) ?></td>
                                        <td>Rp. <?php echo number_format($value->due,0) ?></td>
                                        <td><?php print_r(getPurchaseIngredients($value->id)) ?></td>  
                                        <td><?php echo userName($value->user_id) ?></td>
                                        <td><?php echo getRestoNameById($value->menu_resto_id) ?></td>
                                    </tr>
                                    <?php
                                }
                            endif;
                            ?>
                          <tr> 
                                <td>&nbsp;</td> 
                                <td>&nbsp;</td> 
                                <td>&nbsp;</td>  
                                <td style="text-align: right"><?php echo lang('total'); ?> </td>
                                <td>Rp. <?= number_format($sum_of_grand_total, 0) ?></td>
                                <td>Rp. <?= number_format($sum_of_paid, 0) ?></td>
                                <td>Rp. <?= number_format($sum_of_due, 0) ?></td> 
                                <td>&nbsp;</td>  
                                <td>&nbsp;</td>  
                                <td>&nbsp;</td>  
                            </tr> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>
<script src="<?php echo base_url(); ?>assets/new_ui/js/jquery.min.js"></script>

<script>  
    var jqry = $.noConflict();
    jqry(document).ready(function(){

    var TITLE = "<?php echo lang('purchase_report'); ?> "+today; 

    jqry('#datatable').DataTable( {
        'autoWidth'   : false,
        'ordering'    : false,
        dom: 'Bfrtip',
        buttons: [ 
            {
                extend: 'print',
                title: TITLE
            },
            {
                extend: 'excelHtml5',
                title: TITLE
            },
            {
                extend: 'pdfHtml5',
                title: TITLE
            }
        ]
    } );
} );
</script>


<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Detail Rekap Penjualan</h4>
              </div> 
          </div>
    </div>

    

    <div class="row"> 
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <h5  style="text-align: left;margin-top: 0px">Masukan tanggal untuk mencari data</h5>
    <hr style="border: 1px solid #3c8dbc;">
    <div class="row">
        <div class="col-md-2">
            <?php echo form_open(base_url() . 'Report/detailedSaleReport') ?>
            <div class="form-group">
                <input tabindex="1" type="text" id="" name="startDate" readonly class="form-control customDatepicker" placeholder="Start Date" value="<?php echo set_value('startDate'); ?>">
            </div>
        </div>
        <div class="col-md-2">

            <div class="form-group">
                <input tabindex="2" type="text" id="endMonth" name="endDate" readonly class="form-control customDatepicker" placeholder="End Date" value="<?php echo set_value('endDate'); ?>">
            </div>
        </div>
        <!-- <div class="col-md-2">

            <div class="form-group">
                <select tabindex="2" class="form-control select2" id="user_id" name="user_id" style="width: 100%;">
                    <option value="">User</option>
                    <option value="<?= $this->session->userdata['user_id']; ?>"><?= $this->session->userdata['full_name']; ?></option>
                    <?php
                    foreach ($users as $value) {
                        ?>
                        <option value="<?php echo $value->id ?>" <?php echo set_select('user_id', $value->id); ?>><?php echo $value->full_name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div> -->
        <div class="col-md-1">
            <div class="form-group">
                <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left">Submit</button>
            </div>
        </div>
        <div class="hidden-lg">
            <div class="clearfix"></div>
        </div> 
    </div>
                </div>
            </div>
        </div>
    </div>


    <style type="text/css">
    h1,h2,h3,h4,p{
        margin:3px 0px;
        text-align: center;
    }

    .tbl  {
        border-collapse:collapse;
        border-spacing:0;
        width: 100%;

    }
    .tbl tr td{
        padding:14px;
        font-family:Arial, sans-serif;
        font-size:15px;
        border-style:solid;
        border-width:1px;
        word-break:break-all;
    }

    .title{
        font-weight: bold;
    }
    .box-primary{
        border-top-color: white !important;
        margin-top: 5px;
    }
</style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <h3>Detail Rekap Penjualan</h3>
                    <h4><?= isset($start_date) && $start_date && isset($end_date) && $end_date ? "Tanggal: " . date($this->session->userdata('date_format'), strtotime($start_date)) . " - " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?><?= isset($start_date) && $start_date && !$end_date ? "Tanggal: " . date($this->session->userdata('date_format'), strtotime($start_date)) : '' ?><?= isset($end_date) && $end_date && !$start_date ? "Tanggal: " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?></h4>
                    
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 2%;text-align: center">SN</th>
                                <th>Tanggal</th>
                                <th>RF</th>
                                <th>Total Items</th>
                                <th>Subtotal</th>
                                <th>Pajak</th>
                                <!-- <th>PPN</th> -->
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pGrandTotal = 0;
                            $subGrandTotal = 0;
                            $itemsGrandTotal = 0;
                            $disGrandTotal = 0;
                            $vatGrandTotal = 0;
                            if (isset($detailedSaleReport)):
                                foreach ($detailedSaleReport as $key => $value) {
                                    $pGrandTotal+=$value->total_payable;
                                    $subGrandTotal+=$value->sub_total; 
                                    $itemsGrandTotal+=$value->total_items;
                                    $disGrandTotal+=$value->total_discount_amount;
                                    $vatGrandTotal+=$value->vat;
                                    $key++;
                                    ?>
                                    <tr>
                                        <td style="text-align: center"><?php echo $key; ?></td>
                                        <td><?= date($this->session->userdata('date_format'), strtotime($value->sale_date)) ?></td>
                                        <td><?php echo $value->sale_no ?></td>
                                        <td><?php echo $value->total_items ?></td>
                                        <td>Rp. <?php echo number_format($value->sub_total,0) ?></td>
                                        <td>Rp. <?php echo number_format($value->total_discount_amount,0) ?></td>
                                        <!-- <td>Rp. <?php echo number_format($value->vat,0) ?></td> -->
                                        <td>Rp. <?php echo number_format($value->total_payable,0) ?></td>
                                        
                                    </tr>
                                    <?php
                                }
                            endif;
                            ?>
                        </tbody>
                        <tfoot>
                        <th style="width: 2%;text-align: center"></th>
                        <th></th>
                        <th style="text-align: right">Total </th>
                        <th><?= $itemsGrandTotal ?></th>
                        <th>Rp. <?= number_format($subGrandTotal, 0) ?></th>

                        <th>Rp. <?= number_format($disGrandTotal, 0) ?></th>
                        <!-- <th>Rp. <?= number_format($vatGrandTotal, 0) ?></th> -->
                        <th>Rp. <?= number_format($pGrandTotal, 0) ?></th>
                        </tfoot>
                    </table>
                    <hr><br>
                    <h3>Detail Rekap Compliment</h3>
                    <h4><?= isset($start_date) && $start_date && isset($end_date) && $end_date ? "Tanggal: " . date($this->session->userdata('date_format'), strtotime($start_date)) . " - " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?><?= isset($start_date) && $start_date && !$end_date ? "Tanggal: " . date($this->session->userdata('date_format'), strtotime($start_date)) : '' ?><?= isset($end_date) && $end_date && !$start_date ? "Tanggal: " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?></h4>
                    
                    <table id="datatable2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 2%;text-align: center">SN</th>
                                <th>Tanggal</th>
                                <th>RF</th>
                                <th>Total Items</th>
                                <th>Total</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pGrandTotal = 0;
                            $subGrandTotal = 0;
                            $itemsGrandTotal = 0;
                            $disGrandTotal = 0;
                            $vatGrandTotal = 0;
                            if (isset($compliment)):
                                foreach ($compliment as $key => $value) {
                                    $pGrandTotal+=$value->total_payable;
                                    $subGrandTotal+=$value->sub_total; 
                                    $itemsGrandTotal+=$value->total_items;
                                    $disGrandTotal+=$value->total_discount_amount;
                                    $vatGrandTotal+=$value->vat;
                                    $key++;
                                    ?>
                                    <tr>
                                        <td style="text-align: center"><?php echo $key; ?></td>
                                        <td><?= date($this->session->userdata('date_format'), strtotime($value->sale_date)) ?></td>
                                        <td><?php echo $value->sale_no ?></td>
                                        <td><?php echo $value->total_items ?></td>
                                        <td>Rp. <?php echo number_format($value->sub_total,0) ?></td>
                                        
                                        
                                    </tr>
                                    <?php
                                }
                            endif;
                            ?>
                        </tbody>
                        <tfoot>
                        <th style="width: 2%;text-align: center"></th>
                        <th></th>
                        <th style="text-align: right">Total </th>
                        <th><?= $itemsGrandTotal ?></th>
                        <th>Rp. <?= number_format($subGrandTotal, 0) ?></th>

                        
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="<?php echo base_url(); ?>assets/new_ui/js/jquery.min.js"></script>
<script>  
    var jqry = $.noConflict();
    jqry(document).ready(function(){

    var TITLE = "Detail Rekap Penjualan "; 
    var TITLE2 = "Detail Rekap Compliment "; 

    jqry('#datatable').DataTable( {
        'autoWidth'   : false,
        'ordering'    : false,
        dom: 'Bfrtip',
        buttons: [ 
            {
                extend: 'print',
                title: TITLE
            },
            {
                extend: 'excelHtml5',
                title: TITLE
            },
            {
                extend: 'pdfHtml5',
                title: TITLE
            }
        ]
    } );

    jqry('#datatable2').DataTable( {
        'autoWidth'   : false,
        'ordering'    : false,
        dom: 'Bfrtip',
        buttons: [ 
            {
                extend: 'print',
                title: TITLE2
            },
            {
                extend: 'excelHtml5',
                title: TITLE2
            },
            {
                extend: 'pdfHtml5',
                title: TITLE2
            }
        ]
    } );
} );
</script> 
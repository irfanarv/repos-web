<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                    <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Inventory/index">Inventory</a>
                          </li>
                          <li class="breadcrumb-item active">Rekap Data</li>
                      </ol>
                  </div>
                  <h4 class="page-title">Rekap Data Inventory</h4>
              </div>
          </div>
    </div>

    <div class="row">
          <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <?php echo form_open(base_url() . 'Inventory/index') ?>
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="hiddentIngredientID" id="hiddentIngredientID" value="<?= isset($ingredient_id) ? $ingredient_id : '' ?>">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group"> 
                                            <select class="form-control select2 category_id" name="category_id" id="category_id" style="width: 100%;">
                                                <option value="">Unit</option>
                                                <?php foreach ($ingredient_categories as $value) { ?>
                                                    <option value="<?php echo $value->id ?>" <?php echo set_select('category_id', $value->id); ?>><?php echo $value->category_name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div> 
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group"> 
                                            <select class="form-control select2" name="ingredient_id" id="ingredient_id" style="width: 100%;">
                                                <option value=""><?php echo lang('ingredient'); ?></option>
                                                <?php foreach ($ingredients as $value) { ?>
                                                    <option value="<?php echo $value->id ?>" <?php echo set_select('ingredient_id', $value->id); ?>><?php echo $value->name . "(" . $value->code . ")" ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <select class="form-control select2" name="food_id" id="food_id" style="width: 100%;">
                                                <option value=""><?php echo lang('food_menu'); ?></option>
                                                <?php foreach ($foodMenus as $value) { ?>
                                                    <option value="<?php echo $value->id ?>" <?php echo set_select('food_id', $value->id); ?>><?php echo substr(ucwords(strtolower($value->name)), 0, 18) . "(" . $value->code . ")" ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left">Cari</button>
                                    </div>
                                    <div class="col-sm-3">
                                        
                                            <a href="<?= base_url() . 'Inventory/getInventoryAlertList' ?>" class="btn btn-block btn-primary pull-left"><span style="color:red"><?= getAlertCount() ?></span> Bahan minim stock </a>
                                        
                                    </div>
                                    <div class="col-sm-4">
                                        <strong id="stockValue"></strong>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
          </div>
    </div>
 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                </div>
            </div>
        </div>
    </div>


</div>

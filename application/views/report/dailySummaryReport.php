<style type="text/css">
    h1,h2,h3,h4,p{
        margin:3px 0px;
        text-align: center;
    }

    .tbl  {
        border-collapse:collapse;
        border-spacing:0;
        width: 100%;

    }
    .tbl tr td{
        padding:5px;
        font-family:Arial, sans-serif;
        font-size:15px;
        border-style:solid; 
        border-width:1px;
        word-break:break-all;
    }
    .tbl tr th{
        padding:14px;
        font-family:Arial, sans-serif;
        font-size:15px;
        border-style:solid;
        border-width:1px;
        word-break:break-all;
    }

    .title{
        font-weight: bold;
    }
    .box-primary{
        border-top-color: white !important;
        margin-top: 5px;
    }

</style> 
<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Laporan <?php echo $this->session->userdata('outlet_name');?></h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <h5 class="mt-0">Cari menurut tanggal yang diinginkan</h5>
                    <div class="row"> 
                        <div class="col-md-2">
                            <?php echo form_open(base_url() . 'Report/dailySummaryReport') ?>
                            <div class="form-group"> 
                                <input tabindex="1" type="text" id="" name="startDate" readonly class="form-control customDatepicker" placeholder="<?php echo lang('start_date'); ?>" value="<?php echo set_value('startDate'); ?>">
                            </div> 
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <input tabindex="2" type="text" id="endMonth" name="endDate" readonly class="form-control customDatepicker" placeholder="<?php echo lang('end_date'); ?>" value="<?php echo set_value('endDate'); ?>">
                            </div>
                        </div> 
                        
                        
                        <div class="col-md-1">
                            <div class="form-group">
                                <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left"><?php echo lang('submit'); ?></button>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <a class="btn btn-block btn-primary pull-left" href="<?php echo base_url(); ?>Report/printDailySummaryReport/<?php echo $start_date; ?>/<?php echo $end_date; ?>"><?php echo lang('print'); ?></a>
                            </div>
                        </div>
                        <div class="hidden-lg">
                            <div class="clearfix"></div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card"> 
                <div class="card-body">
                    <div class="box-body table-responsive" > 
                        <h1>Laporan</h1>
                        <h3 style="text-align: center;"><?php echo $this->session->userdata('outlet_name'); ?></h3>
                        <h4><?= isset($start_date) && $start_date && isset($end_date) && $end_date ? "Periode: " . date($this->session->userdata('date_format'), strtotime($start_date)) . " - " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?><?= isset($start_date) && $start_date && !$end_date ? "Periode: " . date($this->session->userdata('date_format'), strtotime($start_date)) : '' ?><?= isset($end_date) && $end_date && !$start_date ? "Periode : " . date($this->session->userdata('date_format'), strtotime($end_date)) : '' ?></h4> 
                        <hr>
                        <?php  
                                            
                                            $sum_pajak = 0;
                                            if (!empty($result['pajak']) && isset($result['pajak'])): foreach ($result['pajak'] as $key => $value): 
                                                
                                                
                                                $sum_pajak += $value->total_discount_amount; 
                                            ?>  
                                            <?php endforeach; endif;?>
                        <div class="container">
                            <h4 style="font-weight: bold; text-align: center; margin-top: 20px;">Pemasukan </h4>
                            <div class="row">
                                <div class="col-sm">
                                    <hr>
                                    <table style="width: 100%" class="tbl">    
                                        <tr>
                                            <th style="font-weight: bold; text-align: center;">No</th>
                                            <th>Unit</th>
                                            <th>Total </th>
                                        </tr> 
                                        <?php  
                                            $sum_menu_price_with_discount = 0;
                                            // $sum_grand = 0;
                                            // $sum_of_spaid_amount = 0; 
                                            // $sum_of_sdue_amount = 0; 
                                            if (!empty($result['sales']) && isset($result['sales'])):
                                                foreach ($result['sales'] as $key => $value): 
                                                    $sum_menu_price_with_discount += $value->menu_price_with_discount; 
                                                    
                                                    // $sum_of_spaid_amount += $value->paid_amount;  
                                                    // $sum_of_sdue_amount += $value->due_amount;  
                                                    $key++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center"><?= $key ?></td>
                                                        <td><?= getRestoNameById($value->category_id) ?></td>  
                                                        <!-- <td>Rp. <?= number_format($value->total_discount_amount,0); ?></td> -->
                                                        <td>Rp. <?= number_format($value->menu_price_with_discount,0); ?></td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                        ?>
                                        <tr>  
                                            <td>&nbsp;</td> 
                                            <td style="font-weight: bold; text-align: right;">Total Penjualan</td>  
                                            <td>Rp. <?= number_format($sum_menu_price_with_discount,0) ?></td>
                                        </tr>
                                        <?php if ($pajakaktif == 'Aktif') { ?> 
                                        <tr>  
                                            <td>&nbsp;</td> 
                                            <td style="font-weight: bold; text-align: right;">Tax & Services (Pajak)</td>
                                            <?php  
                                            
                                            $sum_pajak = 0;
                                            if (!empty($result['pajak']) && isset($result['pajak'])): foreach ($result['pajak'] as $key => $value): 
                                                
                                                
                                                $sum_pajak += $value->total_discount_amount; 
                                            ?>  
                                            <?php endforeach; endif;?>
                                            <td>Rp. <?= number_format($sum_pajak, 0) ?></td>
                                            
                                        </tr>
                                        <?php }?>
                                        <tr>  
                                            <td>&nbsp;</td> 
                                            <td style="font-weight: bold; text-align: right;">Total Pendapatan</td>  
                                            <td>Rp. <?= number_format($sum_menu_price_with_discount+$sum_pajak,0); ?></td>
                                        </tr>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                        <br>
                        <hr>
                        <!-- COMPLIMENT -->
                        <div class="container">
                            <h4 style="font-weight: bold; text-align: center; margin-top: 20px;">Compliment </h4>
                            <div class="row">
                                <div class="col-sm">
                                    <hr>
                                    <table style="width: 100%" class="tbl">    
                                        <tr>
                                            <th style="font-weight: bold; text-align: center;">No</th>
                                            <th>Unit</th>
                                            <th>Total </th>
                                        </tr> 
                                        <?php  
                                            $sum_menu_price_with_discount = 0;
                                            $sum_grand = 0;
                                            
                                            if (!empty($result['compliment']) && isset($result['compliment'])):
                                                foreach ($result['compliment'] as $key => $value): 
                                                    $sum_menu_price_with_discount += $value->menu_price_with_discount; 
                                                    $sum_grand += $value->total_discount_amount += $value->menu_price_with_discount;  
                                                    
                                                    $key++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center"><?= $key ?></td>
                                                        <td><?= getRestoNameById($value->category_id) ?></td>  
                                                        <td>Rp. <?= number_format($value->menu_price_with_discount,0); ?></td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                        ?>
                                        <tr>  
                                            <td>&nbsp;</td> 
                                            <td style="font-weight: bold; text-align: right;">Total</td>  
                                            <td>Rp. <?= number_format($sum_menu_price_with_discount,0) ?></td>
                                        </tr>
                                       
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                        <br>
                        <hr>
                        <!-- COMPLIMET -->
                        <div class="container">
                            <h4 style="font-weight: bold; text-align: center; margin-top: 20px;">Pengeluaran </h4>
                            <div class="row">
                                <div class="col-sm">
                                    <h4 style="font-weight: bold; text-align: center; margin-top: 20px;">Pembelian Bahan Baku</h4>
                                    <hr>
                                    <table style="width: 100%" class="tbl">    
                                        <tr>
                                            <th style="font-weight: bold; text-align: center;">No</th>
                                            <th>Unit</th>
                                            <th>Lunas</th>
                                            <th>Sisa (Piutang)</th>
                                            <th>Total</th>
                                        </tr> 
                                        <?php  
                                            $sum_of_gtotal = 0; 
                                            $jml_beli_lunas = 0; 
                                            $sisa_utang_beli = 0; 
                                            if (!empty($result['purchases']) && isset($result['purchases'])):
                                                foreach ($result['purchases'] as $key => $value): 
                                                    $sum_of_gtotal += $value->grand_total; 
                                                    $jml_beli_lunas += $value->paid; 
                                                    $sisa_utang_beli += $value->due; 
                                                    $key++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center"><?= $key ?></td>
                                                        <td><?= getRestoNameById($value->menu_resto_id) ?></td>  
                                                        <td>Rp. <?= number_format($value->paid,0) ?></td>
                                                        <td>Rp. <?= number_format($value->due,0) ?></td>
                                                        <td>Rp. <?= number_format($value->grand_total,0) ?></td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                        ?>
                                        <tr> 
                                            <td>&nbsp;</td> 
                                            <td style="font-weight: bold; text-align: right;">Total</td>  
                                            <td>Rp. <?= number_format($jml_beli_lunas) ?></td>
                                            <td>Rp. <?= number_format($sisa_utang_beli) ?></td>
                                            <td>Rp. <?= number_format($sum_of_gtotal) ?></td>
                                        </tr>
                                    </table> 
                                </div>
                                <div class="col-sm">
                                    <h4 style="font-weight: bold; text-align: center; margin-top: 20px;">Operasional</h4>
                                    <hr>
                                    <table style="width: 100%" class="tbl">    
                                        <tr>
                                            <th style="font-weight: bold; text-align: center;">No</th>
                                            <th>Unit</th>
                                            <th>Total</th>
                                        </tr> 
                                        <?php  
                                            $sum_of_eamount = 0; 
                                            if (!empty($result['expenses']) && isset($result['expenses'])):
                                                foreach ($result['expenses'] as $key => $value): 
                                                    $sum_of_eamount += $value->amount;  
                                                    $key++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center"><?= $key ?></td>
                                                        <td><?= getRestoNameById($value->menu_resto_id) ?></td>  
                                                        <td>Rp. <?= number_format($value->amount); ?></td> 
                                                        
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                        ?>
                                        <tr>   
                                        <td>&nbsp;</td>
                                            <td style="font-weight: bold; text-align: right;">Total</td>  
                                            <td>Rp. <?= number_format($sum_of_eamount,0) ?></td> 
                                            
                                            
                                        </tr>
                                    </table> 
                                </div>
                            </div>
                        </div>

                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <hr>
                                    <h4 style="font-weight: bold; text-align: center; margin-top: 20px;">Limbah</h4>
                                    <hr>
                                    <table style="width: 100%" class="tbl">    
                                        <tr>
                                            <th style="font-weight: bold; text-align: center;">NO</th>
                                            <th>RF</th>
                                            <th>Total Kerugian</th> 
                                            <th>Keterangan</th> 
                                        </tr> 
                                        <?php  
                                            $sum_of_wamount = 0; 
                                            if (!empty($result['wastes']) && isset($result['wastes'])):
                                                foreach ($result['wastes'] as $key => $value): 
                                                    $sum_of_wamount += $value->total_loss;  
                                                    $key++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center"><?= $key ?></td>
                                                        <td><?= $value->reference_no; ?></td> 
                                                        <td>Rp. <?= number_format($value->total_loss,0); ?></td> 
                                                        <td><?= $value->note ?></td>  
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                        ?>
                                        <tr>   
                                            <td>&nbsp;</td>
                                            <td style="font-weight: bold; text-align: right;">Total</td>  
                                            <td>Rp. <?= number_format($sum_of_wamount,0) ?></td> 
                                            <td>&nbsp;</td>
                                            
                                        </tr>
                                    </table> 
                                </div>
                            </div>
                        </div>

                        
                        
                    </div>    
                        
                </div>
            </div>
        </div>
    </div>

</div>

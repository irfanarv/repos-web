<div class="container-fluid">
    <div class="row">

    <div class="col-sm-12"> 
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Piutang Supplier</h4>
                  <div class="row d-flex justify-content-center">
                        <div class="col-lg-12 col-xl-3 ml-auto align-self-center">
                            <div class="text-center text-muted">
                                <?php echo form_open(base_url() . 'SupplierPayment/supplierPayments') ?>
                                <select name="category_id" class="form-control select2" >
                                    <option value="">Pilih Unit</option>
                                    <?php foreach ($menu_resto as $resto) { ?>
                                        <option value="<?php echo $resto->id ?>" <?php echo set_select('menu_resto_id', $resto->id); ?>><?php echo $resto->category_name ?></option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-1">
                        <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left"><?php echo lang('submit'); ?></button>
                                    </div>
                        <div class="col-lg-12 col-xl-4">
                            <div class="float-right d-print-none">
                            <a href="<?php echo base_url() ?>SupplierPayment/addSupplierPayment"><button type="button" class="btn btn-block btn-primary pull-right">Tambah Piutang Supplier</button></a>
                            </div>
                        </div>
                    </div>
              </div>
          </div>
          
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 9%"><?php echo lang('date'); ?></th>
                                <th style="width: 18%"><?php echo lang('supplier'); ?></th>
                                <th style="width: 14%">Jumlah</th>
                                <th style="width: 28%"><?php echo lang('note'); ?></th>
                                <th style="width: 19%"><?php echo lang('added_by'); ?></th>
                                <th style="width: 10%">Unit</th>
                                <th style="width: 6%"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($supplierPayments && !empty($supplierPayments)) {
                                $i = count($supplierPayments);
                            }
                            foreach ($supplierPayments as $spns) {
                                ?>                       
                                <tr> 
                                    <td><?php echo $i--; ?></td> 
                                    <td><?php echo date($this->session->userdata('date_format'), strtotime($spns->date)); ?></td> 
                                    <td><?php echo getSupplierNameById($spns->supplier_id); ?></td> 
                                    <td> <?php echo $this->session->userdata('currency'); ?> <?php echo number_format($spns->amount,0); ?></td>
                                    <td><?php if ($spns->note != NULL) echo $spns->note; ?></td> 
                                    <td><?php echo userName($spns->user_id); ?></td>  
                                    <td><?php echo getRestoNameById($spns->menu_resto_id); ?></td>

                                    <td style="text-align: center">
                                        <div class="">
                                        
                                            <a class="delete" href="<?php echo base_url() ?>SupplierPayment/deleteSupplierPayment/<?php echo $this->custom->encrypt_decrypt($spns->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>  

                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>
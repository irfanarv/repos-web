<div class="container-fluid">
    <div class="row">
          


          <div class="col-sm-12"> 
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Daftar Operasional</h4>
                  <div class="row d-flex justify-content-center">
                        <div class="col-lg-12 col-xl-3 ml-auto align-self-center">
                            <div class="text-center text-muted">
                                <?php echo form_open(base_url() . 'Expense/expenses') ?>
                                <select name="category_id" class="form-control select2" >
                                    <option value="">Pilih Unit</option>
                                    <?php foreach ($menu_resto as $resto) { ?>
                                        <option value="<?php echo $resto->id ?>" <?php echo set_select('menu_resto_id', $resto->id); ?>><?php echo $resto->category_name ?></option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-1">
                        <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left"><?php echo lang('submit'); ?></button>
                                    </div>
                        <div class="col-lg-12 col-xl-4">
                            <div class="float-right d-print-none">
                            <a href="<?php echo base_url() ?>Expense/addEditExpense"><button type="button" class="btn btn-block btn-primary pull-right">Tambah Operasional</button></a>
                            </div>
                        </div>
                    </div>
              </div>
          </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    if ($this->session->flashdata('exception')) {

                        echo '<section class="content-header"><div class="alert alert-success alert-dismissible"> 
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><i class="icon fa fa-check"></i>';
                        echo $this->session->flashdata('exception');
                        echo '</p></div></section>';
                    }
                    ?> 

<div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 8%"><?php echo lang('date'); ?></th>
                                <th style="width: 10%">Jumlah</th>
                                <th style="width: 15%">Kategori</th>
                                <th style="width: 16%"><?php echo lang('responsible_person'); ?></th>
                                <th style="width: 24%" style="width: 22%;"><?php echo lang('note'); ?></th>
                                <th style="width: 16%">Unit</th>
                                <th style="width: 10%"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($expenses && !empty($expenses)) {
                                $i = count($expenses);
                            }
                            foreach ($expenses as $expnss) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo date($this->session->userdata('date_format'), strtotime($expnss->date)); ?></td> 
                                    <td> <?php echo $this->session->userdata('currency'); ?> <?php echo number_format($expnss->amount,0); ?></td>
                                    <td><?php echo expenseItemName($expnss->category_id); ?></td> 
                                    <td><?php echo employeeName($expnss->employee_id); ?></td> 
                                    <td><?php if ($expnss->note != NULL) echo $expnss->note; ?></td> 
                                    <td><?php echo getRestoNameById($expnss->menu_resto_id); ?></td>
                                    
                                    <td style="text-align: center">
                                        <div class="">
                                        <a href="<?php echo base_url() ?>Expense/addEditExpense/<?php echo $this->custom->encrypt_decrypt($expnss->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Expense/deleteExpense/<?php echo $this->custom->encrypt_decrypt($expnss->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>  

                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>


<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>

 
<script type="text/javascript">  
    var ingredient_id_container = [];


    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    })

</script>
<style>
    .input-sm{
        font-size:17px;
    }
</style>
<div class="container-fluid">

    <div class="row">
          <div class="col-sm-12"> 
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Daftar Menu</h4>
                  <div class="row d-flex justify-content-center">
                        <div class="col-lg-12 col-xl-3 ml-auto align-self-center">
                            <div class="text-center text-muted">
                                <?php echo form_open(base_url() . 'Master/foodMenus') ?>
                                <select name="category_id" class="form-control select2" >
                                    <option value="">Pilih Menu</option>
                                    <?php foreach ($foodMenuCategories as $ctry) { ?>
                                        <option value="<?php echo $ctry->id ?>" <?php echo set_select('category_id', $ctry->id); ?>><?php echo $ctry->category_name ?></option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-1">
                        <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left"><?php echo lang('submit'); ?></button>
                                    </div>
                        <div class="col-lg-12 col-xl-4">
                            <div class="float-right d-print-none">
                                <a href="<?php echo base_url() ?>Master/addEditFoodMenu" class="btn btn-info text-light">Tambah Menu</a>
                                <a href="<?php echo base_url() ?>Master/uploadFoodMenu" class="btn btn-success text-light">Upload Menu</a>
                                <!-- <a href="<?php echo base_url() ?>Master/uploadFoodMenuIngredients" class="btn btn-warning text-light">Upload Resep</a> -->
                            </div>
                        </div>
                    </div>
              </div>
          </div>
          
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 8%"><?php echo lang('code'); ?></th>
                                <th style="width: 25%"><?php echo lang('name'); ?></th>
                                <th style="width: 13%">Unit</th>
                                <th style="width: 13%"><?php echo lang('sale_price'); ?></th>
                                <!-- <th style="width: 13%"><?php echo lang('total_ingredients'); ?></th> -->
                                <th style="width: 18%"><?php echo lang('added_by'); ?></th>
                                <th style="width: 16%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($foodMenus && !empty($foodMenus)) {
                                $i = count($foodMenus);
                            }
                            foreach ($foodMenus as $value) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $value->code; ?></td> 
                                    <td><?php echo $value->name; ?></td> 
                                    <td><?php echo foodMenucategoryName($value->category_id); ?></td> 
                                    <!-- <td> <?php echo $this->session->userdata('currency'); ?> <?php echo $value->sale_price; ?></td> -->
                                    <td><?php echo $this->session->userdata('currency'); ?> <?php echo number_format($value->sale_price,0,',','.')?></td>
                                    <!-- <td style="text-align: center"><?php echo totalIngredients($value->id); ?></td> -->
                                    <td><?php echo userName($value->user_id); ?></td>  

                                    <td style="text-align: center">
                                        <div class="">
                                            <!-- <a href="<?php echo base_url() ?>Master/foodMenuDetails/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fas fa-file-alt text-info mr-2"></i>
                                            </a> -->
                                            <a href="<?php echo base_url() ?>Master/addEditFoodMenu/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit text-success mr-2"></i>
                                            </a>
                                            <!-- <a href="<?php echo base_url() ?>Master/assignFoodMenuModifier/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fas fa-user-edit text-warning mr-2"></i>
                                            </a> -->
                                            <a class="delete" href="<?php echo base_url() ?>Master/deleteFoodMenu/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>  

                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>
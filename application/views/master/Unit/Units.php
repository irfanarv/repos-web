<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                      <a href="<?php echo base_url() ?>Master/addEditUnit"><button type="button" class="btn btn-block btn-primary pull-right">Tambah Satuan Bahan</button></a>
                  </div>
                  <h4 class="page-title">Satuan Bahan</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 25%"><?php echo lang('unit_name'); ?></th>
                                <th style="width: 15%"><?php echo lang('description'); ?></th>
                                <th style="width: 16%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($Units && !empty($Units)) {
                                $i = count($Units);
                            }
                            foreach ($Units as $unit) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $unit->unit_name; ?></td>
                                    <td><?php echo $unit->description; ?></td>
                                    <td style="text-align: center">
                                        <div class="">
                                        <a href="<?php echo base_url() ?>Master/addEditUnit/<?php echo $this->custom->encrypt_decrypt($unit->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Master/deleteUnit/<?php echo $this->custom->encrypt_decrypt($unit->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>
                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>

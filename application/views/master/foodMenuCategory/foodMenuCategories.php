<div class="container-fluid">
    <div class="row"> 
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                          <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                          </li>
                          
                          <li class="breadcrumb-item active">Unit Usaha</li>
                      </ol> -->
                      <a href="<?php echo base_url() ?>Master/addEditFoodMenuCategory"><button type="button" class="btn btn-block btn-primary pull-right">Tambah Baru</button></a><br>
                  </div>
                  <h4 class="page-title">Unit Usaha</h4>
              </div>
          </div>
    </div>
  
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <?php
if ($this->session->flashdata('exception')) {

    echo '<section class="content-header"><div class="alert alert-success alert-dismissible"> 
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p><i class="icon fa fa-check"></i>';
    echo $this->session->flashdata('exception');
    echo '</p></div></section>';
}
?><div class="box box-primary">
<!-- /.box-header -->
<div class="box-body table-responsive">
    <table id="datatable" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="width: 1%">No</th>
                <th style="width: 28%">Nama Unit</th>
                <th style="width: 35%">Deskripsi</th>
                <th style="width: 6%;text-align: center%">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($foodMenuCategories && !empty($foodMenuCategories)) {
                $i = count($foodMenuCategories);
            }
            foreach ($foodMenuCategories as $fmc) {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $i--; ?></td>
                    <td><?php echo $fmc->category_name; ?></td>
                    <td><?php echo $fmc->description; ?></td>
                    <td style="text-align: center">
                        <div class="">
                        <a href="<?php echo base_url() ?>Master/addEditFoodMenuCategory/<?php echo $this->custom->encrypt_decrypt($fmc->id, 'encrypt'); ?>" >
                                <i class="fa fa-edit mr-2"></i>
                            </a>
                            <a class="delete" href="<?php echo base_url() ?>Master/deleteFoodMenuCategory/<?php echo $this->custom->encrypt_decrypt($fmc->id, 'encrypt'); ?>" >
                                <i class="fa fa-trash text-danger mr-2"></i>
                            </a>
                        </div>
                    </td>

                    
                </tr>
                <?php
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Nama Unit</th>
                <th>Deskripsi</th>
                <th style="text-align: center">Aksi</th>
            </tr>
        </tfoot>
    </table>
</div>
<!-- /.box-body -->
</div>
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    $(function () {
        $('#datatable').DataTable({
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>
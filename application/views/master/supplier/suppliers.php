<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                      <a href="<?php echo base_url() ?>Master/addEditSupplier"><button type="button" class="btn btn-block btn-primary pull-right">Tambah Supplier</button></a>
                  </div>
                  <h4 class="page-title">Supplier</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 14%"><?php echo lang('name'); ?></th>
                                <th style="width: 14%"><?php echo lang('contact_person'); ?></th>
                                <th style="width: 9%">Telpon</th>
                                <th style="width: 8%"><?php echo lang('email'); ?></th>
                                <th style="width: 18%">Alamat</th>
                                <th style="width: 15%"><?php echo lang('description'); ?></th>
                                <th style="width: 13%"><?php echo lang('added_by'); ?></th>
                                <th style="width: 10%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($suppliers && !empty($suppliers)) {
                                $i = count($suppliers);
                            }
                            foreach ($suppliers as $si) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $si->name; ?></td> 
                                    <td><?php echo $si->contact_person; ?></td> 
                                    <td><?php echo $si->phone; ?></td> 
                                    <td><?php echo $si->email; ?></td>
                                    <td><?php echo $si->address; ?></td>
                                    <td><?php if ($si->description != NULL) echo $si->description; ?></td>
                                    <td><?php echo userName($si->user_id); ?></td> 
                                    
                                    <td style="text-align: center">
                                        <div class="">
                                        <a href="<?php echo base_url() ?>Master/addEditSupplier/<?php echo $this->custom->encrypt_decrypt($si->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Master/deleteSupplier/<?php echo $this->custom->encrypt_decrypt($si->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>  
                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>

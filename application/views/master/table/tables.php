<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Manajemen Meja</li>
                      </ol> -->
                      <a href="<?php echo base_url() ?>Master/addEditTable"><button type="button" class="btn btn-block btn-primary pull-right"><?php echo lang('add_table'); ?></button></a>
                  </div>
                  <h4 class="page-title">Manajemen Meja</h4> 
              </div>
              <?php
                if ($this->session->flashdata('exception')) {

                    echo '<section class="content-header"><div class="alert alert-success alert-dismissible"> 
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><i class="icon fa fa-check"></i>';
                    echo $this->session->flashdata('exception');
                    echo '</p></div></section>';
                }
                ?> 
                <br>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 20%"><?php echo lang('table_name'); ?></th>
                                <th style="width: 10%"><?php echo lang('seat_capacity'); ?></th>
                                <th style="width: 10%"><?php echo lang('position'); ?></th>
                                <th style="width: 20%"><?php echo lang('description'); ?></th>
                                <th style="width: 26%"><?php echo lang('added_by'); ?></th>
                                <th style="width: 10%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($tables && !empty($tables)) {
                                $i = count($tables);
                            }
                            foreach ($tables as $value) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $value->name; ?></td> 
                                    <td><?php echo $value->sit_capacity; ?></td> 
                                    <td><?php echo $value->position; ?></td> 
                                    <td><?php echo $value->description; ?></td> 
                                    <td><?php echo userName($value->user_id); ?></td>
                                    <td style="text-align:center;">
                                        <div class="">
                                            <a href="<?php echo base_url() ?>Master/addEditTable/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Master/deleteTable/<?php echo $this->custom->encrypt_decrypt($value->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>  
                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 20%"><?php echo lang('table_name'); ?></th>
                                <th style="width: 10%"><?php echo lang('seat_capacity'); ?></th>
                                <th style="width: 10%"><?php echo lang('position'); ?></th>
                                <th style="width: 20%"><?php echo lang('description'); ?></th>
                                <th style="width: 26%"><?php echo lang('added_by'); ?></th>
                                <th style="width: 10%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>
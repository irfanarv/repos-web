<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                      <a href="<?php echo base_url() ?>Master/addEditExpenseItem"><button type="button" class="btn btn-block btn-primary pull-right">Tambah Kategori Operasional</button></a>
                  </div>
                  <h4 class="page-title">Kategori Operasional</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <?php
if ($this->session->flashdata('exception')) {

    echo '<section class="content-header"><div class="alert alert-success alert-dismissible"> 
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p><i class="icon fa fa-check"></i>';
    echo $this->session->flashdata('exception');
    echo '</p></div></section>';
}
?> 
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 30%">Nama</th>
                                <th style="width: 34%"><?php echo lang('description'); ?></th>
                                <th style="width: 26%"><?php echo lang('added_by'); ?></th>
                                <th style="width: 6%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($expenseItems && !empty($expenseItems)) {
                                $i = count($expenseItems);
                            }
                            foreach ($expenseItems as $ei) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $ei->name; ?></td> 
                                    <td><?php echo $ei->description; ?></td> 
                                    <td><?php echo userName($ei->user_id); ?></td>  

                                    <td style="text-align: center">
                                        <div class="">
                                        <a href="<?php echo base_url() ?>Master/addEditExpenseItem/<?php echo $this->custom->encrypt_decrypt($ei->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Master/deleteExpenseItem/<?php echo $this->custom->encrypt_decrypt($ei->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>  

                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>


<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>
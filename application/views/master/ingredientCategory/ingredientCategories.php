<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                      <a href="<?php echo base_url() ?>Master/addEditIngredientCategory"><button type="button" class="btn btn-block btn-primary pull-right">Tambah Kategori Bahan Baku</button></a>
                  </div>
                  <h4 class="page-title">Kategori Bahan Baku</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 23%"><?php echo lang('category_name'); ?></th>
                                <th style="width: 38%"><?php echo lang('description'); ?></th>
                                <th style="width: 13%"><?php echo lang('added_by'); ?></th>
                                <th style="width: 17%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($ingredientCategories && !empty($ingredientCategories)) {
                                $i = count($ingredientCategories);
                            }
                            foreach ($ingredientCategories as $ic) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $ic->category_name; ?></td> 
                                    <td><?php echo $ic->description; ?></td> 
                                    <td><?php echo userName($ic->user_id); ?></td>
                                    <td style="text-align: center">
                                        <div class="">
                                        <a href="<?php echo base_url() ?>Master/addEditIngredientCategory/<?php echo $this->custom->encrypt_decrypt($ic->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Master/deleteIngredientCategory/<?php echo $this->custom->encrypt_decrypt($ic->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>

                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th><?php echo lang('category_name'); ?></th>
                                <th><?php echo lang('description'); ?></th>
                                <th><?php echo lang('added_by'); ?></th>  
                                <th style="text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>
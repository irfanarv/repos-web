<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12"> 
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Bahan Baku</h4>
                  <div class="row d-flex justify-content-center">
                        <div class="col-lg-12 col-xl-3 ml-auto align-self-center">
                            
                        </div>
                        <div class="col-lg-12 col-xl-1">
                        
                                    </div>
                        <div class="col-lg-12 col-xl-4">
                            <div class="float-right d-print-none">
                                <a href="<?php echo base_url() ?>Master/addEditIngredient" class="btn btn-info text-light">Tambah Bahan Baku</a>
                                <!-- <a href="<?php echo base_url() ?>Master/uploadingredients" class="btn btn-success text-light">Upload Menu</a> -->
                                <!-- <a href="<?php echo base_url() ?>Master/uploadFoodMenuIngredients" class="btn btn-warning text-light">Upload Resep</a> -->
                            </div>
                        </div>
                    </div>
              </div>
          </div>
          
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <?php
                if ($this->session->flashdata('exception')) {

                    echo '<section class="content-header"><div class="alert alert-success alert-dismissible"> 
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><i class="icon fa fa-check"></i>';
                    echo $this->session->flashdata('exception');
                    echo '</p></div></section>';
                }
                ?> 
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 6%"><?php echo lang('code'); ?></th>
                                <th style="width: 22%"><?php echo lang('name'); ?></th>
                                <th style="width: 16%">Kategori</th>
                                <th style="width: 12%"><?php echo lang('purchase_price'); ?></th>
                                <th style="width: 15%"><?php echo lang('alert_quantity_amount'); ?></th>
                                <th style="width: 4%"><?php echo lang('unit'); ?></th>
                                <th style="width: 15%"><?php echo lang('added_by'); ?></th>
                                <th style="width: 10%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($ingredients && !empty($ingredients)) {
                                $i = count($ingredients);
                            }
                            foreach ($ingredients as $ingrnts) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $ingrnts->code; ?></td> 
                                    <td><?php echo $ingrnts->name; ?></td> 
                                    <td><?php echo categoryName($ingrnts->category_id); ?></td> 
                                    <td> <?php echo $this->session->userdata('currency'); ?> <?php echo number_format($ingrnts->purchase_price,0,',','.')?></td>
                                    <td><?php echo number_format($ingrnts->alert_quantity,0); ?></td> 
                                    <td><?php echo unitName($ingrnts->unit_id); ?></td>  
                                    <td><?php echo userName($ingrnts->user_id); ?></td>

                                    <td style="text-align: center">
                                        <div class="">
                                        <a href="<?php echo base_url() ?>Master/addEditIngredient/<?php echo $this->custom->encrypt_decrypt($ingrnts->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Master/deleteIngredient/<?php echo $this->custom->encrypt_decrypt($ingrnts->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>

                                    <!-- <td style="text-align: center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-gear tiny-icon"></i><span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" role="menu"> 
                                                <li><a href="<?php echo base_url() ?>Master/addEditIngredient/<?php echo $this->custom->encrypt_decrypt($ingrnts->id, 'encrypt'); ?>" ><i class="fa fa-pencil tiny-icon"></i>Edit</a></li>
                                                <li><a class="delete" href="<?php echo base_url() ?>Master/deleteIngredient/<?php echo $this->custom->encrypt_decrypt($ingrnts->id, 'encrypt'); ?>" ><i class="fa fa-trash tiny-icon"></i>Delete</a></li> 
                                            </ul> 
                                        </div>
                                    </td> -->
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>


<div class="modal fade" id="uploadingredentsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-2x">×</i></span></button>
                <h4 class="modal-title" id="myModalLabel"><i style="color:#3c8dbc" class="fa fa-plus-square-o"></i> <?php echo lang('upload_ingredients'); ?></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?php echo base_url() ?>Master/ExcelDataAddIngredints" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo lang('upload_file'); ?><span style="color:red;">  *</span></label>
                        <div class="col-sm-7">
                            <input type="file" class="form-control" name="userfile" id="userfile" placeholder="Upload file" value="">
                            <div class="alert alert-danger error-msg customer_err_msg_contnr" style="padding: 5px !important;">
                                <p class="customer_err_msg"></p>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addNewGuest">
                    <i class="fa fa-save"></i> <?php echo lang('upload'); ?> </button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>Master/downloadPDF/Ingredient_Upload.xlsx">
                    <i class="fa fa-save"></i> <?php echo lang('download_sample'); ?></a>
            </div>
        </div>
    </div>
</div>
<!-- DataTables -->

<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>

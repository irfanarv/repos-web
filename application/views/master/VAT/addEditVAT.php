<style type="text/css">
    .required_star{
        color: #dd4b39;
    }

    .radio_button_problem{
        margin-bottom: 19px;
    }
</style> 

<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                          <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Manajemen Pajak</a>
                          </li>
                          <li class="breadcrumb-item active"><?= isset($VATs) ? lang('edit') : lang('add') ?> <?php echo strtoupper(lang('vat')); ?></li>
                      </ol> -->
                  </div>
                  <h4 class="page-title"><?= isset($VATs) ? lang('edit') : lang('add') ?> <?php echo strtoupper(lang('vat')); ?></h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- form start -->
                <?php echo form_open(base_url('Master/addEditVAT/' . (isset($VATs) ? $this->custom->encrypt_decrypt($VATs->id, 'encrypt') : ''))); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Nama Pajak <span class="required_star">*</span></label>
                                <input tabindex="1" type="text" name="name" class="form-control" placeholder="<?php echo strtoupper(lang('vat')); ?> <?php echo lang('name'); ?>" value="<?= isset($VATs) && $VATs ? $VATs->name : set_value('name') ?>">
                            </div>
                            <?php if (form_error('name')) { ?>
                                <div class="alert alert-danger" style="padding: 5px !important;">
                                    <p><?php echo form_error('name'); ?></p>
                                </div>
                            <?php } ?>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Presentase <span class="required_star">*</span></label>
                                <input tabindex="2" type="text" name="percentage" class="form-control integerchk" placeholder="<?php echo lang('percentage'); ?>"  value="<?= isset($VATs) && $VATs ? $VATs->percentage : set_value('percentage') ?>">
                            </div> 
                            <?php if (form_error('percentage')) { ?>
                                <div class="alert alert-danger" style="padding: 5px !important;">
                                    <p><?php echo form_error('percentage'); ?></p>
                                </div>
                            <?php } ?> 

                        </div> 

                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name="submit" value="submit" class="btn btn-primary"><?php echo lang('submit'); ?></button>
                    <a href="<?php echo base_url() ?>Master/VATs"><button type="button" class="btn btn-primary"><?php echo lang('back'); ?></button></a>
                </div>
                <?php echo form_close(); ?>
            </div>
                </div>
            </div>
        </div>
    </div>


</div>

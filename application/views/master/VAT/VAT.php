<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                          <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Manajemen Pajak</li>
                      </ol> -->
                      <!-- <a href="<?php echo base_url() ?>Master/addEditVAT"><button type="button" class="btn btn-block btn-primary pull-right">Tambah Daftar Pajak</button></a> -->
                  </div>
                  <h4 class="page-title">Manajemen Pajak</h4>
              </div>
          </div>
    </div>
 
    <div class="row">
        <div class="col-md-12">
        <?php
if ($this->session->flashdata('exception')) {

    echo '<section class="content-header"><div class="alert alert-success alert-dismissible"> 
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p><i class="icon fa fa-check"></i>';
    echo $this->session->flashdata('exception');
    echo '</p></div></section>';
}
?> 
            <div class="card">
                <div class="card-body">
                <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive"> 
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 25%"><?php echo lang('name'); ?></th>
                                <th style="width: 16%">Presentase</th>
                                <th style="width: 10%">Status</th>
                                <th style="width: 10%;text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($VATs && !empty($VATs)) {
                                $i = count($VATs);
                            }
                            foreach ($VATs as $vat) {
                                ?>                       
                                <tr> 
                                    <td style="text-align: center"><?php echo $i--; ?></td>
                                    <td><?php echo $vat->name; ?></td>
                                    <td><?php echo $vat->percentage; ?> %</td>
                                    <td><?php echo $vat->status; ?></td>
                                    <td style="text-align: center">
                                        <div class="">
                                            <?php if ($vat->status == 'Aktif') { ?>
                                                <a href="<?php echo base_url() ?>Master/deactivateVAT/<?php echo $this->custom->encrypt_decrypt($vat->id, 'encrypt'); ?>" >
                                                    <i class="fas fa-lock mr-2"></i>
                                                </a>
                                                <?php } else { ?>
                                                    <a href="<?php echo base_url() ?>Master/activateVat/<?php echo $this->custom->encrypt_decrypt($vat->id, 'encrypt'); ?>" >
                                                    <i class="fa fa-lock-open mr-2"></i>
                                                </a> 
                                            <?php } ?>
                                            <a href="<?php echo base_url() ?>Master/addEditVAT/<?php echo $this->custom->encrypt_decrypt($vat->id, 'encrypt'); ?>">
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <a class="delete" href="<?php echo base_url() ?>Master/deleteVAT/<?php echo $this->custom->encrypt_decrypt($vat->id, 'encrypt'); ?>" >
                                                <i class="fa fa-trash text-danger mr-2"></i>
                                            </a>
                                        </div>
                                    </td>  
                                    
                                </tr>
                                <?php
                            }
                            ?> 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div> 
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    $(function () { 
        $('#datatable').DataTable({ 
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>
<div class="container-fluid">

    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                    <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Inventory</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Inventory</h4>
              </div>
          </div>
    </div>

    <div class="row">
          <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <?php echo form_open(base_url() . 'Inventory/index') ?>
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="hiddentIngredientID" id="hiddentIngredientID" value="<?= isset($ingredient_id) ? $ingredient_id : '' ?>">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group"> 
                                            <select class="form-control select2 category_id" name="category_id" id="category_id" style="width: 100%;">
                                                <option value="">Unit</option>
                                                <?php foreach ($ingredient_categories as $value) { ?>
                                                    <option value="<?php echo $value->id ?>" <?php echo set_select('category_id', $value->id); ?>><?php echo $value->category_name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div> 
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group"> 
                                            <select class="form-control select2" name="ingredient_id" id="ingredient_id" style="width: 100%;">
                                                <option value=""><?php echo lang('ingredient'); ?></option>
                                                <?php foreach ($ingredients as $value) { ?>
                                                    <option value="<?php echo $value->id ?>" <?php echo set_select('ingredient_id', $value->id); ?>><?php echo $value->name . "(" . $value->code . ")" ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <select class="form-control select2" name="food_id" id="food_id" style="width: 100%;">
                                                <option value=""><?php echo lang('food_menu'); ?></option>
                                                <?php foreach ($foodMenus as $value) { ?>
                                                    <option value="<?php echo $value->id ?>" <?php echo set_select('food_id', $value->id); ?>><?php echo substr(ucwords(strtolower($value->name)), 0, 18) . "(" . $value->code . ")" ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <button type="submit" name="submit" value="submit" class="btn btn-block btn-primary pull-left">Cari</button>
                                    </div>
                                    <div class="col-sm-3">
                                        
                                            <a href="<?= base_url() . 'Inventory/getInventoryAlertList' ?>" class="btn btn-block btn-primary pull-left"><span style="color:red"><?= getAlertCount() ?></span> Bahan minim stock </a>
                                        
                                    </div>
                                    <div class="col-sm-4">
                                        <strong id="stockValue"></strong>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
          </div>
    </div>

    <div class="row">
          <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                    <div class="box box-primary"> 
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="title" style="width: 5%"><?php echo lang('sn'); ?></th>
                                <th class="title" style="width: 37%"><?php echo lang('ingredient'); ?></th>
                                <th class="title" style="width: 20%">Unit</th>
                                <th class="title" style="width: 20%"><?php echo lang('stock_qty_amount'); ?></th>
                                <th class="title" style="width: 20%"><?php echo lang('alert_qty_amount'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $totalStock = 0;
                                $grandTotal = 0;
                                $alertCount = 0;
                                $totalTK = 0;
                                if (!empty($inventory) && isset($inventory)):
                                    foreach ($inventory as $key => $value):
                                        $totalStock = $value->total_purchase - $value->total_consumption - $value->total_modifiers_consumption - $value->total_waste + $value->total_consumption_plus - $value->total_consumption_minus;
                                        $totalTK = $totalStock * getLastPurchaseAmount($value->id);
                                        if ($totalStock >= 0) {
                                            $grandTotal = $grandTotal + $totalStock * getLastPurchaseAmount($value->id);
                                        }
                                        $key++;
                                        ?>
                                        <tr>
                                            <td style="text-align: center"><?= $key ?></td>
                                            <td><?= $value->name ?></td>
                                            <td><?= $value->category_name ?></td>
                                            <td><span style="<?= ($totalStock <= number_format($value->alert_quantity,0)) ? 'color:red' : '' ?>"><?= ($totalStock) ? $totalStock : '0' ?><?= " " . $value->unit_name ?></span></td>
                                            <td><?= number_format($value->alert_quantity,0) . " " . $value->unit_name ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                        </tbody>
                        <tfoot>
                            <tr>
                            <th class="title" style="width: 5%"><?php echo lang('sn'); ?></th>
                                <th class="title" style="width: 37%"><?php echo lang('ingredient'); ?></th>
                                <th class="title" style="width: 20%">Unit</th>
                                <th class="title" style="width: 20%"><?php echo lang('stock_qty_amount'); ?></th>
                                <th class="title" style="width: 20%"><?php echo lang('alert_qty_amount'); ?></th>
                            </tr>
                        </tfoot> 
                    </table>
                    <input type="hidden" value="<?= number_format($grandTotal, 0) ?>" id="grandTotal" name="grandTotal">
                </div>
                <!-- /.box-body -->
            </div>
                    </div>
                </div>
          </div>
    </div>

    <script type="text/javascript">
        $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        })
        $(function(){
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
        }) 
        $(function(){
            $('#food_id').on('change', function(){
                var value = this.value;
                if(value){
                    $('#category_id').prop('disabled', true);
                    $('#ingredient_id').prop('disabled', true);
                }else{
                    $('#category_id').prop('disabled', false);
                    $('#ingredient_id').prop('disabled', false);
                }
            });
            $('#ingredient_id').on('change', function(){
                var ingredient_id = this.value;
                var category_id = $('select.category_id').find(':selected').val();
                if(ingredient_id || category_id){
                    $('#food_id').prop('disabled', true);
                }else{
                    $('#food_id').prop('disabled', false);
                }
            });
            $('#category_id').on('change', function(){
                var category_id = this.value;
                if(category_id){
                    $('#food_id').prop('disabled', true);
                }else{
                    $('#food_id').prop('disabled', false);
                }

                var options = '';
                $.ajax({
                    type : 'get',
                    url : '<?php echo base_url(); ?>Inventory/getIngredientInfoAjax',
                    data: {category_id: category_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
                    datatype: 'json',
                    success : function(data){
                        var json = $.parseJSON(data);
                        options += '<option  value=""><?php echo lang('ingredient'); ?></option>';
                        $.each(json, function (i, v) {
                            options += '<option  value="'+v.id+'">'+v.name+'('+v.code+')</option>';
                        });
                        $('#ingredient_id').html(options);
                    }
                });
            });
        });

        $(document).ready(function(){
            var category_id = $('select.category_id').find(':selected').val();
            var ingredient_id = $('select.ingredient_id').find(':selected').val();
            var food_id = $('select.food_id').find(':selected').val();
            if(food_id){
                $('#category_id').prop('disabled', false);
                $('#ingredient_id').prop('disabled', false);

            }else if(ingredient_id || category_id){
                $('#category_id').prop('disabled', false);
                $('#ingredient_id').prop('disabled', false);
            }
            else{
                if(food_id){
                    $('#category_id').prop('disabled', true);
                    $('#ingredient_id').prop('disabled', true);
                }

            }
            if(category_id){
                var options = '';
                var selectedID=$("#hiddentIngredientID").val();
                $.ajax({
                    type : 'get',
                    url : '<?php echo base_url(); ?>Inventory/getIngredientInfoAjax',
                    data: {category_id: category_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
                    datatype: 'json',
                    success : function(data){
                        var json = $.parseJSON(data);
                        options += '<option  value=""><?php echo lang('ingredient'); ?></option>';
                        $.each(json, function (i, v) {
                            options += '<option  value="'+v.id+'">'+v.name+'('+v.code+')</option>';
                        });
                        $('#ingredient_id').html(options);
                        $('#ingredient_id').val(selectedID).change();
                    }
                });
            }
            $('#stockValue').html('Nilai Stock: <?php echo $this->session->userdata('currency'); ?> '+$('#grandTotal').val()+'<a title="Dihitung berdasarkan harga beli terakhir dan Bahan dengan Stock Qty / Jumlah negatif tidak dianggap" data-placement="top" data-toggle="tooltip" style="cursor:pointer" > <i class =" fa fa-question fa-lg form_question"></i></a>');
        });
    </script>  

    <script> 
        $(function(){ 

        var TITLE = "Rekap Data Inventory "+today; 

        $('#datatable').DataTable( {
            'autoWidth'   : false,
            'ordering'    : false,
            dom: 'Bfrtip',
            buttons: [ 
                {
                    extend: 'print',
                    title: TITLE
                },
                {
                    extend: 'excelHtml5',
                    title: TITLE
                },
                {
                    extend: 'pdfHtml5',
                    title: TITLE
                }
            ]
        } );
    } );
    </script> 
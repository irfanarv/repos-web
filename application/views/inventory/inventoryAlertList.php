<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Dashboard/dashboard">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                              <a href="<?php echo base_url() ?>Inventory/index">Inventory</a>
                            </li>
                          <li class="breadcrumb-item active">Bahan minim stock</li>
                      </ol> -->
                      
                  </div>
                  <h4 class="page-title"> 
                  <a href="<?= base_url() . 'Inventory/index' ?>" class="btn btn-twitter btn-round ml-1"><i class=" fas fa-angle-left mr-2"></i>Kembali</a>
                  Bahan Minim Stock </h4> 
              </div>
          </div>
    </div>

<style type="text/css">
    h1,h2,h3,h4,p{
        margin:3px 0px;
    }
    .body_area{
        padding:1px;
    }
    .tbl  {
        border-collapse:collapse;
        border-spacing:0;
        width: 100%;

    }
    .tbl tr td{
        padding:14px;
        font-family:Arial, sans-serif;
        font-size:15px;
        border-style:solid;
        border-width:1px;
        word-break:break-all;
    }

    .title{
        font-weight: bold;
    }
</style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="title" style="width: 5%">SN</th>
                                <th class="title" style="width: 37%">Bahan</th>
                                <th class="title" style="width: 20%">Unit</th>
                                <th class="title" style="width: 20%">Jumlah Stock</th>
                                <th class="title" style="width: 20%">Jumlah stock peringatan</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        <?php
                        $totalStock = 0;
                        $grandTotal = 0;
                        $totalTK = 0;
                        $i = 1;
                        if (!empty($inventory) && isset($inventory)):
                            foreach ($inventory as $key => $value):
                                $totalStock = $value->total_purchase - $value->total_consumption - $value->total_modifiers_consumption - $value->total_waste + $value->total_consumption_plus - $value->total_consumption_minus;
                                $totalTK = $totalStock * getLastPurchaseAmount($value->id);
                                if ($totalStock <= $value->alert_quantity):
                                    if ($totalStock >= 0) {
                                        $grandTotal = $grandTotal + $totalStock * getLastPurchaseAmount($value->id);
                                    }
                                    $key++;
                                    ?>
                                    <tr>
                                        <td style="text-align: center"><?= $i ?></td>
                                        <td><?= $value->name ?></td>
                                        <td><?= $value->category_name ?></td>
                                        <td><span style="<?= ($totalStock <= $value->alert_quantity) ? 'color:red' : '' ?>"><?= ($totalStock) ? $totalStock : '0' ?><?= " " . $value->unit_name ?></span></td>
                                        <td><?= number_format($value->alert_quantity,0) . " " . $value->unit_name ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                endif;
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="title" style="width: 5%">SN</th>
                                <th class="title" style="width: 37%">Bahan</th>
                                <th class="title" style="width: 20%">Unit</th>
                                <th class="title" style="width: 20%">Jumlah Stock</th>
                                <th class="title" style="width: 20%">Jumlah stock peringatan</th>
                            </tr>
                        </tfoot> 
                    </table>
                    <input type="hidden" value="<?= number_format($grandTotal, 0) ?>" id="grandTotal" name="grandTotal">
                </div>
                <!-- /.box-body -->
            </div>
                </div>
            </div>
        </div>
    </div>


</div>


<script type="text/javascript">
    $(function(){
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
    })
    $(function(){
        $('#category_id').on('change', function(){
            var category_id = this.value;
            var options = '';
            $.ajax({
                type : 'get',
                url : '<?php echo base_url(); ?>Inventory/getIngredientInfoAjax',
                data: {category_id: category_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
                datatype: 'json',
                success : function(data){
                    var json = $.parseJSON(data);
                    options += '<option  value="">Ingredient</option>';
                    $.each(json, function (i, v) {
                        options += '<option  value="'+v.id+'">'+v.name+'</option>';
                    });
                    $('#ingredient_id').html(options);
                }
            });
        });
    });

    $(document).ready(function(){
        var category_id = $('select.category_id').find(':selected').val();
        if(category_id){
            var options = '';
            $.ajax({
                type : 'get',
                url : '<?php echo base_url(); ?>Inventory/getIngredientInfoAjax',
                data: {category_id: category_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
                datatype: 'json',
                success : function(data){
                    var json = $.parseJSON(data);
                    options += '<option  value="">Ingredient</option>';
                    $.each(json, function (i, v) {
                        options += '<option  value="'+v.id+'">'+v.name+'</option>';
                    });
                    $('#ingredient_id').html(options);
                }
            });
        }
    });
</script> 

<script> 
    $(function(){ 

    var TITLE = "Rekap Data Inventory "+today; 

    $('#datatable').DataTable( {
        'autoWidth'   : false,
        'ordering'    : false,
        dom: 'Bfrtip',
        buttons: [ 
            {
                extend: 'print',
                title: TITLE
            },
            {
                extend: 'excelHtml5',
                title: TITLE
            },
            {
                extend: 'pdfHtml5',
                title: TITLE
            }
        ]
    } );
} );
</script> 
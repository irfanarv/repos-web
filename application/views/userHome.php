<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            #loading {
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            display: block;
            opacity: 0.7;
            background-color: #fff;
            z-index: 99;
            text-align: center;
            }

            #loading-image {
            position: absolute;
            top: 280px;
            left: 600px;
            z-index: 100;
            }
        </style>
        <meta charset="utf-8">
        <title>RePOS Cafe</title>
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">
        <link href="<?php echo base_url(); ?>assets/new_ui/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/new_ui/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/new_ui/css/style.css" rel="stylesheet" type="text/css">
        <!-- <link href="<?php echo base_url(); ?>assets/new_ui/css/pace.css" rel="stylesheet" type="text/css">  -->

        <script src="<?php echo base_url(); ?>assets/new_ui/js/jquery.min.js"></script>
        <link href="<?php echo base_url(); ?>assets/new_ui/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/select2/select2.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_ui/plugins/select2/select2.min.css">
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/iCheck/icheck.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_ui/plugins/iCheck/all.css">
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/sweetalert2/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_ui/plugins/sweetalert2/dist/sweetalert.min.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/buttons.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/responsive.bootstrap4.min.css">
        <!-- <script src="<?php echo base_url(); ?>assets/new_ui/js/pace.min.js"></script> -->

        

        <script>
            jQuery(document).ready(function($) { 
                $('[data-mask]').inputmask()
                $('.select2').select2() 
                $(".delete").click(function(e){
                    e.preventDefault();
                    var linkURL = this.href;
                    warnBeforeRedirect(linkURL);
                });
                function warnBeforeRedirect(linkURL) {
                    swal({
                        title: "<?php echo lang('alert') ?>!",
                        text: "<?php echo lang('are_you_sure') ?>?",
                        cancelButtonText:'<?php echo lang('cancel'); ?>',
                        confirmButtonText:'<?php echo lang('ok'); ?>',
                        confirmButtonColor: '#3c8dbc',
                        showCancelButton: true
                    }, function() {
                        window.location.href = linkURL;
                    });
                }
                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass   : 'iradio_minimal-blue'
                })


                $(document).on('keydown', '.integerchk', function(e){ 
                    return (
                    keys == 8 ||
                        keys == 9 ||
                        keys == 13 ||
                        keys == 46 ||
                        keys == 110 ||
                        keys == 86 ||
                        keys == 190 ||
                        (keys >= 35 && keys <= 40) ||
                        (keys >= 48 && keys <= 57) ||
                        (keys >= 96 && keys <= 105));
                });

                $(document).on('keyup', '.integerchk', function(e){
                    var input = $(this).val();
                    var ponto = input.split('.').length;
                    var slash = input.split('-').length;
                    if (ponto > 2)
                        $(this).val(input.substr(0,(input.length)-1));
                    $(this).val(input.replace(/[^0-9]/,''));
                    if(slash > 2)
                        $(this).val(input.substr(0,(input.length)-1));
                    if (ponto ==2)
                        $(this).val(input.substr(0,(input.indexOf('.')+3)));
                    if(input == '.')
                        $(this).val("");

                });
                
            });
        </script>
        <script>  
            $(function () {

                //Date picker
                $('#date').datepicker({
                    format: 'yyyy-mm-dd',
                    orientation: "bottom",
                    todayHighlight: true,
                    todayBtn: true,
                    autoclose: true
                });
                $('#dates2').datepicker({
                    format: 'yyyy-mm-dd',
                    orientation: "bottom",
                    todayHighlight: true,
                    todayBtn: true,
                    autoclose: true
                });
                $('.customDatepicker').datepicker({
                    format: 'yyyy-mm-dd',
                    orientation: "bottom",
                    todayHighlight: true,
                    todayBtn: true,
                    autoclose: true
                });
                $(".datepicker_months").datepicker({
                    format: 'yyyy-M',
                    orientation: "bottom",
                    autoclose: true,
                    viewMode: "months",
                    minViewMode: "months"
                });
            })

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd = '0'+dd
            } 

            if(mm<10) {
                mm = '0'+mm
            }  
            today = yyyy + '-' + mm + '-' + dd;
            
        </script>

    </head>
    <div id="loading">
        <img id="loading-image" src="<?php echo base_url(); ?>assets/iconss.gif" alt="Loading..." />
    </div>
    <body>
        
        <div class="topbar">
            <div class="topbar-main">
                <div class="container-fluid">
                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="<?php echo base_url(); ?>" class="logo">
                            <span>
                                <img src="<?php echo base_url(); ?>assets/images/logo_white.png">
                            </span>
                        </a>
                    </div>
                    <!-- Navbar -->
                    <nav class="navbar-custom">
                        
                        <ul class="list-unstyled topbar-nav float-right mb-0">
                            <?php if ($this->session->userdata('outlet_id')) { ?>
                                <li class="hidden-sm" id="close_register_button" style="display:none">
                                    <a class="nav-link waves-effect waves-light" href="#" id="register_close">
                                        <i class="  mdi mdi-window-close mr-2"></i>Tutup Penjualan 
                                    </a>
                                </li>
                            <?php }?>
                            
                            <li>
                                <a
                                    class="nav-link waves-effect waves-light"
                                    href="javascript:void(0);"
                                    id="btn-fullscreen">
                                    <i class="mdi mdi-fullscreen" style="height:20px;"></i> Full Screen
                                </a>
                            </li>

                            <li class="dropdown">
                            <a
                                class="nav-link dropdown-toggle waves-effect waves-light nav-user"
                                data-toggle="dropdown"
                                href="#"
                                role="button"
                                aria-haspopup="false"
                                aria-expanded="false">
                                
                                    <span class="ml-1 nav-user-name hidden-sm"><?php echo $this->session->userdata('full_name'); ?>
                                        <i class="mdi mdi-chevron-down"></i>
                                    </span>
                                </a>
                                <div
                                    class="dropdown-menu dropdown-menu-right"
                                    x-placement="bottom-end"
                                    style="position: absolute; transform: translate3d(-35px, 70px, 0px); top: 0px; left: 0px; will-change: transform;">
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>Authentication/changeProfile">
                                        <i class="dripicons-user text-muted mr-2"></i>
                                        Profile</a>
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>Authentication/changePassword">
                                        <i class="dripicons-lock text-muted mr-2"></i>
                                        Ganti Password</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>Authentication/logOut">
                                        <i class="dripicons-exit text-muted mr-2"></i>
                                        Logout</a>
                                </div>
                            </li>

                            
                            
                            <li class="menu-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link" id="mobileToggle">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span></div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>
                        </ul>

                        <ul class="list-unstyled topbar-nav mb-0">
                        <?php if ($this->session->userdata('outlet_id')) { ?>

                            <?php if (in_array('Sale', $this->session->userdata('menu_access'))) { ?> 
                                <li class="hidden-sm">
                                    <a class="nav-link waves-effect waves-light" href="<?php echo base_url(); ?>Sale/POS" id="bukakasir">
                                    <i class=" mdi mdi-food-fork-drink mr-2"></i>Buka Kasir</a>
                                </li>
                            <?php } ?>

                            <li class="hidden-sm">
                                <a
                                    class="nav-link dropdown-toggle waves-effect waves-light nav-user"
                                    data-toggle="dropdown"
                                    href="#"
                                    role="button"
                                    aria-haspopup="false"
                                    aria-expanded="false">
                                    <i class="mdi mdi-library-plus mr-2"></i>Ringkasan
                                    <i class="mdi mdi-chevron-down"></i>
                                </a>

                                <div
                                    class="dropdown-menu"
                                    x-placement="bottom-start"
                                    style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(129px, 70px, 0px);">
                                    <!-- item-->
                                    <?php if ($this->session->userdata('role') == "Admin") { ?>
                                    <a href="#" onclick="todaysSummary();" class="dropdown-item">Ringkasan hari ini
                                    </a>
                                    <?php }?>
                                    <!-- item-->
                                    <a href="#" id="register_details" class="dropdown-item">Rincian Buka Outlet</a>
                                </div>
                            </li>
                                    
                                    

                        <?php }?>
                        </ul>
                    </nav>
                    <!-- end navbar-->
                </div>
            </div>
            <!--end topbar-main-->
            <!-- MENU Start -->
            <div class="navbar-custom-menu">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <?php if (in_array('Dashboard', $this->session->userdata('menu_access'))) { ?> 
                                <li class="has-submenu <?php if($this->uri->segment(3)=="dashboard"){echo "active";}?>">
                                    <a href="<?php echo base_url(); ?>Dashboard/dashboard"><i class="mdi mdi-view-dashboard"></i> <span>Dashboard</span></a>
                                </li>

                                <li class="has-submenu">
                                    <a href="#"> <i class="mdi mdi-folder-multiple"></i>Master</a>
                                    <ul class="submenu">

                                        <li class="<?php if($this->uri->segment(3)=="setting"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Restaurant_setting/setting">RePOS Setting</a>
                                        </li>

                                        <li class="<?php if($this->uri->segment(3)=="tables"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Master/tables">Manajemen Meja</a>
                                        </li>

                                        <li class="<?php if($this->uri->segment(3)=="foodMenuCategories"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Master/foodMenuCategories">Unit Usaha</a>
                                        </li>

                                        <li class="<?php if($this->uri->segment(3)=="VATs"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Master/VATs">Manajemen Pajak</a>
                                        </li>

                                        <!-- <li class="<?php if($this->uri->segment(3)=="users"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>User/users">Manajemen Pengguna</a>
                                        </li> -->

                                        <li class="has-submenu <?php if($this->uri->segment(3)=="wastes" && $this->uri->segment(3)=="wasteReport" ){echo "active";}?>">
                                            <a href="#">Manajemen Limbah</a>
                                            <ul class="submenu">
                                                <li <?php if($this->uri->segment(3)=="wastes"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Waste/wastes">Data Limbah</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="wasteReport"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Report/wasteReport">Rekap</a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                    </ul>
                                </li>

                                <li class="has-submenu">
                                    <a href="#"><i class="mdi mdi-cart-outline"></i>Pengeluaran</a>
                                    <ul class="submenu">
                                        <li class="has-submenu <?php if($this->uri->segment(2)=="purchases" && $this->uri->segment(3)=="purchaseReportByDate" && $this->uri->segment(3)=="addEditPurchase"){echo "active";}?>">
                                            <a href="#">Pembelian</a>
                                            <ul class="submenu">
                                                <li <?php if($this->uri->segment(3)=="addEditPurchase"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Purchase/addEditPurchase">Tambah Pembelian Bahan Baku</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="purchases" && $this->uri->segment(3)=="purchaseReportByDate"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Purchase/purchases">Pembelian Bahan Baku</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="purchaseReportByDate"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Report/purchaseReportByDate">Rekap Data Pembelian</a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="has-submenu <?php if($this->uri->segment(3)=="expenseItems" && $this->uri->segment(3)=="expenses" && $this->uri->segment(3)=="expenseReport" ){echo "active";}?>">
                                            <a href="#">Operasional</a>
                                            <ul class="submenu">
                                                <li <?php if($this->uri->segment(3)=="expenseItems"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Master/expenseItems">Kategori Operasional</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="expenses"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Expense/expenses">Daftar Operasional</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="expenseReport"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Report/expenseReport">Rekap Data Operasional</a>
                                                </li>


                                            </ul>
                                        </li>

                                        <li class="has-submenu <?php if($this->uri->segment(3)=="suppliers" && $this->uri->segment(3)=="addEditSupplier" && $this->uri->segment(3)=="supplierPayments" && $this->uri->segment(3)=="addSupplierPayment" && $this->uri->segment(3)=="supplierDueReport" && $this->uri->segment(3)=="supplierReport"  ){echo "active";}?>">
                                            <a href="#">Suppliers</a>
                                            <ul class="submenu">
                                                <li <?php if($this->uri->segment(3)=="suppliers" && $this->uri->segment(3)=="addEditSupplier"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Master/suppliers">Data Suppliers</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="supplierPayments" && $this->uri->segment(3)=="addSupplierPayment"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>SupplierPayment/supplierPayments">Piutang Suppliers</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="supplierDueReport"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Report/supplierDueReport">Rekap Piutang Supplier</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="supplierReport"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Report/supplierReport">Legder Suppliers</a>
                                                </li>




                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="has-submenu">
                                    <a href="#"> <i class="mdi mdi-coffee-to-go"></i>Penjualan</a>
                                    <ul class="submenu">
                                        <li class="<?php if($this->uri->segment(3)=="sales"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Sale/sales">Data Penjualan</a>
                                        </li>

                                        <li class="has-submenu <?php if($this->uri->segment(3)=="saleReportByDate" && $this->uri->segment(3)=="foodMenuSales" && $this->uri->segment(3)=="detailedSaleReport" ){echo "active";}?>">
                                            <a href="#">Rekap</a>
                                            <ul class="submenu">
                                                <li <?php if($this->uri->segment(3)=="saleReportByDate"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Report/saleReportByDate">Rekap Data Harian</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="foodMenuSales"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Report/foodMenuSales">Rekap Menu Terjual</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="detailedSaleReport"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Report/detailedSaleReport">Detail Rekap Data Penjualan</a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                    </ul>
                                </li>

                                <li class="has-submenu">
                                    <a href="#"> <i class="mdi mdi-food"></i>Manajemen Menu</a>
                                    <ul class="submenu">
                                        <li class="<?php if($this->uri->segment(3)=="foodMenus"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Master/foodMenus">Daftar Menu</a>
                                        </li>

                                        <li class="has-submenu <?php if($this->uri->segment(3)=="ingredientCategories" && $this->uri->segment(3)=="Units" && $this->uri->segment(3)=="ingredients" ){echo "active";}?>">
                                            <a href="#">Bahan Baku</a>
                                            <ul class="submenu">
                                                <li <?php if($this->uri->segment(3)=="ingredients"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Master/ingredients">Bahan Baku</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="ingredientCategories"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Master/ingredientCategories">Kategori Bahan</a>
                                                </li>
                                                <li <?php if($this->uri->segment(3)=="Units"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Master/Units">Satuan Bahan</a>
                                                </li>
                                                <!-- <li <?php if($this->uri->segment(3)=="modifiers"){echo "active";}?>">
                                                    <a href="<?php echo base_url(); ?>Master/modifiers">Modifier</a>
                                                </li> -->
                                            </ul>
                                        </li>
                                        
                                    </ul>
                                </li>

                                <li class="has-submenu">
                                    <a href="#"> <i class="mdi mdi-store"></i>Inventory</a>
                                    <ul class="submenu">
                                        <li class="<?php if($this->uri->segment(2)=="Inventory"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Inventory/index">Data Inventory</a>
                                        </li>
                                        <li class="<?php if($this->uri->segment(2)=="Inventory"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Inventory/getInventoryAlertList">Bahan Minim Stock</a>
                                        </li>
                                        <!-- <li class="<?php if($this->uri->segment(3)=="inventoryReport"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Report/inventoryReport">Rekap Data Inventory</a>
                                        </li> -->
                                        <li class="<?php if($this->uri->segment(2)=="Inventory_adjustment"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Inventory_adjustment/inventoryAdjustments">Inventory Adjustment</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="has-submenu">
                                    <a href="#"> <i class="mdi mdi-note-text"></i>Laporan</a>
                                    <ul class="submenu">
                                        <li class="<?php if($this->uri->segment(3)=="registerReport"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Report/registerReport">Data Buka Tutup Transaksi</a>
                                        </li>
                                        <li class="<?php if($this->uri->segment(3)=="dailySummaryReport"){echo "active";}?>">
                                            <a href="<?php echo base_url(); ?>Report/dailySummaryReport">Laporan</a>
                                        </li>
                                    </ul>
                                </li>
                                

                            <?php } ?>
                            
                            
                        </ul>
                        <!-- End navigation menu -->
                    </div>
                    <!-- end navigation -->
                </div>
                <!-- end container-fluid -->
            </div>
            <!-- end navbar-custom -->
        </div>
        <!-- Top Bar End -->
        <!-- Page Content-->
        <div class="wrapper">
        <?php
            if (isset($main_content)) {
                echo $main_content;
            }
            
        ?> 
            <footer class="footer text-center text-sm-left">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">© <?php echo date("Y")?> <a href="https://repos.gomaya.id" target="_blank"> RePOS</a> v 2.0
                            <span class="text-muted d-none d-sm-inline-block float-right">Crafted with
                                <i class="mdi mdi-heart text-danger"></i>
                                by <a href="https://www.gomaya.id" target="_blank"> gomaya.id</a></span></div>
                    </div>
                </div>
            </footer>
        </div>



        <div class="modal fade" id="todaysSummary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="ShortCut">
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <h5 class="modal-title align-self-center mt-0" id="mySmallModalLabel"><?php echo lang('todays_summary'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="box-body table-responsive">
                            <table class="table">
                                <tr>
                                    <td style="width: 80%;"><?php echo lang('purchase'); ?>(<?php echo lang('only_paid_amount'); ?>)</td>
                                    <td><span id="purchase"></span></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('sale'); ?>(<?php echo lang('only_paid_amount'); ?>)</td>
                                    <td><span id="sale"></span></td>
                                </tr>
                                <!-- <tr>
                                    <td><?php echo lang('total'); ?> <?php echo lang('vat'); ?></td>
                                    <td><span id="totalVat"></span></td>
                                </tr> -->
                                <tr>
                                    <td>Operasional</td>
                                    <td><span id="Expense"></span></td>
                                </tr>
                                <tr>
                                    <td>Piutang Supplier</td>
                                    <td><span id="supplierDuePayment"></span></td>
                                </tr>
                                <!-- <tr>
                                    <td><?php echo lang('customer_due_receive'); ?></td>
                                    <td><span id="customerDueReceive"></span></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('waste'); ?></td>
                                    <td><span id="waste"></span></td>
                                </tr> -->
                                <!-- <tr>
                                    <td>Balance = (<?php echo lang('sale'); ?> + <?php echo lang('customer_due_receive'); ?>) - (<?php echo lang('purchase'); ?> + <?php echo lang('supplier_due_payment'); ?> + <?php echo lang('expense'); ?>)</td>
                                    <td><span id="balance"></span></td>
                                </tr> -->
                            </table>

                            <br>
                            <div id="showCashStatus"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./wrapper -->

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <h5 class="modal-title align-self-center mt-0" id="mySmallModalLabel"><?php echo lang('register_details'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" id="register_details_body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('close'); ?></button>
                    </div>
                </div>

            </div>
        </div>
        
        <!-- end page-wrapper -->
        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/new_ui/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/js/waves.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/js/jquery.slimscroll.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/jszip.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/pdfmake.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/vfs_fonts.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/buttons.html5.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/buttons.print.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/buttons.colVis.min.js"></script>
        
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/new_ui/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/new_ui/js/app.js"></script>

        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/POS/js/jquery.cookie.js"></script>
        <script>
            
            var currency = "<?php echo $this->session->userdata('currency') ?>";
            var base_url = "<?php echo base_url(); ?>"
            var keluar   = "<?php echo base_url(); ?>Authentication/logOut";

            $.ajax({
                url: '<?php echo base_url("Register/checkRegisterAjax") ?>',
                method:"POST",
                data:{
                    csrf_test_name: $.cookie('csrf_cookie_name')
                },
                success:function(response) {
                    if(response=='2'){
                        $('#close_register_button').css('display','none');
                    }else{
                        $('#close_register_button').css('display','block');

                    }
                },
                error:function(){
                    alert("error");
                }
            });
            
            function tutupotomatis(){
                $.ajax({
                url: '<?php echo base_url("Sale/closeRegister") ?>',
                method:"POST",
                data:{
                    csrf_test_name: $.cookie('csrf_cookie_name')
                },
                success:function(response) {
                    location.href = keluar;
                },
                error:function(){
                    alert("error");
                }
                });
            }

            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            var time = 23 + ":" + 59 + ":" + 58;
            var dateTime = date+' '+time;
            var countDownDate = new Date(dateTime).getTime();

            setInterval(function() {
                var now = new Date().getTime();
                var distance = countDownDate - now;
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                if (distance < 0) {
                    $(document).ready(function() {
                        tutupotomatis();
                    }); 
                    
                }

            }, 1000);
            

            $('#register_details').on('click',function(){
                $.ajax({
                    url: '<?php echo base_url("Sale/registerDetailCalculationToShowAjax") ?>',
                    method:"POST",
                    data:{
                        csrf_test_name: $.cookie('csrf_cookie_name')
                    },
                    success:function(response) {
                        console.log(response);
                        if(!IsJsonString(response)){
                            var r = confirm("Transaksi Belum dibuka, anda yang ingin membuka transaksi?");
                            if (r == true) {
                                window.location.replace(base_url+'Register/openRegister');
                            }
                            return false;    
                        }

                        response = JSON.parse(response);
                        $('#myModal').modal('show');
                        $('#opening_closing_register_time').show();
                        $('#opening_register_time').html(response.opening_date_time);
                        

                        var t1 = response.opening_date_time.split(/[- :]/);
                        var d1 = new Date(Date.UTC(t1[0], t1[1]-1, t1[2], t1[3], t1[4], t1[5]));
                        var t2 = response.closing_date_time.split(/[- :]/);
                        var d2 = new Date(Date.UTC(t2[0], t2[1]-1, t2[2], t2[3], t2[4], t2[5]));

                        if(d1>d2){
                            $('#closing_register_time').html('<?php echo lang('not_closed_yet'); ?>');
                        }else{
                            $('#closing_register_time').html(response.closing_date_time);
                        }

                        
                        var register_detail_modal_content = '';
                        var customer_due_receive = (response.customer_due_receive==null)?0:response.customer_due_receive;
                        var opening_balance = (response.opening_balance==null)?0:response.opening_balance;
                        var sale_due_amount = (response.sale_due_amount==null)?0:response.sale_due_amount;
                        var sale_in_card = (response.sale_in_card==null)?0:response.sale_in_card;
                        var sale_in_cash = (response.sale_in_cash==null)?0:response.sale_in_cash;
                        var sale_in_paypal = (response.sale_in_paypal==null)?0:response.sale_in_paypal;
                        var sale_paid_amount = (response.sale_paid_amount==null)?0:response.sale_paid_amount;
                        var sale_total_payable_amount = (response.sale_total_payable_amount==null)?0:response.sale_total_payable_amount;

                        var balance = (parseFloat(opening_balance)+parseFloat(sale_paid_amount)+parseFloat(customer_due_receive)).toFixed(0);
                        register_detail_modal_content += '<p>Saldo Awal: '+currency+' '+opening_balance+'</p>';
                        // register_detail_modal_content += '<p>Sale Total Amount: '+currency+' '+sale_total_payable_amount+'</p>';
                        register_detail_modal_content += '<p><?php echo lang('sale'); ?> (Selesai): '+currency+' '+sale_paid_amount+'</p>';
                        // register_detail_modal_content += '<p>Sale Due Amount: '+currency+' '+sale_due_amount+'</p>';
                        // register_detail_modal_content += '<p>&nbsp;</p>';
                        // register_detail_modal_content += '<p><?php echo lang('customer_due_receive'); ?>: '+currency+' '+customer_due_receive+'</p>';
                        register_detail_modal_content += '<p>Balance (Saldo Awal + Penjualan (Selesai)): '+currency+' '+balance+'</p>';
                        register_detail_modal_content += '<p style="width:100%;border-bottom:1px solid #b5d6f6;line-height:0px;">&nbsp;</p>';

                        register_detail_modal_content += '<p><?php echo lang('sale'); ?> <?php echo lang('in'); ?> <?php echo lang('cash'); ?>: '+currency+' '+sale_in_cash+'</p>';
                        // register_detail_modal_content += '<p><?php echo lang('sale'); ?> <?php echo lang('in'); ?> <?php echo lang('paypal'); ?>: '+currency+' '+sale_in_paypal+'</p>';
                        // register_detail_modal_content += '<p><?php echo lang('sale'); ?> <?php echo lang('sale'); ?> <?php echo lang('card'); ?>: '+currency+' '+sale_in_card+'</p>';
                        
                        // register_detail_modal_content += '<p style="width:100%;border-bottom:1px solid #b5d6f6;line-height:0px;">&nbsp;</p>';
                        // register_detail_modal_content += '<p>Balance: '+currency+' '+balance+'</p>';

                        
                        $('#register_details_body').html(register_detail_modal_content);
                        // $('#myModal').modal('hide');

                    },
                    error:function(){
                        alert("error");
                    }
                });
            });

            $('#register_close').on('click',function(){
            

                swal({
                title: "Tutup penjualan",
                text: "Kamu yakin ingin menutup penjualan ?",
                type: "warning", 
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                cancelButtonText: "Batal",
                confirmButtonText: "Tutup",
                closeOnConfirm: false,
                closeOnCancel: false
                }, 

                function(isConfirm) {
                if (isConfirm) { 
                    $.ajax({
                        url: '<?php echo base_url("Sale/closeRegister"); ?>',
                        method:"POST",
                        data:{
                            csrf_test_name: $.cookie('csrf_cookie_name')
                        },
                        success:function(response) {
                            swal({
                                title: 'Alert',
                                text: 'Transaksi penjualan berhasil ditutup',
                                confirmButtonColor: '#b6d6f6' 
                            });
                            $('#close_register_button').hide();

                        },
                        error:function(){
                            alert("error");
                        }
                    });
                } else {
                    swal("Batal", "Transaksi penjualan masih dibuka :)", "info");
                }
                });
            
            });
            
            
            function IsJsonString(str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            }
            function todaysSummary() {
                $.ajax({
                    url     : '<?php echo base_url('Report/todayReport') ?>',
                    method  : 'get',
                    dataType: 'json',
                    data    : {},
                    success:function(data){
                        $("#purchase").text("<?php echo $this->session->userdata('currency') ?> "+data.total_purchase_amount);
                        $("#sale").text("<?php echo $this->session->userdata('currency') ?> "+data.total_sales_amount);
                        $("#totalVat").text("<?php echo $this->session->userdata('currency') ?> "+data.total_sales_vat);
                        $("#Expense").text("<?php echo $this->session->userdata('currency') ?> "+data.expense_amount);
                        $("#supplierDuePayment").text("<?php echo $this->session->userdata('currency') ?> "+data.supplier_payment_amount);
                        $("#customerDueReceive").text("<?php echo $this->session->userdata('currency') ?> "+data.customer_receive_amount);
                        $("#waste").text("<?php echo $this->session->userdata('currency') ?> "+data.total_loss_amount);
                        $("#balance").text("<?php echo $this->session->userdata('currency') ?> "+data.balance);
                        $.ajax({
                            url     : '<?php echo base_url('Report/todayReportCashStatus') ?>',
                            method  : 'get',
                            datatype: 'json',
                            data    : {},
                            success:function(data){
                                var json = $.parseJSON(data);
                                var i = 1;
                                var html = '<table class="table">';
                                $.each(json, function (index, value) {
                                    html+='<tr><td style="width: 86%">'+i+'. Sale in '+value.name+'</td> <td><?php echo $this->session->userdata('currency') ?> '+value.total_sales_amount+'</td></tr>';
                                    i++;
                                });
                                html+='</table>';
                                $("#showCashStatus").html(html);
                            }
                        });
                    }
                });
                $("#todaysSummary").modal("show");
            }


        </script>

        <script>
            $(document).ready(function() {
                $("#loading").hide();
            });
            $("#bukakasir").click(function(){
                $("#loading").show();
            }); 
        </script>
    </body>
</html>
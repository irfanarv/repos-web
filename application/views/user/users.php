<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                      <a href="<?php echo base_url() ?>User/addEditUser"><button type="button" class="btn btn-block btn-primary pull-right">Tambah pengguna baru</button></a>
                  </div>
                  <h4 class="page-title">Manajemen Pengguna</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 1%">No</th>
                                <th style="width: 23%" ><?php echo lang('name'); ?></th>
                                <th style="width: 16%"><?php echo lang('designation'); ?></th>
                                <th style="width: 26%"><?php echo lang('email'); ?></th>
                                <th style="width: 17%"><?php echo lang('status'); ?></th>
                                
                                <th style="width: 20%; text-align: center"><?php echo lang('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($users && !empty($users)) {
                                $i = count($users);
                            }
                            foreach ($users as $usrs) {
                                if ($usrs->id != $this->session->userdata['user_id']):
                                    ?>
                                    <tr>
                                        <td style="text-align: center"><?php echo $i--; ?></td>
                                        <td><?php echo $usrs->full_name; ?></td>
                                        <td><?php echo $usrs->designation; ?></td>
                                        <td><?php echo $usrs->email_address; ?></td>
                                        <td><?php echo $usrs->active_status; ?></td>
                                        
                                        <td style="text-align: center">
                                        <div class="">
                                            <a href="<?php echo base_url() ?>User/addEditUser/<?php echo $this->custom->encrypt_decrypt($usrs->id, 'encrypt'); ?>" >
                                                <i class="fa fa-edit mr-2"></i>
                                            </a>
                                            <?php if ($usrs->active_status == 'Active') { ?>
                                                <a href="<?php echo base_url() ?>User/deactivateUser/<?php echo $this->custom->encrypt_decrypt($usrs->id, 'encrypt'); ?>" >
                                                    <i class="fas fa-lock mr-2"></i>
                                                </a>
                                                <?php } else { ?>
                                                    <a href="<?php echo base_url() ?>User/activateUser/<?php echo $this->custom->encrypt_decrypt($usrs->id, 'encrypt'); ?>" >
                                                    <i class="fa fa-lock-open mr-2"></i>
                                                </a> 
                                            <?php } ?>
                                            <?php if ($usrs->role != 'Admin') { ?>
                                                <a class="delete" href="<?php echo base_url() ?>User/deleteUser/<?php echo $this->custom->encrypt_decrypt($usrs->id, 'encrypt'); ?>" >
                                                    <i class="fa fa-trash text-danger mr-2"></i>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </td>  

                                        
                                    </tr>
                                    <?php
                                endif;
                            }
                            ?>
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    $(function () {
        $('#datatable').DataTable({
            'autoWidth'   : false,
            'ordering'    : false
        })
    })
</script>


<div class="container-fluid">
    <div class="row">
          <div class="col-sm-12">
              <div class="page-title-box">
                  <div class="float-right">
                      <!-- <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="javascript:void(0);">Pages</a>
                          </li>
                          <li class="breadcrumb-item active">Starter</li>
                      </ol> -->
                  </div>
                  <h4 class="page-title">Detail Pembelian Bahan Baku</h4>
              </div>
          </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="box box-primary">

<div class="box-body">
    <div class="row">
        <div class="col-md-3"> 
            <div class="form-group">
                <h3><?php echo lang('ref_no'); ?></h3>
                <p class=""><?php echo $purchase_details->reference_no; ?></p>
            </div> 
        </div>

        <div class="col-md-3"> 
            <div class="form-group"> 
                <h3>Unit</h3>
                <?php echo getRestoNameById($purchase_details->menu_resto_id); ?>
            </div>  
        </div>

        <div class="col-md-3"> 
            <div class="form-group"> 
                <h3><?php echo lang('supplier'); ?></h3>
                <?php echo getSupplierNameById($purchase_details->supplier_id); ?>
            </div>  
        </div>

        <div class="col-md-3"> 
            <div class="form-group">
                <h3>Tanggal Pembelian</h3>
                <p class=""><?php echo date($this->session->userdata('date_format'), strtotime($purchase_details->date)); ?></p>
            </div> 
        </div>

        <div class="col-md-3"> 
            <div class="form-group"> 
                <h3>Bahan Baku</h3> 
            </div> 
        </div> 
    </div>  
    <div class="row">
        <div class="col-md-12"> 
            <div class="table-responsive" id="purchase_cart">          
                <table class="table">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="20%">Bahan Baku(<?php echo lang('code'); ?>)</th>
                            <th width="15%">Harga</th>
                            <th width="15%">Qty/Jumlah</th>
                            <th width="20%"><?php echo lang('total'); ?></th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        if ($purchase_ingredients && !empty($purchase_ingredients)) {
                            foreach ($purchase_ingredients as $pi) {
                                $i++;
                                echo '<tr id="row_' . $i . '">' .
                                '<td style="width: 10%; padding-left: 10px;"><p>' . $i . '</p></td>' .
                                '<td style="width: 20%"><span style="padding-bottom: 5px;">' . getIngredientNameById($pi->ingredient_id) . ' (' . getIngredientCodeById($pi->ingredient_id) . ')</span></td>' .
                                '<td style="width: 15%">' . $this->session->userdata('currency') . " " . number_format($pi->unit_price,0) . '</td>' .
                                '<td style="width: 15%">' . number_format($pi->quantity_amount,0) . ' ' . unitName(getUnitIdByIgId(number_format($pi->ingredient_id,0))) . '</td>' .
                                '<td style="width: 20%">' . $this->session->userdata('currency') . " " . number_format($pi->total,0) . '</td>' .
                                '</tr>'
                                ;
                            }
                        }
                        ?>

                    </tbody>
                </table>
            </div> 
        </div> 
    </div> 

    <div class="row">
        <div class="col-md-9">

        </div>
        <div class="col-md-offset-2 col-md-3">
            <div class="form-group">
                <h3><?php echo lang('g_total'); ?></h3>
                <p class=""><?php echo $this->session->userdata('currency') . " " . number_format($purchase_details->grand_total,0) ?></p>
            </div>
            <div class="form-group">
                <h3>Lunas</h3>
                <p class=""><?php echo $this->session->userdata('currency') . " " . number_format($purchase_details->paid,0) ?></p>
            </div>
            <div class="form-group">
                <h3><?php echo lang('due'); ?></h3>
                <p class=""><?php echo $this->session->userdata('currency') . " " . number_format($purchase_details->due,0) ; ?></p>

            </div>
        </div> 
        
    </div>

    <div class="row"> 
        <div class="col-md-offset-6 col-md-3">

        </div>  
        <div class="col-md-3">

        </div> 
    </div>

</div> 
<div class="box-footer"> 
    <a href="<?php echo base_url() ?>Purchase/addEditPurchase/<?php echo $encrypted_id; ?>"><button type="button" class="btn btn-primary"><?php echo lang('edit'); ?></button></a>
    <a href="<?php echo base_url() ?>Purchase/purchases"><button type="button" class="btn btn-primary"><?php echo lang('back'); ?></button></a>
</div>
<?php echo form_close(); ?> 
</div> 
                </div>
            </div>
        </div>
    </div>


</div>

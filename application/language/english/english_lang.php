<?php
/******************* Common*************************/
$lang['enter'] = 'Masuk';
$lang['view_details'] = 'Lihat Rincian';
$lang['edit'] = 'Edit';
$lang['add'] = 'Tambah';
$lang['delete'] = 'Hapus';
$lang['address'] = 'Alamat'; 
$lang['phone'] = 'No Telp'; 
$lang['started_date'] = 'Tanggal Mulai';
$lang['yes'] = 'Iya';
$lang['no'] = 'Tidak';
$lang['submit'] = 'Submit';
$lang['back'] = 'Kembali';
$lang['alert'] = 'Peringatan';
$lang['are_you_sure'] = 'Kamu Yakin';
$lang['sn'] ='SN';
$lang['actions']='Aksi';
$lang['ok'] = 'OK';
$lang['cancel'] = 'Batal';
$lang['insertion_success']="Informasi telah berhasil ditambahkan!";
$lang['update_success']="Informasi telah berhasil diperbarui!";
$lang['delete_success']="Informasi telah berhasil dihapus!";
$lang['please_click_green_button']="Please click on green Enter button of an outlet";
$lang['register_not_open']="Aplikasi belum dibuka, masukan jumlah saldo untuk membuka aplikasi dan mulai bertransaksi";
$lang['user_not_active'] = 'User tidak aktif';
$lang['incorrect_email_password'] = 'Email/Password salah';
$lang['password_changed'] = 'Password berhasil diperbarui!';
$lang['old_password_not_match'] = 'Password lama tidak sama!';
$lang['back'] = 'Kembali'; 

/******************* Two Menus*************************/
$lang['restaurant_setting'] = 'Pengaturan'; 
$lang['all_screen'] = 'Akses ke layar'; 

/******************* Outlet*************************/
$lang['outlets'] = 'Outlets';
$lang['add_outlet'] ='Add Outlet';
$lang['edit_outlet'] ='Restaurant Setting';
$lang['outlet_code'] ='Outlet Code';
$lang['outlet_name'] ='Restaurant Name';
$lang['not_login'] = 'Untuk ditamplikan di struk'; 
$lang['invoice_footer'] = 'Invoice Footer';
$lang['collect_vat'] = 'Kumpulkan PPN';
$lang['collect_tax'] = 'Saya Mengumpulkan Pajak';
$lang['show_invoice_sample'] = 'Tampilkan invoice sampel dengan pajak';
$lang['my_tax_title'] = 'Judul Pajak';
$lang['how_tax_title_works'] = 'Show how Tax Title works';
$lang['tax_registration_no'] = 'My Tax Registration No';
$lang['tax_is_gst'] = 'My Tax is GST';
$lang['if_i_say_yes'] = 'What will happen if I say Yes';
$lang['state_code'] = 'State Code';
$lang['my_tax_fields'] = 'My Tax Fields';
$lang['add_more'] = 'Add More';
$lang['how_tax_fields_work'] = 'Show How tax fields work';
$lang['vat_registration_no'] = 'VAT Registration Number';
$lang['pre_or_post_payment'] = 'Pre or Post Payment';
$lang['post_payment'] ='Post Payment';
$lang['pre_payment'] ='Pre Payment';
$lang['open_register'] ='Buka Transaksi';
$lang['opening_balance'] ='Saldo Pembukaan';


/******************* Dashboard*************************/
$lang['dashboard'] = 'Dashboard';
$lang['business_intelligence'] = '';
$lang['food_items'] = 'Item Makanan';
$lang['manage'] = 'Kelola';
$lang['ingredients'] = 'Bahan - bahan';
$lang['customers'] = 'Customer';
$lang['employees'] = 'Karyawan';
$lang['quick_links'] = 'Pintasan';
$lang['food_menu'] = 'Menu Makanan';
$lang['supplier_payment'] = 'Pembayaran Supplier';
$lang['pos'] = 'Kasir';
$lang['expense'] = 'Operasional';
$lang['purchase'] = 'Pembelian';
$lang['daily_summary_report'] = 'Rekap Ringkasan Harian';
$lang['register_report'] = 'Rekap Buka Tutup Transaksi';
$lang['profit_loss_report'] = 'Rekap Rugi Laba';
$lang['sales_report'] = 'Rekap Penjualan';
$lang['food_sales_report'] = 'Rekap Penjulan Makanan';
$lang['send_sms'] = 'Send SMS';
$lang['inventory'] = 'Inventory';
$lang['inventory_adjustment'] = 'Penyesuian Inventory';
$lang['customer_receive'] = 'Customer Receive';
$lang['attendance'] = 'Absensi';
$lang['dine'] = 'Makan ditempat';
$lang['take_away'] = 'Take Away';
$lang['delivery'] = 'Ojek Online';
$lang['this_month'] = 'Bulan ini';
$lang['operational_comparision'] = 'Perbandingan Global';
$lang['ingredients_alert'] = '';
$lang['low_stock'] = 'Stok Minim';
$lang['ingredient_name'] = 'Nama Bahan';
$lang['current_stock'] = 'Stok Saat Ini';
$lang['top_ten_food_this_month'] = '10 Makanan Teratas Bulan Ini';
$lang['food_name'] ='Nama makanan';
$lang['count'] ='Hitung';
$lang['top_ten_customers'] ='10 Customer Terbaik Bulan Ini';
$lang['customer_name'] ='Nama Customer';
$lang['sale_amount'] ='Jumlah penjualan';
$lang['customer_receiveable'] ='Piutang Customer';
$lang['due_amount'] ='Jumlah Tagihan';
$lang['supplier_payable'] ='Piutang Supplier';
$lang['supplier_name'] ='Nama Supplier';
$lang['monthly_sales_comparision'] ='Perbandingan Penjualan Global Bulanan';
$lang['cust_rcv'] ='Cust Rcv';
$lang['supp_pay'] ='Supp Pay';

/*******************Purchase*************************/
$lang['purchases']='Pembelian';
$lang['add_purchase']='Tambah Pembelian';
$lang['ref_no']='No RF';
$lang['date']='Tanggal';
$lang['supplier']='Supplier';
$lang['g_total']='Total';
$lang['due']='Sisa';
$lang['added_by']='Ditambahkan oleh';

$lang['unit_price']='Unit Harga';
$lang['supplier_field_required']='Wajib diisi.';
$lang['date_field_required']='Wajib diisi.';
$lang['paid_field_required']='Wajib diisi.';
$lang['edit_purchase']='Edit Pembelian';
$lang['details_purchase']='Detail Pembelian';

/*******************Sale*************************/

$lang['exceeding_sit']='Melebihi duduk yang tersedia !!';
$lang['seat_greater_than_zero'] ='Jumlah kursi harus lebih besar dari nol !!!';
$lang['are_you_sure_cancel_booking'] ='Anda yakin membatalkan bookingan ini?';
$lang['are_you_delete_notification'] ='Apakah Anda yakin akan menghapus semua notifikasi?';
$lang['no_notification_select'] ='Tidak ada notifikasi yang dipilih';
$lang['a_error'] ='Error';
$lang['are_you_delete_all_hold_sale'] ='Apa kamu yakin akan menghapus semua pesanan yang ditahan?';
$lang['no_hold'] ='Tidak ada yang ditahan';
$lang['sure_delete_this_hold'] ='Yakin mau dihapus pesanan yang ditahan ini';
$lang['please_select_hold_sale'] ='Silakan pilih pesanan yang ditahan untuk melanjutkan!';
$lang['delete_only_for_admin'] ='Hapus hanya diizinkan untuk Admin';
$lang['sure_delete_this_order'] ='Anda yakin akan menghapus pesanan ini?';
$lang['sure_cancel_this_order'] ='Anda yakin membatalkan pesanan ini?';
$lang['please_select_an_order'] ='Silakan pilih pesanan untuk melanjutkan!';
$lang['cart_not_empty'] ='Keranjang tidak kosong, ingin melanjutkan?';
$lang['cart_not_empty_want_to_clear'] ='Keranjang tidak kosong, ingin membersihkan keranjang?';
$lang['progress_or_done_kitchen'] ='Anda tidak dapat menghapus atau mengubah item apa pun yang Sedang Berlangsung atau Dilakukan di Dapur';
$lang['order_in_progress_or_done'] ='Pesanan Sedang Berlangsung atau Selesai, Anda tidak dapat membatalkannya!';
$lang['close_order_without'] ='Anda tidak dapat menutup pesanan tanpa menagih!';
$lang['want_to_close_order'] ='Apakah Anda ingin menutup pesanan ini?';
$lang['please_select_open_order'] ='Silakan pilih Open Order untuk melanjutkan!';
$lang['cart_empty'] ='Keranjang masih kosong';
$lang['select_a_customer'] ='Pilih Pelanggan';
$lang['select_a_waiter'] ='Pilih Waiter!';
$lang['delivery_not_possible_walk_in'] ='Pilih yang lain';
$lang['delivery_for_customer_must_address'] ='Untuk ojek online, masukan nama dan alamat penerima';
$lang['select_dine_take_delivery'] ='Pilih salah satu, Makan ditempat atau Take Away';
$lang['added_running_order'] ='Pesanan telah ditambahkan ke Running Orders dan pergi ke Panel Dapur juga. Pilih salah satu pesanan dari Menjalankan Pesanan untuk memodifikasinya atau membuat invoice';

$lang['sale'] ='Penjualan';
$lang['add_sale'] ='Tambah penjualan';
$lang['order_type'] ='Tipe Order';
$lang['order_number'] ='Nomber Order';
$lang['order_no'] ='No Order';
$lang['time']='Waktu';
$lang['customer']='Customer';
$lang['total_payable']='Total Sisa';
$lang['payment_method']='Metode Pembayaran';
$lang['view_invoice']='Lihat Invoice';
$lang['change_date']='Ganti tanggal';
$lang['save_changes']='Simpan perubahan';
$lang['close']='Tutup';
$lang['modified']='Diubah';
$lang['modifiers']='Pengedit';
$lang['date']='Tanggal';
//Item Modal
$lang['item_name']='Nama Item';
$lang['cool_haus_1']='Cool Haus';
$lang['cool_haus_1']='Cool Haus 2';
$lang['first_scoo_1']='First Scoo';
$lang['first_scoo_2']='First Scoo 2';
$lang['modifier_1']='Pengedit';
$lang['modifier_2']='Pengedit 2';
$lang['mg_1']='Mg';
$lang['mg_2']='Mg 2';
$lang['add_to_cart']='Tambah ke Keranjang';
//Customer Modal
$lang['add_customer']='Tambah Customer';
$lang['name']='Nama';
$lang['email']='Email';
$lang['dob']='DOB';
$lang['doa']='DOA';
$lang['delivery_address']='Alamat Pengiriman';

//Show Meja Modal2
$lang['new']='Baru';
$lang['Mejas']='Meja';
$lang['proceed_without_Meja']='Proses Tanpa Memilih Meja';
//Kitchen Status Modal
$lang['order_placed_at']='Order Placed at';
$lang['order_Meja']='Order Meja';
$lang['status']='Status';
//Please Read Modal
$lang['please_read']='Please Read';
$lang['please_read_text_1']='Modify Order';
$lang['please_read_text_2']="If you need to add some new item to an order, please select a running order from left and click on Modify Order. We have a perfect mechanism for modifying an order, please do that from there and please don't be confused to do that here, this is only Meja management section of an order.";
$lang['please_read_text_3']='What you can do here';
$lang['please_read_text_4']='An order may contain many person sitting in multiple Mejas, so you can select multiple Mejas for an order You can not set person more than available sit for in a Meja You can proceed without selecting Meja because some people may can gather, take tea and go out As a Meja can have availability of several chairs and sometime those are sharable, so you can select multiple order in a Meja';
//Finalize Order Modal
$lang['finalize_order']='Finalize Order';
$lang['total_payment']='Total Payment';
$lang['pay_amount']='Pay Amount';
$lang['given_amount']='Given Amount';
$lang['change_amount']='Change Amount';


$lang['open_hold_sale'] ='Buka order yang ditahan';
$lang['hold_sale'] ='Order ditahan';
$lang['walk_in_customer'] ='Walk-In-Customer';
$lang['hold_number'] ='No';
$lang['delete_all_hold_sale'] ='Hapus semua order tertahan';
$lang['order_type'] ='Tipe Order';
$lang['edit_in_cart'] ='Edit';

$lang['kitchen_waiter_bar'] ='Kitchen, Waiter & Bar';
$lang['kitchen_panel'] ='Kitchen Panel';
$lang['panels'] ='Panels';
$lang['waiter_name'] ='Waiter Name';
$lang['invoice']='Invoice';
$lang['help'] ='Help';
$lang['cook'] ='Cook';
$lang['done'] ='Done';
$lang['kitchen_help_text_first_para'] ='You should click on one/multiple item to mark it as Started Cooking or Done.';
$lang['kitchen_help_text_second_para'] =' You can not select any item for Take Away or Delivery type orders, as you need to deliver these type of orders as a pack Blue color indicates Started Cooking, where green color indicates that the item is Done cooking.';
$lang['kitchen_help_text_third_para'] ='Until an order is closed from POS Panel, that order will remain here';

$lang['notification'] ='Notification';
$lang['notification_list'] ='Notification List';
$lang['select_all'] ='Select All';
$lang['unselect_all'] ='Unselect All';
$lang['serve_take_delivery'] ='Serve/Take/Delivery';


$lang['waiter_panel'] ='Waiter Panel';
$lang['waiter_help_text_first_para'] ='You should click on one/multiple item to mark it as Started Preparing or Done.';
$lang['waiter_help_text_second_para'] ='You can not select any item for Take Away or Delivery type orders, as you need to deliver these type of orders as a pack Blue color indicates Started Preparing, where green color indicates that the item is Done cooking. ';
$lang['waiter_help_text_third_para'] ='Until an order is closed from POS Panel, that order will remain here';
$lang['remove'] ='Remove';
$lang['collect'] ='Collect';
$lang['bar_panel'] ='Bar Panel';
$lang['prepare'] ='Prepare';

$lang['calculator'] ='Kalkulator';

$lang['read_before_begin'] ='Instruksi';
$lang['read_help_text_1'] ='What is Running Order ';
$lang['read_help_text_2'] ='Placed order goes to Running Orders, to modify/invoice that order just select that order and click on bellow button ';
$lang['read_help_text_3'] ='What is Modify Order ';
$lang['read_help_text_4'] ='Modify order is not limited to only add new item, means modification of anything of that order, remove item, change item qty, change type, change waiter etc ';
$lang['read_help_text_5'] ='Allow Popup ';
$lang['read_help_text_6'] ='Please allow popup of your browser to print Invoice and KOT ';
$lang['read_help_text_7'] ='Print KOT ';
$lang['read_help_text_8'] ='Use Print KOT button if you intend to not to use Kitchen Panel ';
$lang['read_help_text_9'] ='When customer asks for new item or he wants an item more, just modify an order then go to print KOT, and just check that new item/quantity increased item, then reduce quantity and print the KOT, so that you can now only send the new item to kitchen ';
$lang['read_help_text_10'] ='But for Kitchen Panel, no need to worry, kithcen panel will be notified when an order is modified ';
$lang['read_help_text_11'] ='Searching ';
$lang['read_help_text_12'] ='Press Ctrl+Shift+F to focus on Search field ';
$lang['read_help_text_13'] ='Just type VEG, all veg items will be appeared ';
$lang['read_help_text_14'] ='Just type BEV, all beverage items will be appeared ';
$lang['read_help_text_15'] ='Just type Bar, all bar items will be appeared ';
$lang['read_help_text_16'] ='Refresh Button ';
$lang['read_help_text_17'] ='When you see that there refresh button right beside of running orders is red. You need to click on that button to refresh running orders to get update from kitchen. ';
$lang['read_help_text_18'] ='Inventory ';
$lang['read_help_text_19'] ='System will only deduct ingredient from inventory when you close an order by clicking on Create Invoice & Close OR Close Order button. ';
$lang['read_help_text_20'] ='Order Details ';
$lang['read_help_text_21'] ="You can also see an order's details by double clicking on it";
$lang['read_help_text_22'] ='Discount ';
$lang['read_help_text_23'] ='Mention that discount does not applies on Modifier. ';
$lang['read_help_text_24'] ='Clear Cache ';
$lang['read_help_text_25'] ='We are using JS cache to speed up operation, so please clear your cache by Ctrl+F5 after adding a new Food Item. ';


$lang['last_ten_sales'] ='10 Penjualan Terakhir';
$lang['sale_no'] ='No Penjualan';
$lang['print_invoice'] ='Print Invoice';
$lang['invoice_no'] ='No Invoice';

$lang['kitchen_notification'] ='Kitchen Notification';
$lang['dashboard'] ='Dashboard';
$lang['register'] ='Pembukaan';
$lang['logout'] ='Logout';
$lang['running_order'] ='List Orderan Aktif';
$lang['started_cooking'] ='Started Cooking';
$lang['time_count'] ='Waktu Berjalan';
$lang['customer_waiter_order_Meja'] ='Meja, No. Order, Waiter, Customer';
$lang['order_details'] ='Detail Order';
$lang['kot'] ='KOT';
$lang['print_kot'] ='Print KOT';
$lang['create_invoice_close'] ='Buat Invoice & Tutup Order';
$lang['create_invoice'] ='Buat Invoice';
$lang['close_order'] ='Tutup Order';
$lang['modify_order'] ='Ubah Pesanan / Tambah Barang / Ubah Meja';
$lang['cancel_order'] ='Batalkan Order';
$lang['kitchen_status'] ='Status Kitchen';
$lang['waiter'] ='Waiter';
$lang['Meja'] ='Meja';
$lang['item'] ='Item';
$lang['price'] ='Harga';
$lang['qty'] ='Jumlah';
$lang['discount'] ='Diskon';
$lang['total'] ='Total';
$lang['total_item'] ='Total Item';
$lang['sub_total'] ='Sub Total';
$lang['vat'] ='Pajak';
$lang['total_discount'] ='Total Diskon';
$lang['delivery_charge'] ='PPN';
$lang['cancel'] ='Batal';
$lang['hold'] ='Tahan';
$lang['direct_invoice'] ='Selesai (Print)';
$lang['place_order'] ='Tempatkan Order';
$lang['name_code_cat_veg_bev_bar'] ='Ketikan nama atau kode atau kategori makanan untuk mencari';
//alert
$lang['please_select_order_to_proceed'] ='Silakan pilih pesanan untuk melanjutkan';
$lang['register_close'] ='Transaksi telah ditutup';
$lang['you_only_pos_user'] ='You are only a POS User, you can not go to dashboard';

/*******************Inventory*************************/

$lang['code'] ='Kode';
$lang['category'] ='Kategori';
$lang['ingredient'] ='Bahan - bahan';
$lang['stock_value'] ='Stock Value';
$lang['stock_qty_amount'] ='Stock Qty/Jumlah';
$lang['alert_qty_amount'] ='Peringatan Qty/Jumlah';

/*******************Inventory Adjustment*************************/
$lang['inventory_Adjustments'] ='Inventory Adjustments';
$lang['add_inventory_Adjustment'] ='Tambah Inventory Adjustment';
$lang['ingredient_count'] ='Hitung bahan';
$lang['responsible_person'] ='Penanggung Jawab';
$lang['note'] ='Catatan';
$lang['read_me_first'] ='Tips';
$lang['quantity_amount'] ='Qty/Jumlah';
$lang['consumption_status'] ='Status Konsumsi';
$lang['select'] ='Pilih';
$lang['consumption_amount'] ='Jumlah Konsumsi';
$lang['notice'] ='Tips';
$lang['notice_text_1'] ='Anda wajib  memasukan qty/jumlah dalam daftar bahan yang terbuang, jika tidak stock diinventory tidak akan sama';
$lang['click_here'] ='Klik disini';
$lang['notice_text_2'] ='untuk mengetahui pengukuran bahan tertentu, seperti: 1 Cangkir Minyak = 240 ml';
$lang['notice_text_3'] ="Dan yang tidak bisa Anda ukur dengan cara ini. Seperti Anda membeli 1 paket Mie, lalu bagilah dengan berapa banyak yang dapat Anda hasilkan dengan 1 paket. Sesuatu seperti Anda membeli 1 paket Mie, itu beratnya 500 g dan Anda dapat membuat 4 piring. Kemudian masukkan konsumsi 125 g (500 g / 4 piring = 125 g)";
$lang['notice_text_4'] ='NB: Ini hanya contoh, hitung konsumsi sendiri.';
$lang['ingredient_already_remain'] ='Bahan sudah ada di troli, Anda dapat mengubah Jumlah / Jumlah';
$lang['edit_inventory_Adjustment'] ='Edit Inventory Adjustment';
$lang['details_inventory_Adjustment'] ='Inventory Adjustment Details';

$lang['responsible_person_field_required'] ='The Responsible Person field is required.';
$lang['note_field_cannot'] ='The Note field cannot exceed 200 characters in length.';

/*******************Waste*************************/
$lang['wastes'] ='Limbah';
$lang['add_waste'] ='Add Waste';
$lang['total_loss'] ='Total Kerugian';
$lang['food_menu_waste_quantity'] ='Food Menu Waste Quantity';
$lang['food_menu_waste'] ='Food Menu Waste';
$lang['wast_quantity'] ='Wast Quantity';
$lang['only_purchase_ingredient'] ='Bahan baku yang sudah dibeli';
$lang['loss_amount'] ='Total Kerugian';
$lang['loss_amt'] ='Loss Amt';
$lang['wast_amt'] ='Jumlah Terbuang';
$lang['edit_waste'] ='Edit Waste';
$lang['details_waste'] ='Waste Details';
/*******************Expense*************************/
$lang['expenses'] ='Expenses';
$lang['add_expense'] ='Add Expense';
$lang['edit_expense'] ='Edit Expense';
$lang['category'] ='Category';
$lang['amount'] ='Amount';
/*******************Supplier Due Payment*************************/
$lang['supplier_due_payments'] ='Sisa pembayaran ke supplier';
$lang['add_supplier_due_payment'] ='Add Supplier Due Payment';
$lang['supplier'] ='Supplier';
/*******************Customer Due Receive*************************/
$lang['customer_due_receives'] ='Sisa Pembayaran Customer';
$lang['add_customer_due_receive'] ='Add Customer Due Receive';
/*******************Send SMS*************************/
$lang['sms_service_chose_option'] ='SMS Service, please choose option from below';
$lang['signup_text_local'] ='Signup in TextLocal';
$lang['configure_sms'] ='Configure SMS';
$lang['send_test_sms'] ='Send Test SMS';
$lang['check_balance'] ='Check Balance';
$lang['sms_birthday_customer'] ='SMS to Customers Who Have Birthday Today';
$lang['sms_anniversary_customer'] ='SMS to Customers Who Have Anniversary Today';
$lang['send_custom_sms_all_customer'] ='Send Custom SMS to all Customers';
//Configure Sms
$lang['plese_be_informed'] ='Please be informed that you need to verify your TextLocal account before using that here to send sms from this software. First create account in TextLocal, then contact their support to verify it.';
$lang['sms_settings'] ='SMS Settings';
$lang['provide_your_text_local'] ='Provide your TextLocal https://www.textlocal.com/ SMS Credentials';
$lang['email_address'] ='Alamat Email';
$lang['password'] ='Password';
$lang['go_to_send_sms_page'] ='Go to Send SMS Page';
//send Test Sms
$lang['send'] ='Send';
$lang['sms'] ='SMS';
$lang['number'] ='Number';
$lang['must_include_country_code'] ='Must include country code, otherwise sms will fail';
$lang['message'] ='Message';
$lang['there_are'] ='There are';
$lang['customer_has']='customer has';
$lang['today']='today';
$lang['only']='Only';
$lang['customer_has_valid']='customer has valid phone number, you can send sms to these customers.';
$lang['your_current_credit']=' Your Current Credit Balance';
$lang['please_make_sure']='Please make sure that your current balance is sufficient to send sms.';
//Chek SMS Balance
$lang['sms_balance']='SMS Balance';
$lang['your_current_textlocal']='Your current textlocal sms credit balance is';
$lang['please_check_in']='please check in';
$lang['textlocal']='TextLocal';
$lang['to_know_how']='to know how many sms you can send by this credit.';
/*******************Attendances*************************/
$lang['attendances']='Attendances';
$lang['add_attendance']='Add Attendance';
$lang['employee']='Employee';
$lang['in_time']='In Time';
$lang['out_time']='Out Time';
$lang['update_time']='Update Time';
$lang['time_count']='Time Count';
$lang['add_update_attendance']='Add/Update Attendance';
/*******************Report*************************/
//Register Report
$lang['register_cash']='Cash:';
$lang['register_paypal']='Paypal:';
$lang['register_card']='Card:';
$lang['start_date']='Dari Tanggal';
$lang['end_date']='Sampai Tanggal';
$lang['user']='User';
$lang['opening_date_time']='Opening Date & Time';
$lang['closing_date_time']='Closing Date & Time';
$lang['paid_amount']='Paid Amount';
$lang['opening_balance']='Masukan saldo awal untuk membuka transaksi';
$lang['closing_balance']='Closing Balance';
$lang['customer_due_receive']='Sisa Pembayaran Customer';
$lang['sale_in_payment_method']='Sale In Payment Methods';
//Daily Summary Report
$lang['daily_summary_report']='Rekap Harian';
$lang['print']='Print';
$lang['sum']='Sum';
$lang['paid']='Paid';
$lang['sales']='Sales';
//Daily Food Sale Report

$lang['food_sales_report']='Food Sales Report';
$lang['quantity']='Jumlah';
//Daily Sale Report
$lang['daily_sale_report']='Rekap Penjualan Harian';
$lang['sale_report']='Rekap Penjualan';
$lang['total_sale']='Total Penjualan';
$lang['report_date']='Tanggal:';
//Detailed Sale Report
$lang['detailed_sale_report']='Detail Rekap Penjualan';
$lang['reference']='Ref';
$lang['all']='All';
$lang['subtotal']='Subtotal';
$lang['total_items']='Total Items';
//Consumption Report
$lang['consumption_report']='Consumption Report';
$lang['consumption_report_menus']='Consumption Report of Menus';
$lang['consumption_report_modifiers']='Consumption Report of Modifiers';
//Inventory Report
$lang['inventory_report']='Inventory Report';
//Profit Loss Report
$lang['profit_loss_report']='Profit Loss Report';
$lang['only_paid_amount']='Selesai';
$lang['supplier_due_payment']='Supplier Due Payment';
$lang['waste']='Waste';
$lang['gross_profit']='Gross Profit';
$lang['net_profit']='Net Profit';
//vat Report
$lang['vat_report']='Vat Report';
//Kitchen Performance Report
$lang['kitchen_performance_report']='Kitchen Performance Report';
$lang['type']='Type';
$lang['order_time']='Order Time';
$lang['cooking_start_time']='Cooking Start Time';
$lang['cooking_end_time']='Cooking End Time';
$lang['time_taken']='Time Taken';
$lang['hour']='Hour';
//Attendance Report
$lang['attendance_report']='Attendance Report';
$lang['hours']='Hours';
//Supplier Ledger Report
$lang['supplier_report']='Supplier Report';
$lang['suppliers']='Suppliers';
$lang['due_payment']='Due Payment';
$lang['table']='Meja';
$lang['payment_amount']='Payment Amount';
$lang['supplier_ledger_report']='Supplier Ledger Report';
//Supplier Due Report
$lang['supplier_due_report']='Supplier Due Report';
$lang['payable_due']='Payable Due';
//Customer Due Report
$lang['customer_due_report']='Customer Due Report';
//Customer Ledger Report
$lang['customer_report']='Customer Report';
$lang['customer_ledger_report']='Customer Ledger Report';
$lang['receive_amount']='Receive Amount';
$lang['due_receive']='Due Receive';

//Purchase Report By Date

$lang['purchase_report']='Purchase Report';
$lang['grand_total']='Grand Total';
$lang['purchased_by']='Purchased By';
//Expense Report
$lang['expense_report']='Expense Report';
$lang['expense_item']='Expense Item';
//Waste Report
$lang['waste_report']='Rekap Data Limbah';
/*******************Master*************************/
//Ingredient Categories
$lang['ingredient_categories']='Ingredient Categories';
$lang['add_ingredient_category']='Add Ingredient Category';
$lang['edit_ingredient_category']='Edit Ingredient Category';
$lang['category_name']='Nama';
$lang['description']='Deskripsi';
//Ingredient Units
$lang['add_unit']='Add Unit';
$lang['units']='Units';
$lang['unit']='Unit';
$lang['unit_name']='Unit Name';
//Ingredients
$lang['add_ingredient']='Add Ingredient';
$lang['upload_ingredient']='Upload Ingredient';
$lang['upload_ingredients']='Upload Ingredients';
$lang['upload']='Upload';
$lang['upload_file']='Upload File';
$lang['download_sample']='Download Sample';
$lang['remove_all_previous_data']='Remove all prevoius data, before uploading new data.';
$lang['alert_quantity_amount']='Peringatan Qty/Jlm';
$lang['purchase_price']='Harga Beli';
$lang['alert_qty']='Peringatan Qty';
$lang['alert_quantity']='Peringatan Qty';
//Vats
$lang['vats']='VATs';
$lang['add_vat']='Add VAT';
$lang['vat_name']='VAT Name';
$lang['percentage']='Percentage';
//Modifier
$lang['modifier']='Modifier';
$lang['price']='Harga';
$lang['ingredient_consumptions']='Ingredient Consumptions';
$lang['consumption']='Consumption';
$lang['name_field_required']='The Name field is required.';
$lang['price_field_required']='The Price field is required.';
$lang['at_least_ingredient']='At least 1 Ingredient is required.';
$lang['description_field_can_not_exceed']='The Description field cannot exceed 200 characters in length.';
$lang['modifier_details']='Modifier Details';
//Food Menu Categories
$lang['food_menu_categories']='Food Menu Categories';
$lang['add_food_menu_category']='Add Food Menu Category';
$lang['edit_food_menu_category']='Edit Food Menu Category';
//Food Menus
$lang['food_menus']='Food Menus';
$lang['add_food_menu']='Add Food Menu';
$lang['upload_food_menu']='Upload Food Menu';
$lang['upload_food_menu_ingredients']='Upload Recipe';
$lang['sale_price']='Harga Jual';
$lang['total_ingredients']='Total Ingredients';
$lang['assign_modifier']='Assign Modifier';
$lang['photo']='Photo';
$lang['is_it_veg']='Is it Veg Item';
$lang['is_it_beverage']='Is it Beverage';
$lang['is_it_bar']='Is it Bar Item';

$lang['category_field_required']='The Category field is required.';
$lang['sale_price_field_required']='The Sale Price field is required.';
$lang['veg_item_field_required']='The Is it Veg Item? field is required.';
$lang['beverage_item_field_required']='The Is it Beverage? field is required.';
$lang['bar_item_field_required']='The Is it Bar Item? field is required.';

$lang['edit_food_menu']='Edit Food Menu';
$lang['photo_height_width']='Height must be 80px and Width must be 142px';
$lang['food_menu_details']='Food Menu Details';
$lang['assign_food_menu_modifier']='Assign Food Menu Modifier';

//Suppliers
$lang['add_supplier']='Add Supplier';
$lang['contact_person']='Contact Person';
$lang['edit_supplier']='Edit Supplier';
//Customers
$lang['add_customer']='Add Customer';
$lang['upload_customer']='Upload Customer';
$lang['should_country_code']='Should contain country code';
$lang['date_of_birth']='Date Of Birth';
$lang['date_of_anniversary']='Date Of Anniversary';
$lang['gst_number']='GST Number';
$lang['edit_customer']='Edit Customer';
//Expense Items
$lang['expense_items']='Expense Items';
$lang['add_expense_item']='Add Expense Item';
$lang['expense_item_name']='Expense Item Name';
$lang['edit_expense_item']='Edit Expense Item';
//Employees
$lang['add_employee']='Add Employee';
$lang['designation']='Designation';
$lang['enter_waiter']='';
$lang['edit_employee']='Edit Employee';
//Payment Methods
$lang['payment_methods']='Payment Methods';
$lang['add_payment_method']='Add Payment Method';
$lang['payment_method_name']='Payment Method Name';
$lang['edit_payment_method']='Edit Payment Method';
//Mejas
$lang['outlet']='Outlet';
$lang['add_Meja']='Add Meja';
$lang['Meja_name']='Meja Name';
$lang['seat_capacity']='Seat Capacity';
$lang['position']='Position';
$lang['edit_Meja']='Edit Meja';

/*******************Account*************************/
//General Setting
$lang['general_settings']='General Settings';
$lang['date_format']='Date Format';
$lang['country_time_zone']='Country Time Zone';
$lang['currency']='Currency';
//Manage Users
$lang['short_message_service']='Short Message Service';
$lang['users']='Users';
$lang['bar'] = 'Bar';
$lang['kitchen'] = 'Kitchen';
$lang['add_user']='Add User';
$lang['activate']='Activate';
$lang['deactivate']='Deactivate';
$lang['email_address']='Alamat Email';
$lang['password']='Password';
$lang['confirm_password']='Confirm Password';
$lang['designation'] = 'Designation';
$lang['will_login'] = 'Will Login?';
$lang['pos_user']='POS User';
$lang['kitchen_user']='Kitchen User';
$lang['bar_user']='Bar User';
$lang['waiter_user']='Waiter User';
$lang['menu_access']='Menu Access';
$lang['edit_user']='Edit User';
$lang['user_activate']='User has been activated successfully! This user will be able to login again.';
$lang['user_deactivate']='User has been deactivated successfully! This user will no longer be able to login.';
$lang['vat_activate']='Pajak berhasil diaktifkan';
$lang['vat_deactivate']='Pajak dinonaktifkan';
// Profile Change
$lang['change_profile']='Ganti Profil';
// Profile Change
$lang['change_password']='Ganti Password';
$lang['old_password']='Password Lama';
$lang['new_password']='Password Baru';
// Login
$lang['login']='Login';
$lang['please_login']='Mohon Login';

/*******************Menu*************************/
$lang['pos']='Buka Kasir';
$lang['todays_summary']="Ringkasan Hari ini";
$lang['shortcut_keys']='Shortcut Keys';
$lang['register_details']='Rincian Pembukaan Transaksi';
$lang['close_register']='Tutup Transaksi';
$lang['select_language']='Language';
$lang['main_navigation']='MAIN NAVIGATION';
$lang['home']='Home';
$lang['outlet_list']='Outlet List';
$lang['purchase']='Pembelian';
$lang['inventory_adjustments']='Inventory Adjustments';
$lang['waste']='Manajemen Limbah';
$lang['expense']='Operasional';
$lang['supplier_due_payment']='Supplier Due Payment';
$lang['customer_due_receive']='Sisa Pembayaran Customer';
$lang['send_sms']='Send SMS';
$lang['attendance']='Attendance';
$lang['report']='Rekap';
$lang['food_sale_report']='Rekap Menu Terjual';
$lang['low_inventory_report']='Rekap Inventory Sedikit';
$lang['supplier_ledger']='Buku Besar Supplier';
$lang['customer_ledger']='Buku Besar Customer';
$lang['master']='Master';
$lang['ingredient_units']='Unit Bahan';
$lang['account']='Pengaturan'; 
$lang['sms_setting']='SMS Setting';
$lang['manage_users']='Manajemen User';
$lang['in']='Masuk'; 
$lang['cash']='Cash'; 
$lang['card']='Card'; 
$lang['card']='Paypal';
$lang['not_closed_yet']='Belum ditutup';
$lang['tables']='Meja';
$lang['add_table']='Tambah Meja';
$lang['to']='S/D';

$lang['table_name']='Nama Meja';
$lang['seat_capacity']='Kapasitas';
$lang['position']='Posisi';
$lang['description']='Deskripsi';

/* End of file afar_lang.php */
/* Location: ./application/language/afar/afar_lang.php */

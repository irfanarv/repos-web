-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 02 Apr 2021 pada 17.34
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `repos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_admin_currencies`
--

CREATE TABLE `tbl_admin_currencies` (
  `id` int(11) NOT NULL,
  `country` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `code` varchar(25) DEFAULT NULL,
  `symbol` varchar(25) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_admin_currencies`
--

INSERT INTO `tbl_admin_currencies` (`id`, `country`, `currency`, `code`, `symbol`, `del_status`) VALUES
(1, 'Albania', 'Leke', 'ALL', 'Lek', 'Live'),
(2, 'America', 'Dollars', 'USD', '$', 'Live'),
(3, 'Afghanistan', 'Afghanis', 'AF', '؋', 'Live'),
(4, 'Argentina', 'Pesos', 'ARS', '$', 'Live'),
(5, 'Aruba', 'Guilders', 'AWG', 'ƒ', 'Live'),
(6, 'Australia', 'Dollars', 'AUD', '$', 'Live'),
(7, 'Azerbaijan', 'New Manats', 'AZ', 'ман', 'Live'),
(8, 'Bahamas', 'Dollars', 'BSD', '$', 'Live'),
(9, 'Barbados', 'Dollars', 'BBD', '$', 'Live'),
(10, 'Belarus', 'Rubles', 'BYR', 'p.', 'Live'),
(11, 'Belgium', 'Euro', 'EUR', '€', 'Live'),
(12, 'Beliz', 'Dollars', 'BZD', 'BZ$', 'Live'),
(13, 'Bermuda', 'Dollars', 'BMD', '$', 'Live'),
(14, 'Bolivia', 'Bolivianos', 'BOB', '$b', 'Live'),
(15, 'Bosnia and Herzegovina', 'Convertible Marka', 'BAM', 'KM', 'Live'),
(16, 'Botswana', 'Pula\'s', 'BWP', 'P', 'Live'),
(17, 'Bulgaria', 'Leva', 'BG', 'лв', 'Live'),
(18, 'Brazil', 'Reais', 'BRL', 'R$', 'Live'),
(19, 'Britain (United Kingdom)', 'Pounds', 'GBP', '£', 'Live'),
(20, 'Brunei Darussalam', 'Dollars', 'BND', '$', 'Live'),
(21, 'Cambodia', 'Riels', 'KHR', '៛', 'Live'),
(22, 'Canada', 'Dollars', 'CAD', '$', 'Live'),
(23, 'Cayman Islands', 'Dollars', 'KYD', '$', 'Live'),
(24, 'Chile', 'Pesos', 'CLP', '$', 'Live'),
(25, 'China', 'Yuan Renminbi', 'CNY', '¥', 'Live'),
(26, 'Colombia', 'Pesos', 'COP', '$', 'Live'),
(27, 'Costa Rica', 'Colón', 'CRC', '₡', 'Live'),
(28, 'Croatia', 'Kuna', 'HRK', 'kn', 'Live'),
(29, 'Cuba', 'Pesos', 'CUP', '₱', 'Live'),
(30, 'Cyprus', 'Euro', 'EUR', '€', 'Live'),
(31, 'Czech Republic', 'Koruny', 'CZK', 'Kč', 'Live'),
(32, 'Denmark', 'Kroner', 'DKK', 'kr', 'Live'),
(33, 'Dominican Republic', 'Pesos', 'DOP ', 'RD$', 'Live'),
(34, 'East Caribbean', 'Dollars', 'XCD', '$', 'Live'),
(35, 'Egypt', 'Pounds', 'EGP', '£', 'Live'),
(36, 'El Salvador', 'Colones', 'SVC', '$', 'Live'),
(37, 'England (United Kingdom)', 'Pounds', 'GBP', '£', 'Live'),
(38, 'Euro', 'Euro', 'EUR', '€', 'Live'),
(39, 'Falkland Islands', 'Pounds', 'FKP', '£', 'Live'),
(40, 'Fiji', 'Dollars', 'FJD', '$', 'Live'),
(41, 'France', 'Euro', 'EUR', '€', 'Live'),
(42, 'Ghana', 'Cedis', 'GHC', '¢', 'Live'),
(43, 'Gibraltar', 'Pounds', 'GIP', '£', 'Live'),
(44, 'Greece', 'Euro', 'EUR', '€', 'Live'),
(45, 'Guatemala', 'Quetzales', 'GTQ', 'Q', 'Live'),
(46, 'Guernsey', 'Pounds', 'GGP', '£', 'Live'),
(47, 'Guyana', 'Dollars', 'GYD', '$', 'Live'),
(48, 'Holland (Netherlands)', 'Euro', 'EUR', '€', 'Live'),
(49, 'Honduras', 'Lempiras', 'HNL', 'L', 'Live'),
(50, 'Hong Kong', 'Dollars', 'HKD', '$', 'Live'),
(51, 'Hungary', 'Forint', 'HUF', 'Ft', 'Live'),
(52, 'Iceland', 'Kronur', 'ISK', 'kr', 'Live'),
(53, 'India', 'Rupees', 'INR', 'Rp', 'Live'),
(54, 'Indonesia', 'Rupiahs', 'IDR', 'Rp', 'Live'),
(55, 'Iran', 'Rials', 'IRR', '﷼', 'Live'),
(56, 'Ireland', 'Euro', 'EUR', '€', 'Live'),
(57, 'Isle of Man', 'Pounds', 'IMP', '£', 'Live'),
(58, 'Israel', 'New Shekels', 'ILS', '₪', 'Live'),
(59, 'Italy', 'Euro', 'EUR', '€', 'Live'),
(60, 'Jamaica', 'Dollars', 'JMD', 'J$', 'Live'),
(61, 'Japan', 'Yen', 'JPY', '¥', 'Live'),
(62, 'Jersey', 'Pounds', 'JEP', '£', 'Live'),
(63, 'Kazakhstan', 'Tenge', 'KZT', 'лв', 'Live'),
(64, 'Korea (North)', 'Won', 'KPW', '₩', 'Live'),
(65, 'Korea (South)', 'Won', 'KRW', '₩', 'Live'),
(66, 'Kyrgyzstan', 'Soms', 'KGS', 'лв', 'Live'),
(67, 'Laos', 'Kips', 'LAK', '₭', 'Live'),
(68, 'Latvia', 'Lati', 'LVL', 'Ls', 'Live'),
(69, 'Lebanon', 'Pounds', 'LBP', '£', 'Live'),
(70, 'Liberia', 'Dollars', 'LRD', '$', 'Live'),
(71, 'Liechtenstein', 'Switzerland Francs', 'CHF', 'CHF', 'Live'),
(72, 'Lithuania', 'Litai', 'LTL', 'Lt', 'Live'),
(73, 'Luxembourg', 'Euro', 'EUR', '€', 'Live'),
(74, 'Macedonia', 'Denars', 'MKD', 'ден', 'Live'),
(75, 'Malaysia', 'Ringgits', 'MYR', 'RM', 'Live'),
(76, 'Malta', 'Euro', 'EUR', '€', 'Live'),
(77, 'Mauritius', 'Rupees', 'MUR', '₨', 'Live'),
(78, 'Mexico', 'Pesos', 'MX', '$', 'Live'),
(79, 'Mongolia', 'Tugriks', 'MNT', '₮', 'Live'),
(80, 'Mozambique', 'Meticais', 'MZ', 'MT', 'Live'),
(81, 'Namibia', 'Dollars', 'NAD', '$', 'Live'),
(82, 'Nepal', 'Rupees', 'NPR', '₨', 'Live'),
(83, 'Netherlands Antilles', 'Guilders', 'ANG', 'ƒ', 'Live'),
(84, 'Netherlands', 'Euro', 'EUR', '€', 'Live'),
(85, 'New Zealand', 'Dollars', 'NZD', '$', 'Live'),
(86, 'Nicaragua', 'Cordobas', 'NIO', 'C$', 'Live'),
(87, 'Nigeria', 'Nairas', 'NG', '₦', 'Live'),
(88, 'North Korea', 'Won', 'KPW', '₩', 'Live'),
(89, 'Norway', 'Krone', 'NOK', 'kr', 'Live'),
(90, 'Oman', 'Rials', 'OMR', '﷼', 'Live'),
(91, 'Pakistan', 'Rupees', 'PKR', '₨', 'Live'),
(92, 'Panama', 'Balboa', 'PAB', 'B/.', 'Live'),
(93, 'Paraguay', 'Guarani', 'PYG', 'Gs', 'Live'),
(94, 'Peru', 'Nuevos Soles', 'PE', 'S/.', 'Live'),
(95, 'Philippines', 'Pesos', 'PHP', 'Php', 'Live'),
(96, 'Poland', 'Zlotych', 'PL', 'zł', 'Live'),
(97, 'Qatar', 'Rials', 'QAR', '﷼', 'Live'),
(98, 'Romania', 'New Lei', 'RO', 'lei', 'Live'),
(99, 'Russia', 'Rubles', 'RUB', 'руб', 'Live'),
(100, 'Saint Helena', 'Pounds', 'SHP', '£', 'Live'),
(101, 'Saudi Arabia', 'Riyals', 'SAR', '﷼', 'Live'),
(102, 'Serbia', 'Dinars', 'RSD', 'Дин.', 'Live'),
(103, 'Seychelles', 'Rupees', 'SCR', '₨', 'Live'),
(104, 'Singapore', 'Dollars', 'SGD', '$', 'Live'),
(105, 'Slovenia', 'Euro', 'EUR', '€', 'Live'),
(106, 'Solomon Islands', 'Dollars', 'SBD', '$', 'Live'),
(107, 'Somalia', 'Shillings', 'SOS', 'S', 'Live'),
(108, 'South Africa', 'Rand', 'ZAR', 'R', 'Live'),
(109, 'South Korea', 'Won', 'KRW', '₩', 'Live'),
(110, 'Spain', 'Euro', 'EUR', '€', 'Live'),
(111, 'Sri Lanka', 'Rupees', 'LKR', '₨', 'Live'),
(112, 'Sweden', 'Kronor', 'SEK', 'kr', 'Live'),
(113, 'Switzerland', 'Francs', 'CHF', 'CHF', 'Live'),
(114, 'Suriname', 'Dollars', 'SRD', '$', 'Live'),
(115, 'Syria', 'Pounds', 'SYP', '£', 'Live'),
(116, 'Taiwan', 'New Dollars', 'TWD', 'NT$', 'Live'),
(117, 'Thailand', 'Baht', 'THB', '฿', 'Live'),
(118, 'Trinidad and Tobago', 'Dollars', 'TTD', 'TT$', 'Live'),
(119, 'Turkey', 'Lira', 'TRY', 'TL', 'Live'),
(120, 'Turkey', 'Liras', 'TRL', '£', 'Live'),
(121, 'Tuvalu', 'Dollars', 'TVD', '$', 'Live'),
(122, 'Ukraine', 'Hryvnia', 'UAH', '₴', 'Live'),
(123, 'United Kingdom', 'Pounds', 'GBP', '£', 'Live'),
(124, 'United States of America', 'Dollars', 'USD', '$', 'Live'),
(125, 'Uruguay', 'Pesos', 'UYU', '$U', 'Live'),
(126, 'Uzbekistan', 'Sums', 'UZS', 'лв', 'Live'),
(127, 'Vatican City', 'Euro', 'EUR', '€', 'Live'),
(128, 'Venezuela', 'Bolivares Fuertes', 'VEF', 'Bs', 'Live'),
(129, 'Vietnam', 'Dong', 'VND', '₫', 'Live'),
(130, 'Yemen', 'Rials', 'YER', '﷼', 'Live'),
(131, 'Zimbabwe', 'Zimbabwe Dollars', 'ZWD', 'Z$', 'Live'),
(132, 'Bangladesh', 'Bangladeshi Taka', 'BDT', '৳', 'Live'),
(133, 'Kuwait ', 'KWD', 'KWD', 'KWD', 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_admin_user_menus`
--

CREATE TABLE `tbl_admin_user_menus` (
  `id` int(10) NOT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `controller_name` varchar(50) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_admin_user_menus`
--

INSERT INTO `tbl_admin_user_menus` (`id`, `menu_name`, `controller_name`, `del_status`) VALUES
(1, 'Sale', 'Sale', 'Live'),
(2, 'Purchase', 'Purchase', 'Live'),
(3, 'Inventory', 'Inventory', 'Live'),
(4, 'Waste', 'Waste', 'Live'),
(6, 'Expense', 'Expense', 'Live'),
(7, 'Report', 'Report', 'Live'),
(8, 'Dashboard', 'Dashboard', 'Live'),
(9, 'Master', 'Master', 'Live'),
(10, 'User', 'User', 'Live'),
(11, 'Supplier Payment', 'SupplierPayment', 'Live'),
(13, 'Inventory Adjustment', 'Inventory_adjustment', 'Live'),
(14, 'Short Message Service', 'Short_message_service', 'Live'),
(15, 'Customer Due Receive', 'Customer_due_receive', 'Live'),
(16, 'Attendance', 'Attendance', 'Live'),
(17, 'Bar', 'Bar', 'Live'),
(18, 'Kitchen', 'Kitchen', 'Live'),
(19, 'Waiter', 'Waiter', 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `id` int(10) NOT NULL,
  `reference_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `in_time` time DEFAULT NULL,
  `out_time` time DEFAULT NULL,
  `note` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Live'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_companies`
--

CREATE TABLE `tbl_companies` (
  `id` int(10) NOT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `timezone` varchar(50) DEFAULT NULL,
  `date_format` varchar(50) DEFAULT NULL,
  `outlet_id` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_companies`
--

INSERT INTO `tbl_companies` (`id`, `currency`, `timezone`, `date_format`, `outlet_id`) VALUES
(1, '৳', 'Asia/Dhaka', 'd/m/Y', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_customers`
--

CREATE TABLE `tbl_customers` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `gst_number` varchar(50) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `date_of_birth` date DEFAULT NULL,
  `date_of_anniversary` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_customers`
--

INSERT INTO `tbl_customers` (`id`, `name`, `phone`, `email`, `address`, `gst_number`, `area_id`, `user_id`, `company_id`, `del_status`, `date_of_birth`, `date_of_anniversary`) VALUES
(1, 'Walk-in Customer', '', NULL, NULL, NULL, 0, 1, 1, 'Live', NULL, NULL),
(2, 'Compliment', '08111111', '', '', '', NULL, 1, 1, 'Live', '1970-01-01', '1970-01-01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_customer_due_receives`
--

CREATE TABLE `tbl_customer_due_receives` (
  `id` int(10) NOT NULL,
  `reference_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `only_date` date DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `note` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Live'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_employees`
--

CREATE TABLE `tbl_employees` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_expenses`
--

CREATE TABLE `tbl_expenses` (
  `id` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `menu_resto_id` int(10) NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `employee_id` int(10) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_expenses`
--

INSERT INTO `tbl_expenses` (`id`, `date`, `amount`, `menu_resto_id`, `category_id`, `employee_id`, `note`, `user_id`, `outlet_id`, `del_status`) VALUES
(1, '2020-06-21', 100000.00, 5, 2, 1, 'Ongkir Kopi', 1, 1, 'Live'),
(2, '2020-06-21', 100000.00, 3, 1, 1, 'Pembelian Pulpen, Nota dll', 1, 1, 'Live'),
(3, '2020-06-21', 100000.00, 3, 1, 1, 'tes', 1, 1, 'Live'),
(4, '2020-06-21', 64000.00, 5, 2, 1, 'hfhf', 1, 1, 'Live'),
(5, '2020-06-22', 10000.00, 5, 1, 1, 'fa', 1, 1, 'Live'),
(6, '2020-06-23', 100000.00, 5, 2, 1, 'hjghj', 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_expense_items`
--

CREATE TABLE `tbl_expense_items` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_expense_items`
--

INSERT INTO `tbl_expense_items` (`id`, `name`, `description`, `user_id`, `company_id`, `del_status`) VALUES
(1, 'ATK', '', 1, 1, 'Live'),
(2, 'Ongkos Kirim', '', 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_food_menus`
--

CREATE TABLE `tbl_food_menus` (
  `id` int(10) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `sale_price` float DEFAULT NULL,
  `tax_information` text DEFAULT NULL,
  `vat_id` int(11) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `veg_item` varchar(50) DEFAULT 'Veg No',
  `beverage_item` varchar(50) DEFAULT 'Beverage No',
  `bar_item` varchar(50) DEFAULT 'Bar No',
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_food_menus`
--

INSERT INTO `tbl_food_menus` (`id`, `code`, `name`, `category_id`, `description`, `sale_price`, `tax_information`, `vat_id`, `user_id`, `company_id`, `photo`, `veg_item`, `beverage_item`, `bar_item`, `del_status`) VALUES
(1, '1', 'CAPPUCINO ORIGINAL', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(2, '2', 'CAPPUCINO CARAMEL', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(3, '3', 'CAPPUCINO VANILLA', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(4, '4', 'CAPPUCINO HAZELNUT', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(5, '5', 'CAPPUCINO TIRAMISU', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(6, '6', 'CAPPUCINO BUTTERSCOTH', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(7, '7', 'CAPPUCINO IRISH', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(8, '8', 'LATTE ORIGINAL', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(9, '9', 'LATTE CARAMEL', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(10, '10', 'LATTE VANILLA', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(11, '11', 'LATTE HAZELNUT', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(12, '12', 'LATTE BUTERSCOTH', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(13, '13', 'LATTE TIRAMISU', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(14, '14', 'LATTE IRISH', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(15, '15', 'MACHITAO ORIGINAL', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(16, '16', 'MACHIATO CARAMEL', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(17, '17', 'MACHIATO VANILLA', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(18, '18', 'MACHIATO TIRAMISU', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(19, '19', 'AVOCADO COFFE', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(20, '20', 'EXPRESSO', 5, '', 10000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(21, '21', 'BAFAGEH RAINBOW', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(22, '22', 'BALCK COFFE', 5, '', 10000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(23, '23', 'ORIGINAL TEA', 5, '', 10000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(24, '24', 'MIN TEA', 5, '', 12000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(25, '25', 'BANDREK', 5, '', 12000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(26, '26', 'BANDREK SUSU', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(27, '27', 'MILLO HOT', 5, '', 12000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(28, '28', 'OVALTINE', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(29, '29', 'CHOCOLATE', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(30, '30', 'GINGER MILK', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(31, '31', 'LEMON TEA', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(32, '32', 'MILK TEA', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(33, '33', 'TEA POT', 5, '', 30000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(34, '34', 'GREEN TEA', 5, '', 35000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(35, '35', 'MINT TEA', 5, '', 40000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(36, '36', 'GREAN TEA MINT', 5, '', 45000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(37, '37', 'MINERAL WATER', 5, '', 8000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(38, '38', 'FANTA', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(39, '39', 'COCA COLA', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(40, '40', 'SPRITE', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(41, '41', 'SEVEN UP', 5, '', 15000, '[]', NULL, 1, 1, '', NULL, NULL, NULL, 'Live'),
(42, '42', 'PEPSI', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(43, '43', 'REDBULL', 5, '', 30000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(44, '44', 'ICE MILLO', 5, '', 15000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(45, '45', 'OVALTINE', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(46, '46', 'LEMON TEA', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(47, '47', 'CHOCOLATE', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(48, '48', 'MILK SHAKE VANILLA', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(49, '49', 'MILK SHAKE STRAWBERRY', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(50, '50', 'MILK SHAKE CHOCOLATE', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(51, '51', 'LEMON TEA MINT', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(52, '52', 'GREAN TEA MACCHA', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(53, '53', 'MILK TEA', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(54, '54', 'SODA GEMBIRA', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(55, '55', 'ORANGE JUICE', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(56, '56', 'AVOCADO JUICE', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(57, '57', 'MANGGO JUICE', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(58, '58', 'MELON JUICE', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(59, '59', 'WATER MELON JUICE', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(60, '60', 'LEMON MINT JUICE', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(61, '61', 'PITCH UP', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(62, '62', 'LYCHEE FIZZ', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(63, '63', 'MELON FIZZ', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(64, '64', 'BELLINI', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(65, '65', 'DRAGON FRUIT', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(66, '66', 'RED DEVIL', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(67, '67', 'CAPPUCINO ORIGINAL', 5, '', 17000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(68, '68', 'CAPPUCINO CARAMEL', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(69, '69', 'CAPPUCINO VANILLA', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(70, '70', 'CAPPUCINO HAZELNUT', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(71, '71', 'CAPPUCINO TIRAMISU', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(72, '72', 'CAPPUCINO BUTTERSCOTH', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(73, '73', 'CAPPUCINI IRIS', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(74, '74', 'LATTE ORIGINAL', 5, '', 17000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(75, '75', 'LATTE CARAMEL', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(76, '76', 'LATTE VANILLA', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(77, '77', 'LATTE HAZELNUT', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(78, '78', 'LATTE TIRAMISU', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(79, '79', 'LATTE BUTERSCOTH', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(80, '80', 'LATTE IRIS', 5, '', 22000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(81, '81', 'ICE BLACK COFFE', 5, '', 12000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(82, '82', 'ICE COFFE MILK', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(83, '83', 'ICE BROWN COFFE', 5, '', 30000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(84, '84', 'KENTANG GORENG', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(85, '85', 'PISANG BAKAR', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(86, '86', 'ROTI BAKAR', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(87, '87', 'BANANA ROLL', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(88, '88', 'BANTAL COKLAT', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(89, '89', 'NUGGET', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(90, '90', 'SOSIS BAKAR', 5, '', 15000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(91, '91', 'BANANA SPLIT', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(92, '92', 'CALAMARI RING', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(93, '93', 'SAKSUKA', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(94, '94', 'OMELETE', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(95, '95', 'SAMBOSA', 5, '', 30000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(96, '96', 'PAN CAKE', 5, '', 15000, '[]', NULL, 1, 1, 'ee4683ea3ee7f39972f5bda7b9e206a6.jpg', NULL, NULL, NULL, 'Live'),
(97, '97', 'PAN CAKE ICE CREAM', 5, '', 18000, '[]', NULL, 1, 1, '049fef6759f9d6408154b55a404d783d.jpeg', NULL, NULL, NULL, 'Live'),
(98, '98', 'ICE CREAM GORENG', 5, '', 25000, '[]', NULL, 1, 1, 'b0f971cebd9071f6b108025a66ec52cd.jpg', NULL, NULL, NULL, 'Live'),
(99, '99', 'SANDWICH ICE CREAM', 5, '', 25000, '[]', NULL, 1, 1, '1d9bc691bd616ee6c16c8997a819a517.jpeg', NULL, NULL, NULL, 'Live'),
(100, '100', 'Nasi Mandi', 4, '', 50000, '[]', NULL, 1, 1, '', NULL, NULL, NULL, 'Live'),
(101, '101', 'CHIKEN WINGS', 5, '', 25000, '[]', NULL, 1, 1, '4d12fe4da520105bb4781b557b3296c5.jpeg', NULL, NULL, NULL, 'Live'),
(102, '102', 'KENTANG MIX NUGGET', 5, '', 25000, '[]', NULL, 1, 1, '4c9b025d66ebc946275c7993c509354a.jpg', NULL, NULL, NULL, 'Live'),
(103, '103', 'DADAR GULUNG COKLAT', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(104, '104', 'BAFAGEH SNACKS', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(105, '105', 'GOSUKE', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(106, '106', 'PISANG CRISPY', 5, '', 20000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(107, '107', 'CHIKEN CRISPY', 5, '', 30000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(108, '108', 'NASI GORENG TELOR', 5, '', 20000, '[]', NULL, 1, 1, '431cfeb2701d186b7dd0761323746229.jpeg', NULL, NULL, NULL, 'Live'),
(109, '109', 'NASI GORENG KORNET', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(110, '110', 'NASI GORENG SPECIAL', 5, '', 30000, '[]', NULL, 1, 1, '86785904ec8692a3b7006450f1f24fbc.jpeg', NULL, NULL, NULL, 'Live'),
(111, '111', 'AYAM PURULUK', 5, '', 25000, '[]', NULL, 1, 1, '5bdb2ccdc8bf5e40a96de59457f04328.jpg', NULL, NULL, NULL, 'Live'),
(112, '112', 'AYAM BAKAR', 5, '', 25000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(113, '113', 'SOP IGA ORIGINAL', 5, '', 35000, '[]', NULL, 1, 1, '797a15ee3065a4442e0f16da2e5e21db.jpeg', NULL, NULL, NULL, 'Live'),
(114, '114', 'INDOMIE TELOR', 5, '', 10000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(115, '115', 'INDOMIE KORNET', 5, '', 20000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(116, '116', 'INDOMIE KEJU', 5, '', 30000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(117, '117', 'INDOMIE SPECIAL', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(118, '118', 'HOTPLATE MIE', 5, '', 25000, '[]', NULL, 1, 1, '1e7a8ff2032c9aa93750700fa9ec3153.jpeg', NULL, NULL, NULL, 'Live'),
(119, '119', 'AYAM GEPREK', 5, '', 25000, '[]', NULL, 1, 1, 'cfd4dbf29aa49a9159ac245f50ef673f.jpeg', NULL, NULL, NULL, 'Live'),
(120, '120', 'SPAGHETTI', 5, '', 30000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(121, '121', 'BURGER FRENCHISE', 5, '', 30000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(122, '122', 'SOP IGA BAKAR', 5, '', 40000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(123, '123', 'SOP IGA GORENG', 5, '', 40000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(124, '124', 'KHOBUS', 5, '', 5000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(125, '125', 'KEBAB DJAZ', 5, '', 50000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(126, '126', 'FATAIR AYAM', 5, '', 65000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(127, '127', 'FATAIR SUSU', 5, '', 65000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(128, '128', 'FATAIR ZATAR', 5, '', 65000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(129, '129', 'FATAIR TUNA', 5, '', 75000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(130, '130', 'FATAIR MIX', 5, '', 75000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(131, '131', 'FATAIR LAHM', 5, '', 75000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(132, '132', 'SAKSUKA', 5, '', 25000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(133, '133', 'FATAIR KEJU', 5, '', 60000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(134, '134', 'FUL MEDAMES', 5, '', 40000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(135, '135', 'SHAWARMA DJAZ', 3, '', 50000, '[]', NULL, 1, 1, '', NULL, NULL, NULL, 'Live'),
(136, '136', 'TWO APPLE FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(137, '137', 'GRAPE FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(138, '138', 'BLUEBERRY FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(139, '139', 'ORANGE FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(140, '140', 'BUBBLE GUM FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(141, '141', 'STRAWBERRY FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(142, '142', 'MINT FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(143, '143', 'KIWI FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(144, '144', 'MELON FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(145, '145', 'LEMON FLAVOUR', 5, '', 38000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(146, '146', 'TWO APPLE FLAVOUR', 5, '', 45000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(147, '147', 'GRAPE FLAVOUR', 5, '', 45000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(148, '148', 'BLUEBERRY FLAVOUR', 5, '', 45000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(149, '149', 'ORANGE FLAVOUR', 5, '', 45000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(150, '150', 'BUBBLE GUM FLAVOUR', 5, '', 45000, '', 3, 1, 1, '', 'Veg No', 'Bev No', 'Bar No', 'Live'),
(151, '151', 'STRAWBERRY FLAVOUR', 5, '', 45000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(152, '152', 'MINT FLAVOUR', 5, '', 45000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(153, '153', 'KIWI FLAVOUR', 5, '', 45000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(154, '154', 'MELON FLAVOUR', 5, '', 45000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(155, '155', 'LEMON FLAVOUR', 5, '', 45000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(156, '156', 'MIX FLAVOUR', 5, '', 45000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(157, '157', 'APPLE', 5, '', 70000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(158, '158', 'PINEAPPLE', 5, '', 70000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(159, '159', 'Reffil Muasal', 5, '', 25000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(160, '160', 'Ice Tea', 5, '', 12000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(161, '161', 'Long Black', 5, '', 10000, '[]', 0, 1, 1, '', '', '', '', 'Live'),
(162, '162', 'Paket Promo Furikake', 5, '', 45000, '[]', NULL, 1, 1, NULL, NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_food_menus_ingredients`
--

CREATE TABLE `tbl_food_menus_ingredients` (
  `id` bigint(50) NOT NULL,
  `ingredient_id` int(10) DEFAULT NULL,
  `consumption` float(10,2) DEFAULT NULL,
  `food_menu_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_food_menus_ingredients`
--

INSERT INTO `tbl_food_menus_ingredients` (`id`, `ingredient_id`, `consumption`, `food_menu_id`, `user_id`, `company_id`, `del_status`) VALUES
(4, 3, 0.00, 29, 1, 1, 'Live'),
(7, 1, 0.00, 26, 1, 1, 'Live'),
(8, 1, 0.00, 25, 1, 1, 'Live'),
(9, 1, 0.00, 24, 1, 1, 'Live'),
(10, 1, 0.00, 23, 1, 1, 'Live'),
(11, 1, 0.00, 22, 1, 1, 'Live'),
(12, 1, 0.00, 21, 1, 1, 'Live'),
(13, 1, 0.00, 20, 1, 1, 'Live'),
(14, 1, 0.00, 19, 1, 1, 'Live'),
(15, 1, 0.00, 18, 1, 1, 'Live'),
(16, 1, 0.00, 17, 1, 1, 'Live'),
(17, 1, 0.00, 16, 1, 1, 'Live'),
(18, 1, 0.00, 15, 1, 1, 'Live'),
(19, 1, 0.00, 14, 1, 1, 'Live'),
(20, 1, 0.00, 13, 1, 1, 'Live'),
(21, 1, 0.00, 12, 1, 1, 'Live'),
(22, 1, 0.00, 11, 1, 1, 'Live'),
(23, 1, 0.00, 10, 1, 1, 'Live'),
(24, 1, 0.00, 9, 1, 1, 'Live'),
(25, 1, 0.00, 2, 1, 1, 'Live'),
(26, 1, 0.00, 1, 1, 1, 'Live'),
(27, 1, 0.00, 8, 1, 1, 'Live'),
(29, 1, 0.00, 6, 1, 1, 'Live'),
(30, 1, 0.00, 5, 1, 1, 'Live'),
(31, 1, 0.00, 4, 1, 1, 'Live'),
(32, 1, 0.00, 3, 1, 1, 'Live'),
(33, 1, 0.00, 7, 1, 1, 'Live'),
(36, 1, 0.00, 28, 1, 1, 'Live'),
(37, 1, 0.00, 29, 1, 1, 'Live'),
(40, 1, 0.00, 30, 1, 1, 'Live'),
(41, 6, 0.00, 1, 1, 1, 'Live'),
(42, 6, 0.00, 151, 1, 1, 'Live'),
(43, 6, 0.00, 152, 1, 1, 'Live'),
(44, 6, 0.00, 153, 1, 1, 'Live'),
(45, 6, 0.00, 154, 1, 1, 'Live'),
(46, 6, 0.00, 155, 1, 1, 'Live'),
(47, 6, 0.00, 156, 1, 1, 'Live'),
(48, 6, 0.00, 157, 1, 1, 'Live'),
(50, 6, 0.00, 158, 1, 1, 'Live'),
(51, 6, 0.00, 159, 1, 1, 'Live'),
(52, 43, 0.00, 160, 1, 1, 'Live'),
(54, 18, 0.00, 115, 1, 1, 'Live'),
(55, 18, 0.00, 114, 1, 1, 'Live'),
(56, 46, 0.00, 44, 1, 1, 'Live'),
(57, 46, 0.00, 27, 1, 1, 'Live'),
(58, 58, 0.00, 161, 1, 1, 'Live'),
(59, 37, 1.00, 112, 1, 1, 'Live'),
(60, 31, 0.10, 135, 1, 1, 'Live'),
(61, 7, 0.50, 100, 1, 1, 'Live'),
(62, 66, 1.00, 41, 1, 1, 'Live'),
(63, 16, 0.50, 102, 1, 1, 'Live'),
(64, 83, 0.10, 108, 1, 1, 'Live'),
(65, 7, 0.10, 110, 1, 1, 'Live'),
(66, 37, 1.00, 101, 1, 1, 'Live'),
(67, 61, 0.10, 98, 1, 1, 'Live'),
(68, 31, 1.00, 96, 1, 1, 'Live'),
(69, 61, 1.00, 97, 1, 1, 'Live'),
(70, 37, 1.00, 111, 1, 1, 'Live'),
(71, 61, 0.10, 99, 1, 1, 'Live'),
(72, 95, 50.00, 113, 1, 1, 'Live'),
(73, 17, 1.00, 118, 1, 1, 'Live'),
(74, 38, 1.00, 119, 1, 1, 'Live'),
(75, 66, 0.00, 162, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_food_menus_modifiers`
--

CREATE TABLE `tbl_food_menus_modifiers` (
  `id` bigint(50) NOT NULL,
  `modifier_id` int(10) DEFAULT NULL,
  `food_menu_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_food_menu_categories`
--

CREATE TABLE `tbl_food_menu_categories` (
  `id` int(10) NOT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_food_menu_categories`
--

INSERT INTO `tbl_food_menu_categories` (`id`, `category_name`, `description`, `user_id`, `company_id`, `del_status`) VALUES
(3, 'Bakery', '', 1, 1, 'Deleted'),
(4, 'Resto', '', 1, 1, 'Deleted'),
(5, 'Cafe', '', 1, 1, 'Deleted'),
(6, 'Cafe', 'Coffee, Dessert, Eatry', 1, 1, 'Live'),
(7, 'Shisha', 'Shisha', 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_holds`
--

CREATE TABLE `tbl_holds` (
  `id` int(10) NOT NULL,
  `customer_id` varchar(50) DEFAULT NULL,
  `hold_no` varchar(30) NOT NULL DEFAULT '000000',
  `total_items` int(10) DEFAULT NULL,
  `sub_total` float(10,2) DEFAULT NULL,
  `paid_amount` double(10,2) DEFAULT NULL,
  `due_amount` double(10,2) DEFAULT NULL,
  `due_payment_date` date DEFAULT NULL,
  `disc` varchar(50) DEFAULT NULL,
  `disc_actual` float(10,2) DEFAULT NULL,
  `vat` float(10,2) DEFAULT NULL,
  `total_payable` float(10,2) DEFAULT NULL,
  `payment_method_id` int(10) DEFAULT NULL,
  `table_id` int(10) DEFAULT NULL,
  `total_item_discount_amount` float(10,2) NOT NULL,
  `sub_total_with_discount` float(10,2) NOT NULL,
  `sub_total_discount_amount` float(10,2) NOT NULL,
  `total_discount_amount` float(10,2) NOT NULL,
  `delivery_charge` float(10,2) NOT NULL,
  `sub_total_discount_value` varchar(10) NOT NULL,
  `sub_total_discount_type` varchar(20) NOT NULL,
  `token_no` varchar(50) DEFAULT NULL,
  `sale_date` varchar(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `sale_time` varchar(15) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `waiter_id` int(10) DEFAULT 0,
  `outlet_id` int(10) DEFAULT NULL,
  `order_status` tinyint(1) DEFAULT NULL COMMENT '1=new order, 2=cancelled order, 3=invoiced order',
  `sale_vat_objects` text DEFAULT NULL,
  `order_type` tinyint(1) DEFAULT NULL COMMENT '1=dine in, 2 = take away, 3 = delivery',
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_holds`
--

INSERT INTO `tbl_holds` (`id`, `customer_id`, `hold_no`, `total_items`, `sub_total`, `paid_amount`, `due_amount`, `due_payment_date`, `disc`, `disc_actual`, `vat`, `total_payable`, `payment_method_id`, `table_id`, `total_item_discount_amount`, `sub_total_with_discount`, `sub_total_discount_amount`, `total_discount_amount`, `delivery_charge`, `sub_total_discount_value`, `sub_total_discount_type`, `token_no`, `sale_date`, `date_time`, `sale_time`, `user_id`, `waiter_id`, `outlet_id`, `order_status`, `sale_vat_objects`, `order_type`, `del_status`) VALUES
(1, '1', '1', 3, 155000.00, NULL, NULL, NULL, NULL, NULL, 0.00, 170500.00, NULL, 0, 0.00, 170500.00, 15500.00, 15500.00, 0.00, '10%', 'percentage', NULL, '2020-06-25', '2020-06-25 09:25:05', '2020-06-25 04:2', 1, 4, 1, 1, '[]', 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_holds_details`
--

CREATE TABLE `tbl_holds_details` (
  `id` int(10) NOT NULL,
  `food_menu_id` int(10) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `menu_price_without_discount` float(10,2) NOT NULL,
  `menu_price_with_discount` float(10,2) NOT NULL,
  `menu_unit_price` float(10,2) NOT NULL,
  `menu_vat_percentage` float(10,2) NOT NULL,
  `menu_taxes` text DEFAULT NULL,
  `menu_discount_value` varchar(20) DEFAULT NULL,
  `discount_type` varchar(20) NOT NULL,
  `menu_note` varchar(150) DEFAULT NULL,
  `discount_amount` double(10,2) DEFAULT NULL,
  `holds_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_holds_details`
--

INSERT INTO `tbl_holds_details` (`id`, `food_menu_id`, `menu_name`, `qty`, `menu_price_without_discount`, `menu_price_with_discount`, `menu_unit_price`, `menu_vat_percentage`, `menu_taxes`, `menu_discount_value`, `discount_type`, `menu_note`, `discount_amount`, `holds_id`, `user_id`, `outlet_id`, `del_status`) VALUES
(1, 135, 'SHAWARMA DJAZ (135)', 2, 100000.00, 100000.00, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 1, 1, 1, 'Live'),
(2, 41, 'SEVEN UP (41)', 2, 30000.00, 30000.00, 15000.00, 0.00, '[]', '0', 'plain', '', 0.00, 1, 1, 1, 'Live'),
(3, 104, 'BAFAGEH SNACKS (104)', 1, 25000.00, 25000.00, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 1, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_holds_details_modifiers`
--

CREATE TABLE `tbl_holds_details_modifiers` (
  `id` int(15) NOT NULL,
  `modifier_id` int(15) NOT NULL,
  `modifier_price` float(10,2) NOT NULL,
  `food_menu_id` int(10) NOT NULL,
  `holds_id` int(15) NOT NULL,
  `holds_details_id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `outlet_id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ingredients`
--

CREATE TABLE `tbl_ingredients` (
  `id` int(10) NOT NULL,
  `code` varchar(50) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `purchase_price` float(10,2) DEFAULT NULL,
  `alert_quantity` float(10,2) DEFAULT NULL,
  `unit_id` int(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_ingredients`
--

INSERT INTO `tbl_ingredients` (`id`, `code`, `name`, `category_id`, `purchase_price`, `alert_quantity`, `unit_id`, `user_id`, `company_id`, `del_status`) VALUES
(1, '001', 'Bahan Roti', 1, 0.00, 0.00, 1, 1, 1, 'Deleted'),
(2, '002', 'Terigu cakra', 1, 182000.00, 5.00, 8, 1, 1, 'Deleted'),
(3, '003', 'blue band', 1, 345000.00, 1.00, 9, 1, 1, 'Deleted'),
(4, '004', 'teh lipton', 2, 48000.00, 1.00, 9, 1, 1, 'Deleted'),
(5, '005', 'Gula Pasir', 1, 685000.00, 0.00, 8, 1, 1, 'Deleted'),
(6, '006', 'Muasal Afzal (50gram)', 4, 25000.00, 0.00, 3, 1, 1, 'Live'),
(7, '007', 'Beras', 3, 10000.00, 0.00, 11, 1, 1, 'Live'),
(8, '008', 'Sosis Cash', 3, 30000.00, 0.00, 3, 1, 1, 'Live'),
(9, '009', 'Muasal Afzal (250gram)', 4, 100000.00, 0.00, 9, 1, 1, 'Live'),
(10, '010', 'Muasal Alfakher (50gram)', 4, 50000.00, 0.00, 3, 1, 1, 'Live'),
(11, '011', 'Pivet Shisha', 4, 25000.00, 0.00, 3, 1, 1, 'Live'),
(12, '012', 'Arang Shisha', 4, 25000.00, 0.00, 1, 1, 1, 'Live'),
(13, '013', 'Naget Hemato', 3, 17000.00, 0.00, 1, 1, 1, 'Live'),
(14, '014', 'Naget Okey', 3, 22000.00, 0.00, 1, 1, 1, 'Live'),
(15, '015', 'Baso Unyil', 3, 5000.00, 0.00, 3, 1, 1, 'Live'),
(16, '016', 'Kentang', 3, 30000.00, 0.00, 1, 1, 1, 'Live'),
(17, '017', 'Indomie Goreng', 3, 2500.00, 0.00, 10, 1, 1, 'Live'),
(18, '018', 'Indomie Rebus', 3, 2500.00, 0.00, 10, 1, 1, 'Live'),
(19, '019', 'Cabe Merah', 3, 10000.00, 0.00, 10, 1, 1, 'Live'),
(20, '020', 'Cabe Rawit', 3, 5000.00, 0.00, 10, 1, 1, 'Live'),
(21, '021', 'Bawang Merah', 3, 5000.00, 0.00, 10, 1, 1, 'Live'),
(22, '022', 'Bawang Putih', 3, 5000.00, 0.00, 10, 1, 1, 'Live'),
(23, '023', 'Timun', 3, 10000.00, 0.00, 10, 1, 1, 'Live'),
(24, '024', 'Tomat', 3, 10000.00, 0.00, 10, 1, 1, 'Live'),
(25, '025', 'Daun Selada', 3, 5000.00, 0.00, 10, 1, 1, 'Live'),
(26, '026', 'Daun Mint', 3, 5000.00, 0.00, 10, 1, 1, 'Live'),
(27, '027', 'Garam', 3, 1000.00, 0.00, 10, 1, 1, 'Live'),
(28, '028', 'Sasa', 3, 1000.00, 0.00, 10, 1, 1, 'Live'),
(29, '029', 'Royco', 3, 1000.00, 0.00, 10, 1, 1, 'Live'),
(30, '030', 'Minyak Goreng (gaga)', 3, 12000.00, 0.00, 4, 1, 1, 'Live'),
(31, '031', 'Tepung Terigu', 3, 6000.00, 0.00, 1, 1, 1, 'Live'),
(32, '032', 'Tepung Roti', 3, 6000.00, 0.00, 3, 1, 1, 'Live'),
(33, '033', 'Kecap', 3, 10000.00, 0.00, 3, 1, 1, 'Live'),
(34, '034', 'Saus Cabe', 3, 23000.00, 0.00, 1, 1, 1, 'Live'),
(35, '035', 'Saus Tomat', 3, 18000.00, 0.00, 1, 1, 1, 'Live'),
(36, '036', 'Mayonais', 3, 8000.00, 0.00, 1, 1, 1, 'Live'),
(37, '037', 'Ayam Potong', 3, 40000.00, 0.00, 1, 1, 1, 'Live'),
(38, '038', 'Ayam Pilet', 3, 50000.00, 0.00, 1, 1, 1, 'Live'),
(39, '039', 'Bumbu Ayam', 3, 5000.00, 0.00, 3, 1, 1, 'Live'),
(40, '040', 'Chiken Wings', 3, 45000.00, 0.00, 1, 1, 1, 'Live'),
(41, '041', 'Kornet', 3, 20000.00, 0.00, 3, 1, 1, 'Live'),
(42, '042', 'Keju Batang', 3, 15000.00, 0.00, 3, 1, 1, 'Live'),
(43, '043', 'Es Kristal', 3, 22000.00, 0.00, 1, 1, 1, 'Live'),
(44, '044', 'Gula Pasir', 3, 16000.00, 0.00, 1, 1, 1, 'Live'),
(45, '045', 'Bandrek', 3, 15000.00, 0.00, 3, 1, 1, 'Live'),
(46, '046', 'Millo', 3, 17000.00, 0.00, 10, 1, 1, 'Live'),
(47, '047', 'Ovaltine', 3, 15000.00, 0.00, 10, 1, 1, 'Live'),
(48, '048', 'Lemon Tea', 3, 10500.00, 0.00, 10, 1, 1, 'Live'),
(49, '049', 'Skm Putih', 3, 9000.00, 0.00, 7, 1, 1, 'Live'),
(50, '050', 'Skm Coklat', 3, 9000.00, 0.00, 7, 1, 1, 'Live'),
(51, '051', 'Susu Fn', 3, 16000.00, 0.00, 7, 1, 1, 'Live'),
(52, '052', 'Selai Strowberry', 3, 6000.00, 0.00, 10, 1, 1, 'Live'),
(53, '053', 'Selai Kacang', 3, 15000.00, 0.00, 10, 1, 1, 'Live'),
(54, '054', 'Jeruk', 3, 18000.00, 0.00, 10, 1, 1, 'Live'),
(55, '055', 'Mangga', 3, 20000.00, 0.00, 10, 1, 1, 'Live'),
(56, '056', 'Pisang', 3, 15000.00, 0.00, 10, 1, 1, 'Live'),
(57, '057', 'Semangka', 3, 20000.00, 0.00, 10, 1, 1, 'Live'),
(58, '058', 'Biji Kopi', 3, 165000.00, 0.00, 1, 1, 1, 'Live'),
(59, '059', 'Monin', 3, 135000.00, 0.00, 7, 1, 1, 'Live'),
(60, '060', 'Roti Tawar', 3, 15000.00, 0.00, 3, 1, 1, 'Live'),
(61, '061', 'Ice Cream', 3, 170000.00, 0.00, 4, 1, 1, 'Live'),
(62, '062', 'Mineral Water', 3, 45000.00, 0.00, 9, 1, 1, 'Live'),
(63, '063', 'Lemon', 3, 10000.00, 0.00, 10, 1, 1, 'Live'),
(64, '064', 'Fanta', 3, 110000.00, 0.00, 9, 1, 1, 'Live'),
(65, '065', 'Coca Cola', 3, 110000.00, 0.00, 9, 1, 1, 'Live'),
(66, '066', '7 Up', 3, 160000.00, 0.00, 9, 1, 1, 'Live'),
(67, '067', 'Pepsi', 3, 160000.00, 0.00, 9, 1, 1, 'Live'),
(68, '068', 'Plastik Sampah', 3, 11000.00, 0.00, 3, 1, 1, 'Live'),
(69, '069', 'Diamond Uht', 3, 15000.00, 0.00, 4, 1, 1, 'Live'),
(70, '070', 'Fresh Milk', 3, 190000.00, 0.00, 9, 1, 1, 'Live'),
(71, '071', 'Pincup (Box Take Away)', 3, 40000.00, 0.00, 3, 1, 1, 'Live'),
(72, '072', 'Streapoam Take Away', 3, 8000.00, 0.00, 10, 1, 1, 'Live'),
(73, '073', 'Bawang Bombai', 3, 5000.00, 0.00, 10, 1, 1, 'Live'),
(74, '074', 'Alpukat', 3, 20000.00, 0.00, 10, 1, 1, 'Live'),
(75, '075', 'Sendok Plastik (TakeAway)', 3, 3500.00, 0.00, 3, 1, 1, 'Live'),
(76, '076', 'Tissue Jolly', 3, 3500.00, 0.00, 10, 1, 1, 'Live'),
(77, '077', 'Kipau (BantalCoklat)', 3, 15000.00, 0.00, 3, 1, 1, 'Live'),
(78, '078', 'Margarin', 3, 7000.00, 0.00, 10, 1, 1, 'Live'),
(79, '079', 'Sunlight', 3, 2000.00, 0.00, 10, 1, 1, 'Live'),
(80, '080', 'Spon', 3, 3000.00, 0.00, 10, 1, 1, 'Live'),
(81, '081', 'Tissue Passeo', 3, 9000.00, 0.00, 10, 1, 1, 'Live'),
(82, '082', 'Kunyit', 3, 5000.00, 0.00, 5, 1, 1, 'Live'),
(83, '083', 'Telur Ayam', 3, 23000.00, 0.00, 1, 1, 1, 'Live'),
(84, '084', 'Pulsa Hp Cafe', 3, 15000.00, 0.00, 10, 1, 1, 'Live'),
(85, '085', 'Tissue Passeo', 3, 10000.00, 0.00, 3, 1, 1, 'Live'),
(86, '086', 'Spagethi Pizza', 3, 30000.00, 0.00, 10, 1, 1, 'Live'),
(87, '087', 'Meises Coklat', 3, 8500.00, 0.00, 3, 1, 1, 'Live'),
(88, '088', 'Gobiz', 3, 100000.00, 0.00, 9, 1, 1, 'Live'),
(89, '089', 'Gas  Kecil', 3, 23000.00, 0.00, 1, 1, 1, 'Live'),
(90, '090', 'Nota Kontan', 3, 2500.00, 0.00, 10, 1, 1, 'Live'),
(91, '091', 'Teh Lipton', 3, 50000.00, 0.00, 9, 1, 1, 'Live'),
(92, '092', 'Sosis champ', 3, 16000.00, 0.00, 3, 1, 1, 'Live'),
(93, '093', 'nagget champ', 3, 16000.00, 1.00, 1, 1, 1, 'Live'),
(94, '094', 'Minyak Sayur', 3, 11000.00, 1.00, 1, 1, 1, 'Live'),
(95, '095', 'salada sayur', 3, 5000.00, 1.00, 6, 1, 1, 'Live'),
(96, '096', 'kabel 2x50', 3, 3000.00, 1.00, 10, 1, 1, 'Live'),
(97, '097', 'fit lampu bulat', 3, 9500.00, 1.00, 3, 1, 1, 'Live'),
(98, '098', 'Keju Batang', 3, 10000.00, 0.00, 10, 1, 1, 'Live'),
(99, '099', 'Mentega', 3, 6000.00, 0.00, 3, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ingredient_categories`
--

CREATE TABLE `tbl_ingredient_categories` (
  `id` int(10) NOT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_ingredient_categories`
--

INSERT INTO `tbl_ingredient_categories` (`id`, `category_name`, `description`, `user_id`, `company_id`, `del_status`) VALUES
(1, 'Bahan Menu Roti', '', 1, 1, 'Deleted'),
(2, 'Bahan Resto', '', 1, 1, 'Deleted'),
(3, 'Bahan Menu Cafe', '', 1, 1, 'Live'),
(4, 'Kebutuhan Sisha', '', 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_inventory_adjustment`
--

CREATE TABLE `tbl_inventory_adjustment` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(50) DEFAULT NULL,
  `date` date NOT NULL,
  `note` varchar(200) DEFAULT NULL,
  `employee_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_inventory_adjustment_ingredients`
--

CREATE TABLE `tbl_inventory_adjustment_ingredients` (
  `id` int(10) NOT NULL,
  `ingredient_id` int(10) DEFAULT NULL,
  `consumption_amount` float(10,2) DEFAULT NULL,
  `inventory_adjustment_id` int(10) DEFAULT NULL,
  `consumption_status` enum('Plus','Minus','','') DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_modifiers`
--

CREATE TABLE `tbl_modifiers` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_modifier_ingredients`
--

CREATE TABLE `tbl_modifier_ingredients` (
  `id` bigint(50) NOT NULL,
  `ingredient_id` int(10) DEFAULT NULL,
  `consumption` float(10,2) DEFAULT NULL,
  `modifier_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_notifications`
--

CREATE TABLE `tbl_notifications` (
  `id` bigint(50) NOT NULL,
  `notification` text NOT NULL,
  `outlet_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_notification_bar_kitchen_panel`
--

CREATE TABLE `tbl_notification_bar_kitchen_panel` (
  `id` int(15) NOT NULL,
  `notification` text NOT NULL,
  `outlet_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_orders_table`
--

CREATE TABLE `tbl_orders_table` (
  `id` bigint(50) NOT NULL,
  `persons` int(5) NOT NULL,
  `booking_time` datetime NOT NULL,
  `sale_id` int(10) NOT NULL,
  `sale_no` varchar(20) NOT NULL,
  `outlet_id` int(10) NOT NULL,
  `table_id` int(10) NOT NULL,
  `del_status` varchar(20) NOT NULL DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_orders_table`
--

INSERT INTO `tbl_orders_table` (`id`, `persons`, `booking_time`, `sale_id`, `sale_no`, `outlet_id`, `table_id`, `del_status`) VALUES
(1, 1, '2020-06-28 14:56:00', 18, '000018', 1, 2, 'Deleted');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_outlets`
--

CREATE TABLE `tbl_outlets` (
  `id` int(10) NOT NULL,
  `outlet_name` varchar(50) DEFAULT NULL,
  `outlet_code` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `invoice_print` enum('Yes','No') DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `invoice_footer` varchar(500) DEFAULT NULL,
  `collect_tax` varchar(10) DEFAULT NULL,
  `tax_title` varchar(10) DEFAULT NULL,
  `tax_registration_no` varchar(30) DEFAULT NULL,
  `tax_is_gst` varchar(10) NOT NULL,
  `state_code` varchar(10) DEFAULT NULL,
  `pre_or_post_payment` varchar(500) DEFAULT 'Post Payment',
  `user_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_outlets`
--

INSERT INTO `tbl_outlets` (`id`, `outlet_name`, `outlet_code`, `address`, `phone`, `invoice_print`, `starting_date`, `invoice_footer`, `collect_tax`, `tax_title`, `tax_registration_no`, `tax_is_gst`, `state_code`, `pre_or_post_payment`, `user_id`, `company_id`, `del_status`) VALUES
(1, 'RePOS Cafe (Example)', '000001', 'Jl. Raya Puncak KM. 83 Cisarua, Bogor - Jawa Barat 16750', '0821-1698-2479', 'Yes', '2018-02-18', 'Terimakasih atas kunjungan anda, kami tunggu kembali..', 'No', 'PPN', '11223344', 'No', '', NULL, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_outlet_taxes`
--

CREATE TABLE `tbl_outlet_taxes` (
  `id` int(15) NOT NULL,
  `tax` varchar(50) NOT NULL,
  `outlet_id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL,
  `del_status` varchar(100) NOT NULL DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_payment_methods`
--

CREATE TABLE `tbl_payment_methods` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_payment_methods`
--

INSERT INTO `tbl_payment_methods` (`id`, `name`, `description`, `user_id`, `company_id`, `del_status`) VALUES
(3, 'Cash', '', 1, 1, 'Deleted'),
(4, 'Card', '', 1, 1, 'Live'),
(5, 'Paypal', '', 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_purchase`
--

CREATE TABLE `tbl_purchase` (
  `id` int(10) NOT NULL,
  `reference_no` varchar(50) DEFAULT NULL,
  `supplier_id` int(10) DEFAULT NULL,
  `menu_resto_id` int(10) NOT NULL,
  `date` varchar(15) NOT NULL,
  `subtotal` float DEFAULT NULL,
  `other` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `paid` float DEFAULT NULL,
  `due` float DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_purchase`
--

INSERT INTO `tbl_purchase` (`id`, `reference_no`, `supplier_id`, `menu_resto_id`, `date`, `subtotal`, `other`, `grand_total`, `paid`, `due`, `note`, `user_id`, `outlet_id`, `del_status`) VALUES
(1, '000001', 1, 3, '2020-06-21', NULL, NULL, 800000, 800000, 0, NULL, 1, 1, 'Live'),
(2, '000002', 5, 5, '2020-06-21', NULL, NULL, 825000, 800000, 25000, NULL, 1, 1, 'Live'),
(3, '000003', 4, 3, '2020-06-21', NULL, NULL, 100000, 100000, 0, NULL, 1, 1, 'Live'),
(4, '000004', 3, 5, '2020-06-23', NULL, NULL, 160000, 160000, 0, NULL, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_purchase_ingredients`
--

CREATE TABLE `tbl_purchase_ingredients` (
  `id` int(10) NOT NULL,
  `ingredient_id` int(10) DEFAULT NULL,
  `unit_price` float(10,2) DEFAULT NULL,
  `quantity_amount` float(10,2) DEFAULT NULL,
  `total` float(10,2) DEFAULT NULL,
  `purchase_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_purchase_ingredients`
--

INSERT INTO `tbl_purchase_ingredients` (`id`, `ingredient_id`, `unit_price`, `quantity_amount`, `total`, `purchase_id`, `outlet_id`, `del_status`) VALUES
(1, 66, 160000.00, 5.00, 800000.00, 1, 1, 'Live'),
(3, 74, 20000.00, 5.00, 100000.00, 3, 1, 'Live'),
(4, 58, 165000.00, 5.00, 825000.00, 2, 1, 'Live'),
(5, 66, 160000.00, 1.00, 160000.00, 4, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_register`
--

CREATE TABLE `tbl_register` (
  `id` int(15) NOT NULL,
  `opening_balance` float(10,2) NOT NULL,
  `closing_balance` float(10,2) NOT NULL,
  `opening_balance_date_time` datetime DEFAULT NULL,
  `closing_balance_date_time` datetime NOT NULL,
  `sale_paid_amount` float(10,2) NOT NULL,
  `customer_due_receive` float(10,2) NOT NULL,
  `payment_methods_sale` text NOT NULL,
  `register_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=open,2=closed',
  `user_id` int(15) NOT NULL,
  `outlet_id` int(15) NOT NULL,
  `company_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_register`
--

INSERT INTO `tbl_register` (`id`, `opening_balance`, `closing_balance`, `opening_balance_date_time`, `closing_balance_date_time`, `sale_paid_amount`, `customer_due_receive`, `payment_methods_sale`, `register_status`, `user_id`, `outlet_id`, `company_id`) VALUES
(1, 0.00, 231000.00, '2020-06-21 09:26:18', '2020-06-21 14:45:33', 231000.00, 0.00, '{\"Cash\": 379500.00,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(2, 0.00, 0.00, '2020-06-21 15:13:28', '2020-06-22 15:25:31', 0.00, 0.00, '{\"Cash\": 148500.00,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(3, 0.00, 247500.00, '2020-06-22 15:26:06', '2020-06-22 21:17:18', 247500.00, 0.00, '{\"Cash\": 247500.00,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(5, 0.00, 0.00, '2020-06-23 00:15:31', '2020-06-23 00:47:00', 0.00, 0.00, '{\"Cash\": ,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(6, 0.00, 0.00, '2020-06-23 00:48:15', '2020-06-23 00:48:17', 0.00, 0.00, '{\"Cash\": ,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(7, 0.00, 0.00, '2020-06-23 00:49:18', '2020-06-23 00:50:45', 0.00, 0.00, '{\"Cash\": ,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(8, 0.00, 0.00, '2020-06-23 00:51:01', '2020-06-23 23:59:58', 0.00, 0.00, '{\"Cash\": ,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(9, 0.00, 71500.00, '2020-06-23 01:06:16', '2020-06-23 09:59:43', 71500.00, 0.00, '{\"Cash\": 71500.00,\"Paypal\": ,\"Card\": }', 1, 1, 1, 1),
(10, 0.00, 0.00, '2020-06-24 14:12:50', '2020-06-24 23:59:59', 0.00, 0.00, '{\"Cash\": ,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(11, 0.00, 97900.00, '2020-06-25 16:12:19', '2020-06-25 16:15:53', 97900.00, 0.00, '{\"Cash\": 97900.00,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(12, 0.00, 0.00, '2020-06-25 16:22:55', '2020-06-25 17:53:22', 0.00, 0.00, '{\"Cash\": ,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(13, 0.00, 0.00, '2020-06-25 18:02:58', '2020-06-25 18:03:19', 0.00, 0.00, '{\"Cash\": ,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(14, 0.00, 126500.00, '2020-06-27 10:17:01', '2020-06-27 11:20:03', 126500.00, 0.00, '{\"Cash\": 126500.00,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(15, 0.00, 115000.00, '2020-06-27 11:20:12', '2020-06-27 12:33:02', 115000.00, 0.00, '{\"Cash\": 115000.00,\"Paypal\": ,\"Card\": }', 2, 1, 1, 1),
(16, 0.00, 0.00, '2020-06-27 23:34:16', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(17, 0.00, 0.00, '2020-06-28 14:55:18', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(18, 0.00, 0.00, '2020-06-29 20:39:27', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(19, 0.00, 0.00, '2020-06-30 10:32:53', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(20, 0.00, 0.00, '2020-07-02 13:44:15', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(21, 0.00, 0.00, '2020-07-09 18:36:20', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(22, 0.00, 0.00, '2020-07-09 18:36:27', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(23, 0.00, 0.00, '2020-07-17 13:03:25', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(24, 0.00, 0.00, '2020-07-24 14:24:19', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(25, 0.00, 0.00, '2020-07-25 13:14:58', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(26, 0.00, 0.00, '2020-08-12 17:59:37', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(27, 0.00, 0.00, '2020-08-19 22:05:27', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(28, 0.00, 0.00, '2020-08-19 22:05:35', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(29, 0.00, 0.00, '2020-08-26 16:49:08', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(30, 0.00, 0.00, '2020-08-26 16:49:28', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(31, 0.00, 0.00, '2020-08-26 16:49:31', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(32, 0.00, 0.00, '2020-08-26 16:49:37', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(33, 0.00, 0.00, '2020-08-26 16:49:40', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(34, 0.00, 0.00, '2020-08-26 16:49:45', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(35, 0.00, 0.00, '2020-09-04 00:11:17', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(36, 0.00, 0.00, '2020-09-08 14:02:42', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(37, 0.00, 0.00, '2020-09-14 10:56:32', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1),
(38, 0.00, 0.00, '2021-02-24 21:14:19', '0000-00-00 00:00:00', 0.00, 0.00, '', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sales`
--

CREATE TABLE `tbl_sales` (
  `id` int(10) NOT NULL,
  `customer_id` varchar(50) DEFAULT NULL,
  `sale_no` varchar(30) NOT NULL DEFAULT '000000',
  `total_items` int(10) DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `paid_amount` double(10,2) DEFAULT NULL,
  `due_amount` float DEFAULT NULL,
  `disc` varchar(50) DEFAULT NULL,
  `disc_actual` float DEFAULT NULL,
  `vat` float DEFAULT NULL,
  `total_payable` float DEFAULT NULL,
  `payment_method_id` int(10) DEFAULT NULL,
  `close_time` time NOT NULL,
  `table_id` int(10) DEFAULT NULL,
  `total_item_discount_amount` float NOT NULL,
  `sub_total_with_discount` float NOT NULL,
  `sub_total_discount_amount` float NOT NULL,
  `total_discount_amount` float NOT NULL,
  `delivery_charge` float NOT NULL,
  `sub_total_discount_value` varchar(10) NOT NULL,
  `sub_total_discount_type` varchar(20) NOT NULL,
  `sale_date` varchar(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `order_time` time NOT NULL,
  `cooking_start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cooking_done_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `user_id` int(10) DEFAULT NULL,
  `waiter_id` int(10) NOT NULL DEFAULT 0,
  `outlet_id` int(10) DEFAULT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order, 2=invoiced order, 3=closed order',
  `order_type` tinyint(1) NOT NULL COMMENT '1=dine in, 2 = take away, 3 = delivery',
  `del_status` varchar(50) DEFAULT 'Live',
  `sale_vat_objects` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_sales`
--

INSERT INTO `tbl_sales` (`id`, `customer_id`, `sale_no`, `total_items`, `sub_total`, `paid_amount`, `due_amount`, `disc`, `disc_actual`, `vat`, `total_payable`, `payment_method_id`, `close_time`, `table_id`, `total_item_discount_amount`, `sub_total_with_discount`, `sub_total_discount_amount`, `total_discount_amount`, `delivery_charge`, `sub_total_discount_value`, `sub_total_discount_type`, `sale_date`, `date_time`, `order_time`, `cooking_start_time`, `cooking_done_time`, `modified`, `user_id`, `waiter_id`, `outlet_id`, `order_status`, `order_type`, `del_status`, `sale_vat_objects`) VALUES
(1, '1', '000001', 1, 15000, 16500.00, NULL, NULL, NULL, 0, 16500, 3, '10:15:23', NULL, 0, 16500, 1500, 1500, 0, '10%', 'percentage', '2020-06-21', '2020-06-21 03:15:23', '10:15:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(2, '2', '000002', 1, 15000, 16500.00, NULL, NULL, NULL, 0, 16500, 3, '10:17:27', NULL, 0, 16500, 1500, 1500, 0, '10%', 'percentage', '2020-06-21', '2020-06-21 03:17:27', '10:17:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(3, '1', '000003', 4, 145000, 159500.00, NULL, NULL, NULL, 0, 159500, 3, '10:51:34', NULL, 0, 159500, 14500, 14500, 0, '10%', 'percentage', '2020-06-21', '2020-06-21 03:51:34', '10:51:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 1, 'Live', '[]'),
(4, '2', '000004', 3, 70000, 77000.00, NULL, NULL, NULL, 0, 77000, 3, '11:13:15', NULL, 0, 77000, 7000, 7000, 0, '10%', 'percentage', '2020-06-21', '2020-06-21 04:13:15', '11:13:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(5, '1', '000005', 1, 50000, 55000.00, NULL, NULL, NULL, 0, 55000, 3, '11:56:21', NULL, 0, 55000, 5000, 5000, 0, '10%', 'percentage', '2020-06-21', '2020-06-21 04:56:21', '11:56:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(6, '2', '000006', 1, 50000, 55000.00, NULL, NULL, NULL, 0, 55000, 3, '11:57:47', NULL, 0, 55000, 5000, 5000, 0, '10%', 'percentage', '2020-06-21', '2020-06-21 04:57:47', '11:57:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 1, 'Live', '[]'),
(7, '2', '000007', 4, 135000, 148500.00, NULL, NULL, NULL, 0, 148500, 3, '10:33:12', NULL, 0, 148500, 13500, 13500, 0, '10%', 'percentage', '2020-06-22', '2020-06-22 03:33:12', '10:33:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(8, '1', '000008', 3, 125000, 137500.00, NULL, NULL, NULL, 0, 137500, 3, '15:27:33', NULL, 0, 137500, 12500, 12500, 0, '10%', 'percentage', '2020-06-22', '2020-06-22 08:27:33', '15:27:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 1, 'Live', '[]'),
(9, '1', '000009', 2, 100000, 110000.00, NULL, NULL, NULL, 0, 110000, 3, '20:09:00', NULL, 0, 110000, 10000, 10000, 0, '10%', 'percentage', '2020-06-22', '2020-06-22 13:09:00', '20:09:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(10, '1', '000010', 2, 65000, 71500.00, NULL, NULL, NULL, 0, 71500, 3, '01:20:52', NULL, 0, 71500, 6500, 6500, 0, '10%', 'percentage', '2020-06-23', '2020-06-22 18:20:52', '01:20:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(11, '2', '000011', 3, 100000, 110000.00, NULL, NULL, NULL, 0, 110000, 3, '10:06:15', NULL, 0, 110000, 10000, 10000, 0, '10%', 'percentage', '2020-06-23', '2020-06-23 03:06:15', '10:06:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 1, 'Live', '[]'),
(12, '1', '000012', 4, 89000, 97900.00, NULL, NULL, NULL, 0, 97900, 3, '16:13:59', NULL, 0, 97900, 8900, 8900, 0, '10%', 'percentage', '2020-06-25', '2020-06-25 09:13:59', '16:13:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(13, '1', '000013', 2, 65000, 71500.00, NULL, NULL, NULL, 0, 71500, 3, '10:17:42', NULL, 0, 71500, 6500, 6500, 0, '10%', 'percentage', '2020-06-27', '2020-06-27 03:17:42', '10:17:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(14, '1', '000014', 2, 50000, 55000.00, NULL, NULL, NULL, 0, 55000, 3, '10:24:14', NULL, 0, 55000, 5000, 5000, 0, '10%', 'percentage', '2020-06-27', '2020-06-27 03:24:14', '10:24:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(15, '1', '000015', 1, 45000, 45000.00, NULL, NULL, NULL, 0, 45000, 3, '11:23:08', NULL, 0, 45000, 0, 0, 0, '0%', 'percentage', '2020-06-27', '2020-06-27 04:23:08', '11:23:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(16, '1', '000016', 1, 45000, 45000.00, NULL, NULL, NULL, 0, 45000, 3, '11:24:06', NULL, 0, 45000, 0, 0, 0, '0%', 'percentage', '2020-06-27', '2020-06-27 04:24:06', '11:24:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 2, 'Live', '[]'),
(17, '1', '000017', 1, 25000, 25000.00, NULL, NULL, NULL, 0, 25000, 3, '12:27:48', NULL, 0, 25000, 0, 0, 0, '0%', 'percentage', '2020-06-27', '2020-06-27 05:27:48', '12:27:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 1, 'Live', '[]'),
(18, '1', '000018', 3, 75000, 75000.00, NULL, NULL, NULL, 0, 75000, 3, '14:56:00', NULL, 0, 75000, 0, 0, 0, '0%', 'percentage', '2020-06-28', '2020-06-28 07:56:00', '14:56:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 1, 'Live', '[]'),
(19, '1', '000019', 2, 70000, 70000.00, NULL, NULL, NULL, 0, 70000, 3, '13:03:59', NULL, 0, 70000, 0, 0, 0, '0%', 'percentage', '2020-07-17', '2020-07-17 06:03:59', '13:03:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'No', 1, 4, 1, 3, 1, 'Live', '[]');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sales_details`
--

CREATE TABLE `tbl_sales_details` (
  `id` bigint(50) NOT NULL,
  `food_menu_id` int(10) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `menu_price_without_discount` float(10,2) NOT NULL,
  `menu_price_with_discount` float NOT NULL,
  `menu_unit_price` float(10,2) NOT NULL,
  `menu_vat_percentage` float(10,2) NOT NULL,
  `menu_taxes` text DEFAULT NULL,
  `menu_discount_value` varchar(20) DEFAULT NULL,
  `discount_type` varchar(20) NOT NULL,
  `menu_note` varchar(150) DEFAULT NULL,
  `discount_amount` double(10,2) DEFAULT NULL,
  `item_type` varchar(50) DEFAULT NULL,
  `cooking_status` varchar(30) DEFAULT NULL,
  `cooking_start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cooking_done_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `previous_id` bigint(50) NOT NULL,
  `sales_id` int(10) DEFAULT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_sales_details`
--

INSERT INTO `tbl_sales_details` (`id`, `food_menu_id`, `menu_name`, `qty`, `menu_price_without_discount`, `menu_price_with_discount`, `menu_unit_price`, `menu_vat_percentage`, `menu_taxes`, `menu_discount_value`, `discount_type`, `menu_note`, `discount_amount`, `item_type`, `cooking_status`, `cooking_start_time`, `cooking_done_time`, `previous_id`, `sales_id`, `order_status`, `user_id`, `outlet_id`, `del_status`) VALUES
(1, 41, 'SEVEN UP (41)', 1, 15000.00, 15000, 15000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 0, 1, 1, 'Live'),
(2, 41, 'SEVEN UP (41)', 1, 15000.00, 15000, 15000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 2, 0, 1, 1, 'Live'),
(3, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 3, 0, 1, 1, 'Live'),
(4, 106, 'PISANG CRISPY (106)', 1, 20000.00, 20000, 20000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 3, 0, 1, 1, 'Live'),
(5, 100, 'Nasi Mandi (100)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 3, 0, 1, 1, 'Live'),
(6, 109, 'NASI GORENG KORNET (109)', 1, 25000.00, 25000, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 3, 0, 1, 1, 'Live'),
(7, 107, 'CHIKEN CRISPY (107)', 1, 30000.00, 30000, 30000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 4, 0, 1, 1, 'Live'),
(8, 105, 'GOSUKE (105)', 1, 25000.00, 25000, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 4, 0, 1, 1, 'Live'),
(9, 41, 'SEVEN UP (41)', 1, 15000.00, 15000, 15000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 4, 0, 1, 1, 'Live'),
(10, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 5, 0, 1, 1, 'Live'),
(11, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 6, 0, 1, 1, 'Live'),
(12, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '0', '0', 'plain', '', 0.00, 'Bar Item', 'undefined', '1970-01-01 07:00:00', '1970-01-01 07:00:00', 0, 7, 0, 1, 1, 'Live'),
(13, 41, 'SEVEN UP (41)', 1, 15000.00, 15000, 15000.00, 0.00, '0', '0', 'plain', '', 0.00, 'Bar Item', 'undefined', '1970-01-01 07:00:00', '1970-01-01 07:00:00', 0, 7, 0, 1, 1, 'Live'),
(14, 106, 'PISANG CRISPY (106)', 1, 20000.00, 20000, 20000.00, 0.00, '0', '0', 'plain', '', 0.00, 'Kitchen Item', 'undefined', '1970-01-01 07:00:00', '1970-01-01 07:00:00', 0, 7, 0, 1, 1, 'Live'),
(15, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 7, 0, 1, 1, 'Live'),
(16, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 8, 0, 1, 1, 'Live'),
(17, 100, 'Nasi Mandi (100)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 8, 0, 1, 1, 'Live'),
(18, 104, 'BAFAGEH SNACKS (104)', 1, 25000.00, 25000, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 8, 0, 1, 1, 'Live'),
(19, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 9, 0, 1, 1, 'Live'),
(20, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 20, 9, 0, 1, 1, 'Live'),
(21, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 21, 10, 0, 1, 1, 'Live'),
(22, 41, 'SEVEN UP (41)', 1, 15000.00, 15000, 15000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 22, 10, 0, 1, 1, 'Live'),
(23, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, 11, 0, 1, 1, 'Live'),
(24, 104, 'BAFAGEH SNACKS (104)', 1, 25000.00, 25000, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 24, 11, 0, 1, 1, 'Live'),
(25, 103, 'DADAR GULUNG COKLAT (103)', 1, 25000.00, 25000, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 25, 11, 0, 1, 1, 'Live'),
(26, 80, 'LATTE IRIS (80)', 1, 22000.00, 22000, 22000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 26, 12, 0, 1, 1, 'Live'),
(27, 76, 'LATTE VANILLA (76)', 1, 22000.00, 22000, 22000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 27, 12, 0, 1, 1, 'Live'),
(28, 85, 'PISANG BAKAR (85)', 1, 20000.00, 20000, 20000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 28, 12, 0, 1, 1, 'Live'),
(29, 101, 'CHIKEN WINGS (101)', 1, 25000.00, 25000, 25000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 29, 12, 0, 1, 1, 'Live'),
(30, 135, 'SHAWARMA DJAZ (135)', 1, 50000.00, 50000, 50000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 30, 13, 0, 1, 1, 'Live'),
(31, 41, 'SEVEN UP (41)', 1, 15000.00, 15000, 15000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 31, 13, 0, 1, 1, 'Live'),
(32, 104, 'BAFAGEH SNACKS (104)', 1, 25000.00, 25000, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 32, 14, 0, 1, 1, 'Live'),
(33, 109, 'NASI GORENG KORNET (109)', 1, 25000.00, 25000, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 33, 14, 0, 1, 1, 'Live'),
(34, 162, 'Paket Promo Furikake (162)', 1, 45000.00, 45000, 45000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 34, 15, 0, 1, 1, 'Live'),
(35, 162, 'Paket Promo Furikake (162)', 1, 45000.00, 45000, 45000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 35, 16, 0, 1, 1, 'Live'),
(36, 94, 'OMELETE (94)', 1, 25000.00, 25000, 25000.00, 0.00, '\"\"', '0', 'plain', '', 0.00, 'Kitchen Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 36, 17, 0, 1, 1, 'Live'),
(37, 101, 'CHIKEN WINGS (101)', 1, 25000.00, 25000, 25000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 37, 18, 0, 1, 1, 'Live'),
(38, 102, 'KENTANG MIX NUGGET (102)', 1, 25000.00, 25000, 25000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 38, 18, 0, 1, 1, 'Live'),
(39, 98, 'ICE CREAM GORENG (98)', 1, 25000.00, 25000, 25000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 39, 18, 0, 1, 1, 'Live'),
(40, 101, 'CHIKEN WINGS (101)', 1, 25000.00, 25000, 25000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 40, 19, 0, 1, 1, 'Live'),
(41, 162, 'Paket Promo Furikake (162)', 1, 45000.00, 45000, 45000.00, 0.00, '[]', '0', 'plain', '', 0.00, 'Bar Item', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 41, 19, 0, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sales_details_modifiers`
--

CREATE TABLE `tbl_sales_details_modifiers` (
  `id` bigint(50) NOT NULL,
  `modifier_id` int(15) NOT NULL,
  `modifier_price` float(10,2) NOT NULL,
  `food_menu_id` int(10) NOT NULL,
  `sales_id` int(15) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `sales_details_id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `outlet_id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sale_consumptions`
--

CREATE TABLE `tbl_sale_consumptions` (
  `id` bigint(50) NOT NULL,
  `sale_id` int(10) DEFAULT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_sale_consumptions`
--

INSERT INTO `tbl_sale_consumptions` (`id`, `sale_id`, `order_status`, `user_id`, `outlet_id`, `del_status`) VALUES
(1, 1, 0, 1, 1, 'Live'),
(2, 2, 0, 1, 1, 'Live'),
(3, 3, 0, 1, 1, 'Live'),
(4, 4, 0, 1, 1, 'Live'),
(5, 5, 0, 1, 1, 'Live'),
(6, 6, 0, 1, 1, 'Live'),
(7, 7, 0, 1, 1, 'Live'),
(8, 8, 0, 1, 1, 'Live'),
(9, 9, 0, 1, 1, 'Live'),
(10, 10, 0, 1, 1, 'Live'),
(11, 11, 0, 1, 1, 'Live'),
(12, 12, 0, 1, 1, 'Live'),
(13, 13, 0, 1, 1, 'Live'),
(14, 14, 0, 1, 1, 'Live'),
(15, 15, 0, 1, 1, 'Live'),
(16, 16, 0, 1, 1, 'Live'),
(17, 17, 0, 1, 1, 'Live'),
(18, 18, 0, 1, 1, 'Live'),
(19, 19, 0, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sale_consumptions_of_menus`
--

CREATE TABLE `tbl_sale_consumptions_of_menus` (
  `id` bigint(50) NOT NULL,
  `ingredient_id` int(10) DEFAULT NULL,
  `consumption` float(10,2) DEFAULT NULL,
  `sale_consumption_id` int(10) DEFAULT NULL,
  `sales_id` int(10) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `food_menu_id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_sale_consumptions_of_menus`
--

INSERT INTO `tbl_sale_consumptions_of_menus` (`id`, `ingredient_id`, `consumption`, `sale_consumption_id`, `sales_id`, `order_status`, `food_menu_id`, `user_id`, `outlet_id`, `del_status`) VALUES
(1, 66, 1.00, 2, 2, 0, 41, 1, 1, 'Live'),
(2, 31, 0.10, 3, 3, 0, 135, 1, 1, 'Live'),
(3, 7, 0.50, 3, 3, 0, 100, 1, 1, 'Live'),
(4, 66, 1.00, 4, 4, 0, 41, 1, 1, 'Live'),
(5, 31, 0.10, 5, 5, 0, 135, 1, 1, 'Live'),
(6, 31, 0.10, 6, 6, 0, 135, 1, 1, 'Live'),
(7, 31, 0.10, 7, 7, 0, 135, 1, 1, 'Live'),
(8, 66, 1.00, 7, 7, 0, 41, 1, 1, 'Live'),
(9, 31, 0.10, 7, 7, 0, 135, 1, 1, 'Live'),
(10, 31, 0.10, 8, 8, 0, 135, 1, 1, 'Live'),
(11, 7, 0.50, 8, 8, 0, 100, 1, 1, 'Live'),
(12, 31, 0.10, 9, 9, 0, 135, 1, 1, 'Live'),
(13, 31, 0.10, 9, 9, 0, 135, 1, 1, 'Live'),
(14, 31, 0.10, 10, 10, 0, 135, 1, 1, 'Live'),
(15, 66, 1.00, 10, 10, 0, 41, 1, 1, 'Live'),
(16, 31, 0.10, 11, 11, 0, 135, 1, 1, 'Live'),
(17, 37, 1.00, 12, 12, 0, 101, 1, 1, 'Live'),
(18, 31, 0.10, 13, 13, 0, 135, 1, 1, 'Live'),
(19, 66, 1.00, 13, 13, 0, 41, 1, 1, 'Live'),
(20, 66, 0.00, 15, 15, 0, 162, 1, 1, 'Live'),
(21, 66, 0.00, 16, 16, 0, 162, 1, 1, 'Live'),
(22, 37, 1.00, 18, 18, 0, 101, 1, 1, 'Live'),
(23, 16, 0.50, 18, 18, 0, 102, 1, 1, 'Live'),
(24, 61, 0.10, 18, 18, 0, 98, 1, 1, 'Live'),
(25, 37, 1.00, 19, 19, 0, 101, 1, 1, 'Live'),
(26, 66, 0.00, 19, 19, 0, 162, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sale_consumptions_of_modifiers_of_menus`
--

CREATE TABLE `tbl_sale_consumptions_of_modifiers_of_menus` (
  `id` bigint(50) NOT NULL,
  `ingredient_id` int(10) DEFAULT NULL,
  `consumption` float(10,2) DEFAULT NULL,
  `sale_consumption_id` int(10) DEFAULT NULL,
  `sales_id` int(10) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `food_menu_id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL,
  `date_format` varchar(20) DEFAULT NULL,
  `time_zone` varchar(50) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data untuk tabel `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `date_format`, `time_zone`, `currency`, `company_id`, `del_status`) VALUES
(4, 'd/m/Y', 'Asia/Jakarta', 'Rp', 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sms_settings`
--

CREATE TABLE `tbl_sms_settings` (
  `id` int(11) NOT NULL,
  `email_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Live'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_sms_settings`
--

INSERT INTO `tbl_sms_settings` (`id`, `email_address`, `password`, `company_id`, `del_status`) VALUES
(1, '', '', 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_suppliers`
--

CREATE TABLE `tbl_suppliers` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `contact_person` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_suppliers`
--

INSERT INTO `tbl_suppliers` (`id`, `name`, `contact_person`, `phone`, `email`, `address`, `description`, `user_id`, `company_id`, `del_status`) VALUES
(1, 'Toko Mitha', 'Dadang', '0812121212121', '', 'Cisarua', 'Supplier kebutuhan shisha', 1, 1, 'Live'),
(2, 'Alfamart', 'Kosong', '0888888', '', '', '', 1, 1, 'Deleted'),
(3, 'Alfamart', 'Udin', '0798689', '', '', 'Pembelian via online', 1, 1, 'Live'),
(4, 'Toko Armand', 'Beben', '0897452246', '', 'Cisarua', 'Supplier kebutuhan snack', 1, 1, 'Live'),
(5, 'Spl Kopi', 'Bang Ijud', '0578884314', '', 'Bogor', 'Supplier kebutuhan kopi', 1, 1, 'Live'),
(6, 'Sayur Mayur', 'Pepen', '024536215', '', 'Cisarua', 'Supplier kebutuhan dapur', 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_supplier_payments`
--

CREATE TABLE `tbl_supplier_payments` (
  `id` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `menu_resto_id` int(10) NOT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tables`
--

CREATE TABLE `tbl_tables` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `sit_capacity` varchar(50) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_tables`
--

INSERT INTO `tbl_tables` (`id`, `name`, `sit_capacity`, `position`, `description`, `user_id`, `outlet_id`, `company_id`, `del_status`) VALUES
(1, 'Meja 1', '6', 'Bangku Tinggi', 'Dekat Lift', 1, 1, 1, 'Deleted'),
(2, 'Meja 2', '6', 'Bangku tinggi', 'Dekat Lift', 1, 1, 1, 'Deleted'),
(3, 'Meja 3', '4', 'Teras', '', 1, 1, 1, 'Deleted'),
(4, 'Meja 8', '8', 'Sopa depan', '', 1, 1, 1, 'Deleted'),
(5, 'Meja 4', '4', 'Teras', '', 1, 1, 1, 'Deleted'),
(6, 'Meja 01', '6', 'Balkon Lt. 1', 'Meja Kursi Barstool', 1, 1, 1, 'Live'),
(7, 'Meja 02', '2', 'Balkon Lt. 1', 'Meja Kursi Standar', 1, 1, 1, 'Live'),
(8, 'Meja 03', '2', 'Balkon Lt. 1', 'Meja Kursi Standar', 1, 1, 1, 'Live'),
(9, 'Meja 04', '2', 'Balkon Lt. 1', 'Meja Kursi Standar', 1, 1, 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_temp_kot`
--

CREATE TABLE `tbl_temp_kot` (
  `id` int(10) NOT NULL,
  `temp_kot_info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_time_zone`
--

CREATE TABLE `tbl_time_zone` (
  `id` int(10) NOT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `zone_name` varchar(35) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data untuk tabel `tbl_time_zone`
--

INSERT INTO `tbl_time_zone` (`id`, `country_code`, `zone_name`, `del_status`) VALUES
(1, 'AD', 'Europe/Andorra', 'Live'),
(2, 'AE', 'Asia/Dubai', 'Live'),
(3, 'AF', 'Asia/Kabul', 'Live'),
(4, 'AG', 'America/Antigua', 'Live'),
(5, 'AI', 'America/Anguilla', 'Live'),
(6, 'AL', 'Europe/Tirane', 'Live'),
(7, 'AM', 'Asia/Yerevan', 'Live'),
(8, 'AO', 'Africa/Luanda', 'Live'),
(9, 'AQ', 'Antarctica/McMurdo', 'Live'),
(10, 'AQ', 'Antarctica/Casey', 'Live'),
(11, 'AQ', 'Antarctica/Davis', 'Live'),
(12, 'AQ', 'Antarctica/DumontDUrville', 'Live'),
(13, 'AQ', 'Antarctica/Mawson', 'Live'),
(14, 'AQ', 'Antarctica/Palmer', 'Live'),
(15, 'AQ', 'Antarctica/Rothera', 'Live'),
(16, 'AQ', 'Antarctica/Syowa', 'Live'),
(17, 'AQ', 'Antarctica/Troll', 'Live'),
(18, 'AQ', 'Antarctica/Vostok', 'Live'),
(19, 'AR', 'America/Argentina/Buenos_Aires', 'Live'),
(20, 'AR', 'America/Argentina/Cordoba', 'Live'),
(21, 'AR', 'America/Argentina/Salta', 'Live'),
(22, 'AR', 'America/Argentina/Jujuy', 'Live'),
(23, 'AR', 'America/Argentina/Tucuman', 'Live'),
(24, 'AR', 'America/Argentina/Catamarca', 'Live'),
(25, 'AR', 'America/Argentina/La_Rioja', 'Live'),
(26, 'AR', 'America/Argentina/San_Juan', 'Live'),
(27, 'AR', 'America/Argentina/Mendoza', 'Live'),
(28, 'AR', 'America/Argentina/San_Luis', 'Live'),
(29, 'AR', 'America/Argentina/Rio_Gallegos', 'Live'),
(30, 'AR', 'America/Argentina/Ushuaia', 'Live'),
(31, 'AS', 'Pacific/Pago_Pago', 'Live'),
(32, 'AT', 'Europe/Vienna', 'Live'),
(33, 'AU', 'Australia/Lord_Howe', 'Live'),
(34, 'AU', 'Antarctica/Macquarie', 'Live'),
(35, 'AU', 'Australia/Hobart', 'Live'),
(36, 'AU', 'Australia/Currie', 'Live'),
(37, 'AU', 'Australia/Melbourne', 'Live'),
(38, 'AU', 'Australia/Sydney', 'Live'),
(39, 'AU', 'Australia/Broken_Hill', 'Live'),
(40, 'AU', 'Australia/Brisbane', 'Live'),
(41, 'AU', 'Australia/Lindeman', 'Live'),
(42, 'AU', 'Australia/Adelaide', 'Live'),
(43, 'AU', 'Australia/Darwin', 'Live'),
(44, 'AU', 'Australia/Perth', 'Live'),
(45, 'AU', 'Australia/Eucla', 'Live'),
(46, 'AW', 'America/Aruba', 'Live'),
(47, 'AX', 'Europe/Mariehamn', 'Live'),
(48, 'AZ', 'Asia/Baku', 'Live'),
(49, 'BA', 'Europe/Sarajevo', 'Live'),
(50, 'BB', 'America/Barbados', 'Live'),
(51, 'BD', 'Asia/Dhaka', 'Live'),
(52, 'BE', 'Europe/Brussels', 'Live'),
(53, 'BF', 'Africa/Ouagadougou', 'Live'),
(54, 'BG', 'Europe/Sofia', 'Live'),
(55, 'BH', 'Asia/Bahrain', 'Live'),
(56, 'BI', 'Africa/Bujumbura', 'Live'),
(57, 'BJ', 'Africa/Porto-Novo', 'Live'),
(58, 'BL', 'America/St_Barthelemy', 'Live'),
(59, 'BM', 'Atlantic/Bermuda', 'Live'),
(60, 'BN', 'Asia/Brunei', 'Live'),
(61, 'BO', 'America/La_Paz', 'Live'),
(62, 'BQ', 'America/Kralendijk', 'Live'),
(63, 'BR', 'America/Noronha', 'Live'),
(64, 'BR', 'America/Belem', 'Live'),
(65, 'BR', 'America/Fortaleza', 'Live'),
(66, 'BR', 'America/Recife', 'Live'),
(67, 'BR', 'America/Araguaina', 'Live'),
(68, 'BR', 'America/Maceio', 'Live'),
(69, 'BR', 'America/Bahia', 'Live'),
(70, 'BR', 'America/Sao_Paulo', 'Live'),
(71, 'BR', 'America/Campo_Grande', 'Live'),
(72, 'BR', 'America/Cuiaba', 'Live'),
(73, 'BR', 'America/Santarem', 'Live'),
(74, 'BR', 'America/Porto_Velho', 'Live'),
(75, 'BR', 'America/Boa_Vista', 'Live'),
(76, 'BR', 'America/Manaus', 'Live'),
(77, 'BR', 'America/Eirunepe', 'Live'),
(78, 'BR', 'America/Rio_Branco', 'Live'),
(79, 'BS', 'America/Nassau', 'Live'),
(80, 'BT', 'Asia/Thimphu', 'Live'),
(81, 'BW', 'Africa/Gaborone', 'Live'),
(82, 'BY', 'Europe/Minsk', 'Live'),
(83, 'BZ', 'America/Belize', 'Live'),
(84, 'CA', 'America/St_Johns', 'Live'),
(85, 'CA', 'America/Halifax', 'Live'),
(86, 'CA', 'America/Glace_Bay', 'Live'),
(87, 'CA', 'America/Moncton', 'Live'),
(88, 'CA', 'America/Goose_Bay', 'Live'),
(89, 'CA', 'America/Blanc-Sablon', 'Live'),
(90, 'CA', 'America/Toronto', 'Live'),
(91, 'CA', 'America/Nipigon', 'Live'),
(92, 'CA', 'America/Thunder_Bay', 'Live'),
(93, 'CA', 'America/Iqaluit', 'Live'),
(94, 'CA', 'America/Pangnirtung', 'Live'),
(95, 'CA', 'America/Atikokan', 'Live'),
(96, 'CA', 'America/Winnipeg', 'Live'),
(97, 'CA', 'America/Rainy_River', 'Live'),
(98, 'CA', 'America/Resolute', 'Live'),
(99, 'CA', 'America/Rankin_Inlet', 'Live'),
(100, 'CA', 'America/Regina', 'Live'),
(101, 'CA', 'America/Swift_Current', 'Live'),
(102, 'CA', 'America/Edmonton', 'Live'),
(103, 'CA', 'America/Cambridge_Bay', 'Live'),
(104, 'CA', 'America/Yellowknife', 'Live'),
(105, 'CA', 'America/Inuvik', 'Live'),
(106, 'CA', 'America/Creston', 'Live'),
(107, 'CA', 'America/Dawson_Creek', 'Live'),
(108, 'CA', 'America/Fort_Nelson', 'Live'),
(109, 'CA', 'America/Vancouver', 'Live'),
(110, 'CA', 'America/Whitehorse', 'Live'),
(111, 'CA', 'America/Dawson', 'Live'),
(112, 'CC', 'Indian/Cocos', 'Live'),
(113, 'CD', 'Africa/Kinshasa', 'Live'),
(114, 'CD', 'Africa/Lubumbashi', 'Live'),
(115, 'CF', 'Africa/Bangui', 'Live'),
(116, 'CG', 'Africa/Brazzaville', 'Live'),
(117, 'CH', 'Europe/Zurich', 'Live'),
(118, 'CI', 'Africa/Abidjan', 'Live'),
(119, 'CK', 'Pacific/Rarotonga', 'Live'),
(120, 'CL', 'America/Santiago', 'Live'),
(121, 'CL', 'America/Punta_Arenas', 'Live'),
(122, 'CL', 'Pacific/Easter', 'Live'),
(123, 'CM', 'Africa/Douala', 'Live'),
(124, 'CN', 'Asia/Shanghai', 'Live'),
(125, 'CN', 'Asia/Urumqi', 'Live'),
(126, 'CO', 'America/Bogota', 'Live'),
(127, 'CR', 'America/Costa_Rica', 'Live'),
(128, 'CU', 'America/Havana', 'Live'),
(129, 'CV', 'Atlantic/Cape_Verde', 'Live'),
(130, 'CW', 'America/Curacao', 'Live'),
(131, 'CX', 'Indian/Christmas', 'Live'),
(132, 'CY', 'Asia/Nicosia', 'Live'),
(133, 'CY', 'Asia/Famagusta', 'Live'),
(134, 'CZ', 'Europe/Prague', 'Live'),
(135, 'DE', 'Europe/Berlin', 'Live'),
(136, 'DE', 'Europe/Busingen', 'Live'),
(137, 'DJ', 'Africa/Djibouti', 'Live'),
(138, 'DK', 'Europe/Copenhagen', 'Live'),
(139, 'DM', 'America/Dominica', 'Live'),
(140, 'DO', 'America/Santo_Domingo', 'Live'),
(141, 'DZ', 'Africa/Algiers', 'Live'),
(142, 'EC', 'America/Guayaquil', 'Live'),
(143, 'EC', 'Pacific/Galapagos', 'Live'),
(144, 'EE', 'Europe/Tallinn', 'Live'),
(145, 'EG', 'Africa/Cairo', 'Live'),
(146, 'EH', 'Africa/El_Aaiun', 'Live'),
(147, 'ER', 'Africa/Asmara', 'Live'),
(148, 'ES', 'Europe/Madrid', 'Live'),
(149, 'ES', 'Africa/Ceuta', 'Live'),
(150, 'ES', 'Atlantic/Canary', 'Live'),
(151, 'ET', 'Africa/Addis_Ababa', 'Live'),
(152, 'FI', 'Europe/Helsinki', 'Live'),
(153, 'FJ', 'Pacific/Fiji', 'Live'),
(154, 'FK', 'Atlantic/Stanley', 'Live'),
(155, 'FM', 'Pacific/Chuuk', 'Live'),
(156, 'FM', 'Pacific/Pohnpei', 'Live'),
(157, 'FM', 'Pacific/Kosrae', 'Live'),
(158, 'FO', 'Atlantic/Faroe', 'Live'),
(159, 'FR', 'Europe/Paris', 'Live'),
(160, 'GA', 'Africa/Libreville', 'Live'),
(161, 'GB', 'Europe/London', 'Live'),
(162, 'GD', 'America/Grenada', 'Live'),
(163, 'GE', 'Asia/Tbilisi', 'Live'),
(164, 'GF', 'America/Cayenne', 'Live'),
(165, 'GG', 'Europe/Guernsey', 'Live'),
(166, 'GH', 'Africa/Accra', 'Live'),
(167, 'GI', 'Europe/Gibraltar', 'Live'),
(168, 'GL', 'America/Godthab', 'Live'),
(169, 'GL', 'America/Danmarkshavn', 'Live'),
(170, 'GL', 'America/Scoresbysund', 'Live'),
(171, 'GL', 'America/Thule', 'Live'),
(172, 'GM', 'Africa/Banjul', 'Live'),
(173, 'GN', 'Africa/Conakry', 'Live'),
(174, 'GP', 'America/Guadeloupe', 'Live'),
(175, 'GQ', 'Africa/Malabo', 'Live'),
(176, 'GR', 'Europe/Athens', 'Live'),
(177, 'GS', 'Atlantic/South_Georgia', 'Live'),
(178, 'GT', 'America/Guatemala', 'Live'),
(179, 'GU', 'Pacific/Guam', 'Live'),
(180, 'GW', 'Africa/Bissau', 'Live'),
(181, 'GY', 'America/Guyana', 'Live'),
(182, 'HK', 'Asia/Hong_Kong', 'Live'),
(183, 'HN', 'America/Tegucigalpa', 'Live'),
(184, 'HR', 'Europe/Zagreb', 'Live'),
(185, 'HT', 'America/Port-au-Prince', 'Live'),
(186, 'HU', 'Europe/Budapest', 'Live'),
(187, 'ID', 'Asia/Jakarta', 'Live'),
(188, 'ID', 'Asia/Pontianak', 'Live'),
(189, 'ID', 'Asia/Makassar', 'Live'),
(190, 'ID', 'Asia/Jayapura', 'Live'),
(191, 'IE', 'Europe/Dublin', 'Live'),
(192, 'IL', 'Asia/Jerusalem', 'Live'),
(193, 'IM', 'Europe/Isle_of_Man', 'Live'),
(194, 'IN', 'Asia/Kolkata', 'Live'),
(195, 'IO', 'Indian/Chagos', 'Live'),
(196, 'IQ', 'Asia/Baghdad', 'Live'),
(197, 'IR', 'Asia/Tehran', 'Live'),
(198, 'IS', 'Atlantic/Reykjavik', 'Live'),
(199, 'IT', 'Europe/Rome', 'Live'),
(200, 'JE', 'Europe/Jersey', 'Live'),
(201, 'JM', 'America/Jamaica', 'Live'),
(202, 'JO', 'Asia/Amman', 'Live'),
(203, 'JP', 'Asia/Tokyo', 'Live'),
(204, 'KE', 'Africa/Nairobi', 'Live'),
(205, 'KG', 'Asia/Bishkek', 'Live'),
(206, 'KH', 'Asia/Phnom_Penh', 'Live'),
(207, 'KI', 'Pacific/Tarawa', 'Live'),
(208, 'KI', 'Pacific/Enderbury', 'Live'),
(209, 'KI', 'Pacific/Kiritimati', 'Live'),
(210, 'KM', 'Indian/Comoro', 'Live'),
(211, 'KN', 'America/St_Kitts', 'Live'),
(212, 'KP', 'Asia/Pyongyang', 'Live'),
(213, 'KR', 'Asia/Seoul', 'Live'),
(214, 'KW', 'Asia/Kuwait', 'Live'),
(215, 'KY', 'America/Cayman', 'Live'),
(216, 'KZ', 'Asia/Almaty', 'Live'),
(217, 'KZ', 'Asia/Qyzylorda', 'Live'),
(218, 'KZ', 'Asia/Aqtobe', 'Live'),
(219, 'KZ', 'Asia/Aqtau', 'Live'),
(220, 'KZ', 'Asia/Atyrau', 'Live'),
(221, 'KZ', 'Asia/Oral', 'Live'),
(222, 'LA', 'Asia/Vientiane', 'Live'),
(223, 'LB', 'Asia/Beirut', 'Live'),
(224, 'LC', 'America/St_Lucia', 'Live'),
(225, 'LI', 'Europe/Vaduz', 'Live'),
(226, 'LK', 'Asia/Colombo', 'Live'),
(227, 'LR', 'Africa/Monrovia', 'Live'),
(228, 'LS', 'Africa/Maseru', 'Live'),
(229, 'LT', 'Europe/Vilnius', 'Live'),
(230, 'LU', 'Europe/Luxembourg', 'Live'),
(231, 'LV', 'Europe/Riga', 'Live'),
(232, 'LY', 'Africa/Tripoli', 'Live'),
(233, 'MA', 'Africa/Casablanca', 'Live'),
(234, 'MC', 'Europe/Monaco', 'Live'),
(235, 'MD', 'Europe/Chisinau', 'Live'),
(236, 'ME', 'Europe/Podgorica', 'Live'),
(237, 'MF', 'America/Marigot', 'Live'),
(238, 'MG', 'Indian/Antananarivo', 'Live'),
(239, 'MH', 'Pacific/Majuro', 'Live'),
(240, 'MH', 'Pacific/Kwajalein', 'Live'),
(241, 'MK', 'Europe/Skopje', 'Live'),
(242, 'ML', 'Africa/Bamako', 'Live'),
(243, 'MM', 'Asia/Yangon', 'Live'),
(244, 'MN', 'Asia/Ulaanbaatar', 'Live'),
(245, 'MN', 'Asia/Hovd', 'Live'),
(246, 'MN', 'Asia/Choibalsan', 'Live'),
(247, 'MO', 'Asia/Macau', 'Live'),
(248, 'MP', 'Pacific/Saipan', 'Live'),
(249, 'MQ', 'America/Martinique', 'Live'),
(250, 'MR', 'Africa/Nouakchott', 'Live'),
(251, 'MS', 'America/Montserrat', 'Live'),
(252, 'MT', 'Europe/Malta', 'Live'),
(253, 'MU', 'Indian/Mauritius', 'Live'),
(254, 'MV', 'Indian/Maldives', 'Live'),
(255, 'MW', 'Africa/Blantyre', 'Live'),
(256, 'MX', 'America/Mexico_City', 'Live'),
(257, 'MX', 'America/Cancun', 'Live'),
(258, 'MX', 'America/Merida', 'Live'),
(259, 'MX', 'America/Monterrey', 'Live'),
(260, 'MX', 'America/Matamoros', 'Live'),
(261, 'MX', 'America/Mazatlan', 'Live'),
(262, 'MX', 'America/Chihuahua', 'Live'),
(263, 'MX', 'America/Ojinaga', 'Live'),
(264, 'MX', 'America/Hermosillo', 'Live'),
(265, 'MX', 'America/Tijuana', 'Live'),
(266, 'MX', 'America/Bahia_Banderas', 'Live'),
(267, 'MY', 'Asia/Kuala_Lumpur', 'Live'),
(268, 'MY', 'Asia/Kuching', 'Live'),
(269, 'MZ', 'Africa/Maputo', 'Live'),
(270, 'NA', 'Africa/Windhoek', 'Live'),
(271, 'NC', 'Pacific/Noumea', 'Live'),
(272, 'NE', 'Africa/Niamey', 'Live'),
(273, 'NF', 'Pacific/Norfolk', 'Live'),
(274, 'NG', 'Africa/Lagos', 'Live'),
(275, 'NI', 'America/Managua', 'Live'),
(276, 'NL', 'Europe/Amsterdam', 'Live'),
(277, 'NO', 'Europe/Oslo', 'Live'),
(278, 'NP', 'Asia/Kathmandu', 'Live'),
(279, 'NR', 'Pacific/Nauru', 'Live'),
(280, 'NU', 'Pacific/Niue', 'Live'),
(281, 'NZ', 'Pacific/Auckland', 'Live'),
(282, 'NZ', 'Pacific/Chatham', 'Live'),
(283, 'OM', 'Asia/Muscat', 'Live'),
(284, 'PA', 'America/Panama', 'Live'),
(285, 'PE', 'America/Lima', 'Live'),
(286, 'PF', 'Pacific/Tahiti', 'Live'),
(287, 'PF', 'Pacific/Marquesas', 'Live'),
(288, 'PF', 'Pacific/Gambier', 'Live'),
(289, 'PG', 'Pacific/Port_Moresby', 'Live'),
(290, 'PG', 'Pacific/Bougainville', 'Live'),
(291, 'PH', 'Asia/Manila', 'Live'),
(292, 'PK', 'Asia/Karachi', 'Live'),
(293, 'PL', 'Europe/Warsaw', 'Live'),
(294, 'PM', 'America/Miquelon', 'Live'),
(295, 'PN', 'Pacific/Pitcairn', 'Live'),
(296, 'PR', 'America/Puerto_Rico', 'Live'),
(297, 'PS', 'Asia/Gaza', 'Live'),
(298, 'PS', 'Asia/Hebron', 'Live'),
(299, 'PT', 'Europe/Lisbon', 'Live'),
(300, 'PT', 'Atlantic/Madeira', 'Live'),
(301, 'PT', 'Atlantic/Azores', 'Live'),
(302, 'PW', 'Pacific/Palau', 'Live'),
(303, 'PY', 'America/Asuncion', 'Live'),
(304, 'QA', 'Asia/Qatar', 'Live'),
(305, 'RE', 'Indian/Reunion', 'Live'),
(306, 'RO', 'Europe/Bucharest', 'Live'),
(307, 'RS', 'Europe/Belgrade', 'Live'),
(308, 'RU', 'Europe/Kaliningrad', 'Live'),
(309, 'RU', 'Europe/Moscow', 'Live'),
(310, 'RU', 'Europe/Simferopol', 'Live'),
(311, 'RU', 'Europe/Volgograd', 'Live'),
(312, 'RU', 'Europe/Kirov', 'Live'),
(313, 'RU', 'Europe/Astrakhan', 'Live'),
(314, 'RU', 'Europe/Saratov', 'Live'),
(315, 'RU', 'Europe/Ulyanovsk', 'Live'),
(316, 'RU', 'Europe/Samara', 'Live'),
(317, 'RU', 'Asia/Yekaterinburg', 'Live'),
(318, 'RU', 'Asia/Omsk', 'Live'),
(319, 'RU', 'Asia/Novosibirsk', 'Live'),
(320, 'RU', 'Asia/Barnaul', 'Live'),
(321, 'RU', 'Asia/Tomsk', 'Live'),
(322, 'RU', 'Asia/Novokuznetsk', 'Live'),
(323, 'RU', 'Asia/Krasnoyarsk', 'Live'),
(324, 'RU', 'Asia/Irkutsk', 'Live'),
(325, 'RU', 'Asia/Chita', 'Live'),
(326, 'RU', 'Asia/Yakutsk', 'Live'),
(327, 'RU', 'Asia/Khandyga', 'Live'),
(328, 'RU', 'Asia/Vladivostok', 'Live'),
(329, 'RU', 'Asia/Ust-Nera', 'Live'),
(330, 'RU', 'Asia/Magadan', 'Live'),
(331, 'RU', 'Asia/Sakhalin', 'Live'),
(332, 'RU', 'Asia/Srednekolymsk', 'Live'),
(333, 'RU', 'Asia/Kamchatka', 'Live'),
(334, 'RU', 'Asia/Anadyr', 'Live'),
(335, 'RW', 'Africa/Kigali', 'Live'),
(336, 'SA', 'Asia/Riyadh', 'Live'),
(337, 'SB', 'Pacific/Guadalcanal', 'Live'),
(338, 'SC', 'Indian/Mahe', 'Live'),
(339, 'SD', 'Africa/Khartoum', 'Live'),
(340, 'SE', 'Europe/Stockholm', 'Live'),
(341, 'SG', 'Asia/Singapore', 'Live'),
(342, 'SH', 'Atlantic/St_Helena', 'Live'),
(343, 'SI', 'Europe/Ljubljana', 'Live'),
(344, 'SJ', 'Arctic/Longyearbyen', 'Live'),
(345, 'SK', 'Europe/Bratislava', 'Live'),
(346, 'SL', 'Africa/Freetown', 'Live'),
(347, 'SM', 'Europe/San_Marino', 'Live'),
(348, 'SN', 'Africa/Dakar', 'Live'),
(349, 'SO', 'Africa/Mogadishu', 'Live'),
(350, 'SR', 'America/Paramaribo', 'Live'),
(351, 'SS', 'Africa/Juba', 'Live'),
(352, 'ST', 'Africa/Sao_Tome', 'Live'),
(353, 'SV', 'America/El_Salvador', 'Live'),
(354, 'SX', 'America/Lower_Princes', 'Live'),
(355, 'SY', 'Asia/Damascus', 'Live'),
(356, 'SZ', 'Africa/Mbabane', 'Live'),
(357, 'TC', 'America/Grand_Turk', 'Live'),
(358, 'TD', 'Africa/Ndjamena', 'Live'),
(359, 'TF', 'Indian/Kerguelen', 'Live'),
(360, 'TG', 'Africa/Lome', 'Live'),
(361, 'TH', 'Asia/Bangkok', 'Live'),
(362, 'TJ', 'Asia/Dushanbe', 'Live'),
(363, 'TK', 'Pacific/Fakaofo', 'Live'),
(364, 'TL', 'Asia/Dili', 'Live'),
(365, 'TM', 'Asia/Ashgabat', 'Live'),
(366, 'TN', 'Africa/Tunis', 'Live'),
(367, 'TO', 'Pacific/Tongatapu', 'Live'),
(368, 'TR', 'Europe/Istanbul', 'Live'),
(369, 'TT', 'America/Port_of_Spain', 'Live'),
(370, 'TV', 'Pacific/Funafuti', 'Live'),
(371, 'TW', 'Asia/Taipei', 'Live'),
(372, 'TZ', 'Africa/Dar_es_Salaam', 'Live'),
(373, 'UA', 'Europe/Kiev', 'Live'),
(374, 'UA', 'Europe/Uzhgorod', 'Live'),
(375, 'UA', 'Europe/Zaporozhye', 'Live'),
(376, 'UG', 'Africa/Kampala', 'Live'),
(377, 'UM', 'Pacific/Midway', 'Live'),
(378, 'UM', 'Pacific/Wake', 'Live'),
(379, 'US', 'America/New_York', 'Live'),
(380, 'US', 'America/Detroit', 'Live'),
(381, 'US', 'America/Kentucky/Louisville', 'Live'),
(382, 'US', 'America/Kentucky/Monticello', 'Live'),
(383, 'US', 'America/Indiana/Indianapolis', 'Live'),
(384, 'US', 'America/Indiana/Vincennes', 'Live'),
(385, 'US', 'America/Indiana/Winamac', 'Live'),
(386, 'US', 'America/Indiana/Marengo', 'Live'),
(387, 'US', 'America/Indiana/Petersburg', 'Live'),
(388, 'US', 'America/Indiana/Vevay', 'Live'),
(389, 'US', 'America/Chicago', 'Live'),
(390, 'US', 'America/Indiana/Tell_City', 'Live'),
(391, 'US', 'America/Indiana/Knox', 'Live'),
(392, 'US', 'America/Menominee', 'Live'),
(393, 'US', 'America/North_Dakota/Center', 'Live'),
(394, 'US', 'America/North_Dakota/New_Salem', 'Live'),
(395, 'US', 'America/North_Dakota/Beulah', 'Live'),
(396, 'US', 'America/Denver', 'Live'),
(397, 'US', 'America/Boise', 'Live'),
(398, 'US', 'America/Phoenix', 'Live'),
(399, 'US', 'America/Los_Angeles', 'Live'),
(400, 'US', 'America/Anchorage', 'Live'),
(401, 'US', 'America/Juneau', 'Live'),
(402, 'US', 'America/Sitka', 'Live'),
(403, 'US', 'America/Metlakatla', 'Live'),
(404, 'US', 'America/Yakutat', 'Live'),
(405, 'US', 'America/Nome', 'Live'),
(406, 'US', 'America/Adak', 'Live'),
(407, 'US', 'Pacific/Honolulu', 'Live'),
(408, 'UY', 'America/Montevideo', 'Live'),
(409, 'UZ', 'Asia/Samarkand', 'Live'),
(410, 'UZ', 'Asia/Tashkent', 'Live'),
(411, 'VA', 'Europe/Vatican', 'Live'),
(412, 'VC', 'America/St_Vincent', 'Live'),
(413, 'VE', 'America/Caracas', 'Live'),
(414, 'VG', 'America/Tortola', 'Live'),
(415, 'VI', 'America/St_Thomas', 'Live'),
(416, 'VN', 'Asia/Ho_Chi_Minh', 'Live'),
(417, 'VU', 'Pacific/Efate', 'Live'),
(418, 'WF', 'Pacific/Wallis', 'Live'),
(419, 'WS', 'Pacific/Apia', 'Live'),
(420, 'YE', 'Asia/Aden', 'Live'),
(421, 'YT', 'Indian/Mayotte', 'Live'),
(422, 'ZA', 'Africa/Johannesburg', 'Live'),
(423, 'ZM', 'Africa/Lusaka', 'Live'),
(424, 'ZW', 'Africa/Harare', 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_units`
--

CREATE TABLE `tbl_units` (
  `id` int(10) NOT NULL,
  `unit_name` varchar(10) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `company_id` int(1) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_units`
--

INSERT INTO `tbl_units` (`id`, `unit_name`, `description`, `company_id`, `del_status`) VALUES
(1, 'Kg', '', 1, 'Live'),
(2, 'Ml', '', 1, 'Live'),
(3, 'Pcs', '', 1, 'Live'),
(4, 'Ltr', '', 1, 'Live'),
(5, 'Ons', '', 1, 'Live'),
(6, 'Grm', '', 1, 'Live'),
(7, 'Botol', '', 1, 'Live'),
(8, 'Karung', '', 1, 'Live'),
(9, 'Box', '', 1, 'Live'),
(10, 'Satuan', '', 1, 'Live'),
(11, 'Liter', '', 1, 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(10) NOT NULL,
  `full_name` varchar(25) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email_address` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `will_login` varchar(20) NOT NULL DEFAULT 'No',
  `role` varchar(25) NOT NULL,
  `outlet_id` int(10) NOT NULL,
  `company_id` int(10) NOT NULL,
  `account_creation_date` datetime NOT NULL,
  `language` varchar(100) NOT NULL DEFAULT 'english',
  `last_login` datetime NOT NULL,
  `active_status` varchar(25) NOT NULL DEFAULT 'Active',
  `del_status` varchar(10) NOT NULL DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `full_name`, `phone`, `email_address`, `password`, `designation`, `will_login`, `role`, `outlet_id`, `company_id`, `account_creation_date`, `language`, `last_login`, `active_status`, `del_status`) VALUES
(1, 'Irfan Arifin', '082116982479', 'admin@repos.id', '123456', NULL, 'No', 'Admin', 1, 1, '2018-02-17 07:28:32', 'english', '0000-00-00 00:00:00', 'Active', 'Live'),
(4, 'Waiter', NULL, 'waiter@bafageh.com', '123456', 'Waiter', 'Yes', 'User', 1, 1, '0000-00-00 00:00:00', 'english', '0000-00-00 00:00:00', 'Active', 'Live');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user_menu_access`
--

CREATE TABLE `tbl_user_menu_access` (
  `id` int(10) NOT NULL,
  `menu_id` int(10) DEFAULT 0,
  `user_id` int(10) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_user_menu_access`
--

INSERT INTO `tbl_user_menu_access` (`id`, `menu_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 9, 1),
(12, 11, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 5, 1),
(18, 10, 1),
(19, 12, 1),
(20, 17, 1),
(21, 18, 1),
(22, 19, 1),
(47, 19, 3),
(48, 19, 4),
(76, 16, 2),
(77, 17, 2),
(78, 15, 2),
(79, 8, 2),
(80, 6, 2),
(81, 3, 2),
(82, 13, 2),
(83, 18, 2),
(84, 9, 2),
(85, 2, 2),
(86, 7, 2),
(87, 1, 2),
(88, 11, 2),
(89, 10, 2),
(90, 19, 2),
(91, 4, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_vats`
--

CREATE TABLE `tbl_vats` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `percentage` float NOT NULL,
  `user_id` float NOT NULL DEFAULT 1,
  `company_id` int(11) DEFAULT NULL,
  `del_status` varchar(6) DEFAULT 'Live',
  `status` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_vats`
--

INSERT INTO `tbl_vats` (`id`, `name`, `percentage`, `user_id`, `company_id`, `del_status`, `status`) VALUES
(1, 'PPN', 0, 1, 1, 'Live', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_wastes`
--

CREATE TABLE `tbl_wastes` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(50) DEFAULT NULL,
  `date` date NOT NULL,
  `total_loss` float(10,2) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `employee_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `food_menu_id` int(11) DEFAULT NULL,
  `food_menu_waste_qty` int(11) DEFAULT NULL,
  `menu_resto_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_wastes`
--

INSERT INTO `tbl_wastes` (`id`, `reference_no`, `date`, `total_loss`, `note`, `employee_id`, `user_id`, `outlet_id`, `del_status`, `food_menu_id`, `food_menu_waste_qty`, `menu_resto_id`) VALUES
(1, '000001', '2020-06-21', 160000.00, 'Jatuh', 1, 1, 1, 'Live', NULL, 0, 3),
(2, '000002', '2020-06-23', 20000.00, 'jatuh', 1, 1, 1, 'Live', NULL, 0, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_waste_ingredients`
--

CREATE TABLE `tbl_waste_ingredients` (
  `id` int(10) NOT NULL,
  `ingredient_id` int(10) DEFAULT NULL,
  `waste_amount` float(10,2) DEFAULT NULL,
  `last_purchase_price` float(10,2) DEFAULT NULL,
  `loss_amount` float(10,2) DEFAULT NULL,
  `waste_id` int(10) DEFAULT NULL,
  `outlet_id` int(10) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_waste_ingredients`
--

INSERT INTO `tbl_waste_ingredients` (`id`, `ingredient_id`, `waste_amount`, `last_purchase_price`, `loss_amount`, `waste_id`, `outlet_id`, `del_status`) VALUES
(1, 66, 1.00, 160000.00, 160000.00, 1, 1, 'Live'),
(2, 74, 1.00, 20000.00, 20000.00, 2, 1, 'Live');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_admin_currencies`
--
ALTER TABLE `tbl_admin_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_admin_user_menus`
--
ALTER TABLE `tbl_admin_user_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_companies`
--
ALTER TABLE `tbl_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_customers`
--
ALTER TABLE `tbl_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_customer_due_receives`
--
ALTER TABLE `tbl_customer_due_receives`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_employees`
--
ALTER TABLE `tbl_employees`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_expenses`
--
ALTER TABLE `tbl_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_expense_items`
--
ALTER TABLE `tbl_expense_items`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_food_menus`
--
ALTER TABLE `tbl_food_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_food_menus_ingredients`
--
ALTER TABLE `tbl_food_menus_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_food_menus_modifiers`
--
ALTER TABLE `tbl_food_menus_modifiers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_food_menu_categories`
--
ALTER TABLE `tbl_food_menu_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_holds`
--
ALTER TABLE `tbl_holds`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_holds_details`
--
ALTER TABLE `tbl_holds_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_holds_details_modifiers`
--
ALTER TABLE `tbl_holds_details_modifiers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_ingredients`
--
ALTER TABLE `tbl_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_ingredient_categories`
--
ALTER TABLE `tbl_ingredient_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_inventory_adjustment`
--
ALTER TABLE `tbl_inventory_adjustment`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_modifiers`
--
ALTER TABLE `tbl_modifiers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_modifier_ingredients`
--
ALTER TABLE `tbl_modifier_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_notification_bar_kitchen_panel`
--
ALTER TABLE `tbl_notification_bar_kitchen_panel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_orders_table`
--
ALTER TABLE `tbl_orders_table`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_outlets`
--
ALTER TABLE `tbl_outlets`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_outlet_taxes`
--
ALTER TABLE `tbl_outlet_taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_payment_methods`
--
ALTER TABLE `tbl_payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_purchase`
--
ALTER TABLE `tbl_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_purchase_ingredients`
--
ALTER TABLE `tbl_purchase_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_register`
--
ALTER TABLE `tbl_register`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_sales`
--
ALTER TABLE `tbl_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_sales_details`
--
ALTER TABLE `tbl_sales_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_sales_details_modifiers`
--
ALTER TABLE `tbl_sales_details_modifiers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_sale_consumptions`
--
ALTER TABLE `tbl_sale_consumptions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_sale_consumptions_of_menus`
--
ALTER TABLE `tbl_sale_consumptions_of_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_sale_consumptions_of_modifiers_of_menus`
--
ALTER TABLE `tbl_sale_consumptions_of_modifiers_of_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_sms_settings`
--
ALTER TABLE `tbl_sms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_suppliers`
--
ALTER TABLE `tbl_suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_supplier_payments`
--
ALTER TABLE `tbl_supplier_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_tables`
--
ALTER TABLE `tbl_tables`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_temp_kot`
--
ALTER TABLE `tbl_temp_kot`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_time_zone`
--
ALTER TABLE `tbl_time_zone`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_units`
--
ALTER TABLE `tbl_units`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user_menu_access`
--
ALTER TABLE `tbl_user_menu_access`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_vats`
--
ALTER TABLE `tbl_vats`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_wastes`
--
ALTER TABLE `tbl_wastes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_waste_ingredients`
--
ALTER TABLE `tbl_waste_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_admin_currencies`
--
ALTER TABLE `tbl_admin_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT untuk tabel `tbl_admin_user_menus`
--
ALTER TABLE `tbl_admin_user_menus`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_companies`
--
ALTER TABLE `tbl_companies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_customers`
--
ALTER TABLE `tbl_customers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_customer_due_receives`
--
ALTER TABLE `tbl_customer_due_receives`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_employees`
--
ALTER TABLE `tbl_employees`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_expenses`
--
ALTER TABLE `tbl_expenses`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_expense_items`
--
ALTER TABLE `tbl_expense_items`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_food_menus`
--
ALTER TABLE `tbl_food_menus`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT untuk tabel `tbl_food_menus_ingredients`
--
ALTER TABLE `tbl_food_menus_ingredients`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT untuk tabel `tbl_food_menus_modifiers`
--
ALTER TABLE `tbl_food_menus_modifiers`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_food_menu_categories`
--
ALTER TABLE `tbl_food_menu_categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tbl_holds`
--
ALTER TABLE `tbl_holds`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_holds_details`
--
ALTER TABLE `tbl_holds_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_holds_details_modifiers`
--
ALTER TABLE `tbl_holds_details_modifiers`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_ingredients`
--
ALTER TABLE `tbl_ingredients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT untuk tabel `tbl_ingredient_categories`
--
ALTER TABLE `tbl_ingredient_categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_inventory_adjustment`
--
ALTER TABLE `tbl_inventory_adjustment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_modifiers`
--
ALTER TABLE `tbl_modifiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_modifier_ingredients`
--
ALTER TABLE `tbl_modifier_ingredients`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_notification_bar_kitchen_panel`
--
ALTER TABLE `tbl_notification_bar_kitchen_panel`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_orders_table`
--
ALTER TABLE `tbl_orders_table`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_outlets`
--
ALTER TABLE `tbl_outlets`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_outlet_taxes`
--
ALTER TABLE `tbl_outlet_taxes`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_payment_methods`
--
ALTER TABLE `tbl_payment_methods`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_purchase`
--
ALTER TABLE `tbl_purchase`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_purchase_ingredients`
--
ALTER TABLE `tbl_purchase_ingredients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_register`
--
ALTER TABLE `tbl_register`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `tbl_sales`
--
ALTER TABLE `tbl_sales`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `tbl_sales_details`
--
ALTER TABLE `tbl_sales_details`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `tbl_sales_details_modifiers`
--
ALTER TABLE `tbl_sales_details_modifiers`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_sale_consumptions`
--
ALTER TABLE `tbl_sale_consumptions`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `tbl_sale_consumptions_of_menus`
--
ALTER TABLE `tbl_sale_consumptions_of_menus`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `tbl_sale_consumptions_of_modifiers_of_menus`
--
ALTER TABLE `tbl_sale_consumptions_of_modifiers_of_menus`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_sms_settings`
--
ALTER TABLE `tbl_sms_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_suppliers`
--
ALTER TABLE `tbl_suppliers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_supplier_payments`
--
ALTER TABLE `tbl_supplier_payments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_tables`
--
ALTER TABLE `tbl_tables`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tbl_temp_kot`
--
ALTER TABLE `tbl_temp_kot`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_time_zone`
--
ALTER TABLE `tbl_time_zone`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=425;

--
-- AUTO_INCREMENT untuk tabel `tbl_units`
--
ALTER TABLE `tbl_units`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_user_menu_access`
--
ALTER TABLE `tbl_user_menu_access`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT untuk tabel `tbl_vats`
--
ALTER TABLE `tbl_vats`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_wastes`
--
ALTER TABLE `tbl_wastes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_waste_ingredients`
--
ALTER TABLE `tbl_waste_ingredients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
